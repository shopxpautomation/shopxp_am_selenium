﻿

using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using ShopXP_AM.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace ShopXP_AM
{
    
    public class PageOperationBase 
    {
        IWebDriver driver;
        public WebDriverWait wait;
        IJavaScriptExecutor javaScriptExecutor;

        public PageOperationBase(IWebDriver webdriver)
        {

                wait = new WebDriverWait(webdriver, new TimeSpan(0, 0, 110));
                javaScriptExecutor = (IJavaScriptExecutor)webdriver;
            driver = webdriver;
        }

        public void enterText(IWebElement objectName, string value, string objectDescriptiveName)
        {
            try
            {
                objectName.Clear();
                objectName.SendKeys(value);
                Logger.LogPass(""+value+" entered in " + objectDescriptiveName);
            }
            catch (Exception e)
            {
                Logger.LogFail("Failed: Unable to enter text in the text box: " + objectDescriptiveName);
               
            }
        }

        public void AssertEqual(string actual, string expected,[Optional] string txt)
        {
            try
            {
                Assert.IsTrue(actual.Contains(expected));
                Logger.LogPass(txt+" "+expected + " Displayed as Expected ");
            }
            catch (Exception e)
            {
                Logger.LogFail("Expected " + expected +", Actual "+ actual);

            }
        }

        public void AssertEqual(int actual, int expected, [Optional] string txt)
        {
            try
            {
                Assert.IsTrue(actual==expected);
                Logger.LogPass(txt + " " + expected + " Displayed as Expected");
            }
            catch (Exception e)
            {
                Logger.LogFail("Expected " + expected + ", Actual " + actual);

            }
        }

        public string countNoOfItemsInString(string text, string search)
        {
            int count = 0;
            try
            {
                
                string[] textSplit = text.Split( ',');
                foreach (string txt in textSplit)
                {
                    if (txt.Contains(search))
                    {
                        count = count + 1;
                    }
                }
                Logger.LogPass(search+" Displayed");
            }
            catch (Exception e)
            {
                Logger.LogFail(search + " not displayed " );

            }

            return count.ToString();
        }


        public void AssertMoreThan(string actual, string expected)
        {
            try
            {
                int actual_Int = Int32.Parse(actual);
                int expected_Int = Int32.Parse(expected);
                Assert.IsTrue(actual_Int> expected_Int);
                Logger.LogPass(expected + " Displayed as Expected");
            }
            catch (Exception e)
            {
                Logger.LogFail("Expected " + expected + ", Actual " + actual);

            }
        }

        public void isTrue(Boolean condition, string msg)
        {
            try
            {
                if (condition)
                {
                    Logger.LogPass(msg);
                }
                else
                {
                    Logger.LogFail("Failed; " + msg);
                }
                
            }
            catch (Exception e)
            {
                Logger.LogFail("Failed; " + msg);

            }
        }

        public Boolean getAttribute(IWebElement element,string attribute)
        {
            Boolean found =true;
            try
            {
                string text = element.GetAttribute(attribute);

            }
            catch (Exception e)
            {
                found = false;

            }
            return found;
        }

        public bool openNewTabInBrowser(string url)
        {
            bool flag = false;
            try
            {
                javaScriptExecutor.ExecuteScript("window.open();");
                Thread.Sleep(5000);
                this.switchToNewWindow();
                string script = "window.location =\'" + url + "\'";
                javaScriptExecutor.ExecuteScript(script);
                flag = true;
            }
            catch
            {
                flag = false;
            }
            return flag;
        }

        public void navigateToURL(string url)
        {
            try
            {
                driver.Navigate().GoToUrl(url);
                waitForSomeTimeInSeconds(2);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to navigate to URL - " + ex.Message.ToString());
            }
        }

        public void switchToNewWindow()
        {
            try
            {
                string currentWindowHandler = driver.CurrentWindowHandle;
                Thread.Sleep(2000);

                List<string> windows = driver.WindowHandles.ToList<string>();
                windows.ForEach(t =>
                {
                    if (!t.Equals(currentWindowHandler))
                    {
                        driver.SwitchTo().Window(t);
                        string s = driver.Title;
                        driver.Manage().Window.Maximize();
                    }
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to switch to new window - " + ex.Message.ToString());
            }
        }

        public string getTextFromElement(IWebElement element)
        {
            string text = null;
            try
            {
                text = element.Text;
                return text;
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to get the text from element - " + ex.Message.ToString());
            
            }
            return text;
        }

        public void waitForPageToComplete()
        {
            try
            {
                waitForPageLoad();
                IWait<IWebDriver> wait = new WebDriverWait(driver, TimeSpan.FromSeconds(50));
                wait.Until(driver => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
            }
            catch (Exception e)
            {
                WaitForPageLoad();
            }
        }

        public void WaitForPageBodyToLoadFully()
        {
            IWebElement loadStatus = driver.FindElement(By.XPath("//body"));
            string status = loadStatus.GetAttribute("class");
            for (int i = 0; i < 5; i++)
            {
                if (status == "checkout-cart-index page-layout-1column")
                {
                    Thread.Sleep(2000);
                    status = loadStatus.GetAttribute("class");
                }
                else
                {
                    Thread.Sleep(5000);
                }
            }


        }

        public void openNewWindow()
        {

            javaScriptExecutor.ExecuteScript("window.open()");
           List<String> tabs = new List<String>(driver.WindowHandles);
            driver.SwitchTo().Window(tabs[1]);
        }
        public void WaitForPageLoad()
        {
            new WebDriverWait(driver, TimeSpan.FromSeconds(60)).Until(
            d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete"));
        }

        public void WaitForSpinnerDisappear()
        {
            try
            {
                for(int i = 0; i < 100; i++)
                {
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
                    Boolean found = isElementDisplay(driver.FindElement(By.Id("table-spinner")));
                    if (found)
                    {
                        Thread.Sleep(1000);
                    }else
                    {
                        break;
                    }
                }


            }catch(Exception e)
            {

            }
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            waitForPageLoad();

        }

        

        public string getInnerHTMLFromElement(IWebElement element)
        {
            string text = null;
            try
            {
                text = (string)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].innerHTML;", element);
                text = text.Replace(System.Environment.NewLine, string.Empty);
                return text;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to get the text from element - " + ex.Message.ToString());
            }
            return text;
        }

        public void waitForPageLoad()
        {
            Thread.Sleep(3000);
            new WebDriverWait(driver, TimeSpan.FromSeconds(60)).Until(
            d => ((IJavaScriptExecutor)d).ExecuteScript("return document.readyState").Equals("complete"));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
        }

        public void waitForSomeTimeInSeconds(int seconds)
        {
            Thread.Sleep(seconds * 1000);
        }

    
        public bool waitForElementToBeVisible(By element, int retry)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(retry));
            wait.Until(ExpectedConditions.ElementIsVisible(element));
            return true;
        }

        public void clickByJavaScript(IWebElement element, string objectDescriptiveName)
        {
            try
            {
                javaScriptExecutor.ExecuteScript("arguments[0].click();", element);
                Logger.LogPass("Clicked on " + objectDescriptiveName);
            }
            catch (Exception ex)
            {
                Logger.LogFail("Failed: Cannot click on the specified the object " + objectDescriptiveName );
                Console.WriteLine("Failed : Cannot click on the specified the object " + objectDescriptiveName + Environment.NewLine + ex.Message + Environment.NewLine + ex.Source + Environment.NewLine + ex.TargetSite);
            }
        }

        public void selectTextFromDropdown(IWebElement element, string text)
        {
            try
            {
                SelectElement selectElement = new SelectElement(element);
                selectElement.SelectByText(text);
                Logger.LogPass(" Selected " + text);
            }
            catch (Exception ex)
            {
                Logger.LogFail("Failed: Dropdown value selection failed : Reason : " + ex.Message);
                Console.WriteLine("Failed: Dropdown value selection failed : Reason : " + ex.Message);
            }
        }

        public void moveToElement(IWebElement myElement)
        {
            Actions Scroll = new Actions(driver);
            Scroll.MoveToElement(myElement);
            Scroll.Click().Build().Perform();            
        }

        public void scrollToTopofThePage()
        {
            javaScriptExecutor.ExecuteScript("window.scrollTo(0, 0);");
            this.waitForSomeTimeInSeconds(2);
        }

        public void scrollToBottomofThePage()
        {
            javaScriptExecutor.ExecuteScript("window.scrollTo(0, document.body.scrollHeight);");
        }

        public void scrollToMiddleOfThePage()
        {
            javaScriptExecutor.ExecuteScript("window.scrollTo(0, document.body.scrollHeight/2);");
        }

        public void scrollToElementJS(IWebElement myElement)
        {
            javaScriptExecutor.ExecuteScript("arguments[0].scrollIntoView();", myElement);
        }

        public void scrollByPixels(int pixel)
        {
            javaScriptExecutor.ExecuteScript("window.scrollBy(0," + pixel + ")");
        }

        public void clickOnObject(IWebElement element,string objectDescriptiveName)
        {
           
            try
            {
                element.Click();
                Logger.LogPass("Click on Element " + objectDescriptiveName);
            }
            catch (Exception ex)
            {
                Logger.LogFail("Failed: Cannot click on the specified the object " + objectDescriptiveName);
                Console.WriteLine("Failed : Cannot click on the specified the object " + objectDescriptiveName + Environment.NewLine + ex.Message + Environment.NewLine + ex.Source + Environment.NewLine + ex.TargetSite);
            }
        }

        public void switchToWindowBasedOnTitle(string titleOfCurrentWindow)
        {
            try
            {
                List<string> windows = driver.WindowHandles.ToList();
                foreach (var window in windows)
                {
                    driver.SwitchTo().Window(window);
                    if (driver.Title == titleOfCurrentWindow)
                    {
                        break;
                    }
                }
                Logger.LogPass(" Switched to window " + titleOfCurrentWindow);
            }catch(Exception ex)
            {
                Logger.LogFail("Failed: Unable to switch to window " + titleOfCurrentWindow);
                Console.WriteLine("Failed: Unable to switch to window " + titleOfCurrentWindow + Environment.NewLine + ex.Message + Environment.NewLine + ex.Source + Environment.NewLine + ex.TargetSite);
            }


        }

        public bool isElementDisplay(IWebElement element)
        {
            bool isDisplayed = false;
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
            try
            {
                isDisplayed = element.Displayed;
                if (isDisplayed)
                    return isDisplayed;
                else
                    return isDisplayed;
            }
            catch {
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            }
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            return isDisplayed;
        }

        public void checkElementDisplay(IWebElement element, string descriptiveName)
        {
            try
            {
                Assert.IsTrue(element.Displayed);
                Logger.LogPass(" Element displayed : " + descriptiveName);
            }
            catch (Exception ex)
            {
                Logger.LogFail("Failed: Element not displayed " + descriptiveName);
                Console.WriteLine("Failed: Element not displayed " + descriptiveName + Environment.NewLine + ex.Message + Environment.NewLine + ex.Source + Environment.NewLine + ex.TargetSite);
            }
        }

        public void checkElementDisplaybyText(string text)
        {
            try
            {
                IWebElement element = driver.FindElement(By.XPath("//*[contains(text(),'"+ text + "')]"));
                Assert.IsTrue(element.Displayed);
                Logger.LogPass(" Element displayed : " + text);
            }
            catch (Exception ex)
            {
                Logger.LogFail("Failed: Element not displayed " + text);
                Console.WriteLine("Failed: Element not displayed " + text + Environment.NewLine + ex.Message + Environment.NewLine + ex.Source + Environment.NewLine + ex.TargetSite);
            }
        }

        public void checkElementDisplaybyText2(string text)
        {
            try
            {
                IWebElement element = driver.FindElement(By.XPath("(//*[contains(text(),'" + text + "')])[last()]"));
                Assert.IsTrue(element.Displayed);
                Logger.LogPass(" Element displayed : " + text);
            }
            catch (Exception ex)
            {
                Logger.LogFail("Failed: Element not displayed " + text);
                Console.WriteLine("Failed: Element not displayed " + text + Environment.NewLine + ex.Message + Environment.NewLine + ex.Source + Environment.NewLine + ex.TargetSite);
            }
        }
        public bool waitForItemToBeVisible(IWebElement element, int retry)
             {
            while (!element.Displayed)
            {
                if (retry <= 0)
                    return false;
                Thread.Sleep(1000);
                retry--;
            }
            return true;
        }

        public bool waitForItemToBeDisplayed(IWebElement element, int retry)
        {
            bool displayElement = false;
            for (int i = 0; i <= retry; i++)
            {
                try
                {
                    if (element.Displayed)
                    {
                        displayElement = true;
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Thread.Sleep(1000);
                    Console.WriteLine(ex.Message.ToString());
                }
                Thread.Sleep(3000);
            }
            return displayElement;
        }

        public bool waitForItemToBeEnabled(IWebElement element, int retry)
        {
            while (!element.Enabled)
            {
                if (retry <= 0)
                    return false;
                Thread.Sleep(1000);
                retry--;
            }
            return true;
        }

        public string getCssValue(IWebElement element)
        {
            string text = null;
            try
            {
                text = element.GetCssValue("Color");
                return text;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
            return text;
        }        
    }
}