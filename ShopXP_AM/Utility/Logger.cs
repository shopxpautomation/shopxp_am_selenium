﻿
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports.Reporter.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace ShopXP_AM.Utility
{
    class Logger
    {
        public static ExtentHtmlReporter ExtentHtmlReporter;
        public static ExtentTest featureName;
        public static ExtentTest test;
        public static ExtentReports extent;
       
        
        /// <summary>
        /// Start the node for the given tag
        /// </summary>
        /// <param name="tag">Tag name</param>
        /// <returns>Returns corresponding Html representation</returns>
        public static string startNode(string tag)
        {
            return string.Format("<{0}>", tag);
        }

        /// <summary>
        /// Starts the node.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="border">The border.</param>
        /// <returns>starting node </returns>
        public static string startNode(string tag, string border)
        {
            return string.Format("<{0} border=\"{1}\">", tag, border);
        }

        /// <summary>
        /// Close the node for the given tag
        /// </summary>
        /// <param name="tag">Tag name</param>
        /// <returns>Returns corresponding Html representation</returns>
        public static string closeNode(string tag)
        {
            return string.Format("</{0}>", tag);
        }

        public static string getRow(string td1, string td2)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(Logger.startNode("tr"));
            builder.AppendLine(Logger.getNode(TagType.td, td1));
            builder.AppendLine(Logger.getNode(TagType.td, td2));
            builder.AppendLine(Logger.closeNode("tr"));
            return builder.ToString();
        }

        /// <summary>
        /// Get Html string for given tag, inner text and background color
        /// </summary>
        /// <param name="tag">Html tag name</param>
        /// <param name="innerText">Tag inner text</param>
        /// <param name="color">Tag background color</param>
        /// <returns>Returns corresponding Html representation</returns>
        public static string getNode(TagType tag, string innerText, string color = null)
        {
            string style = string.Format("style=\"background-color: {0};\"", color);

            if (color == null)
            {
                return string.Format("<{0}>{1}</{0}>", tag.ToString(), innerText);
            }
            else
            {
                return string.Format("<{0} {1}>{2}</{0}>", tag.ToString(), style, innerText);
            }
        }

        internal enum TagType
        {
            canvas,

            /// <summary>
            /// P(Paragraph) type tag
            /// </summary>
            P,

            /// <summary>
            /// h2(Header) type tag
            /// </summary>
            h2,

            h3,

            h4,

            /// <summary>
            /// h1(Header) type tag
            /// </summary>
            h1,

            /// <summary>
            /// Table cell type tag
            /// </summary>
            td,

            /// <summary>
            /// Table row type tag
            /// </summary>
            tr,

            /// <summary>
            /// Table type tag
            /// </summary>
            table,

            /// <summary>
            /// Table header
            /// </summary>
            th,

            /// <summary>
            /// Empty line
            /// </summary>
            br,

        }
        public static void initializeReport()
        {
            ExtentHtmlReporter = new ExtentHtmlReporter(SetUp.filePath + "\\index.html");
            ExtentHtmlReporter.Config.Theme = Theme.Dark;
            ExtentHtmlReporter.Config.DocumentTitle = "Automation Test Run Report";
            ExtentHtmlReporter.Config.ReportName = ConfigurationManager.AppSettings["Suite"];
           
            extent = new ExtentReports();
            extent.AttachReporter(ExtentHtmlReporter);
            extent.AddSystemInfo("Machine", Environment.MachineName);
            extent.AddSystemInfo("OS", Environment.OSVersion.VersionString);
            extent.AddSystemInfo("URL", SetUp.url);
            extent.AddSystemInfo("Author", "Vinay K V");

            string browserType = ConfigurationManager.AppSettings["Browser"];
            
            switch (browserType)
            {
                case "chrome":
                    extent.AddSystemInfo("Browser Type", "Chrome");
                    break;

                case "firefox":
                    extent.AddSystemInfo("Browser Type", "Firefox");
                    break;

                case "ie":
                    extent.AddSystemInfo("Browser Type", "IE");
                    break;

                case "edge":
                    extent.AddSystemInfo("Browser Type", "Microsoft Edge");
                    break;
                default:
                    break;
            }
        }
        
        public static void CreateTest(string testName)
        {
            test = extent.CreateTest(testName);
        }
        
        public static void LogPass( string message)
        {
            test.Log(Status.Pass, message);
        }

        public static void LogFail( string message)
        {
            test.Log(Status.Fail, message);
        }

        public static void LogInfo(string message)
        {
            test.Log(Status.Info, message);
        }
        public static void LogSnapShot(string name,IWebDriver driver)
        {
            ITakesScreenshot screenshotDriver = driver as ITakesScreenshot;
            Screenshot screenshot = screenshotDriver.GetScreenshot();
            string random = string.Format("{0}-{1}-{2}-{3}-{4}-{5}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            string path = SetUp.Snap_filePath + "/name_" + random + ".png";
            screenshot.SaveAsFile(path, ScreenshotImageFormat.Png);
            test.Log(Status.Pass, name);
            test.AddScreenCaptureFromPath(path);
        }

        public static void LogFailSnapShot(IWebDriver driver)
        {
            ITakesScreenshot screenshotDriver = driver as ITakesScreenshot;
            Screenshot screenshot = screenshotDriver.GetScreenshot();
            string random = string.Format("{0}-{1}-{2}-{3}-{4}-{5}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            string path = SetUp.Snap_filePath + "/name_" + random + ".png";
            screenshot.SaveAsFile(path, ScreenshotImageFormat.Png);
            test.Log(Status.Fail, "Test Case Failed ");
            test.AddScreenCaptureFromPath(path);
            Assert.Fail("Failed");
           
        }


    }
}
