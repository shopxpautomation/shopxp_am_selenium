﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;

namespace ShopXP_AM.Utility
{
    public class RestClientHelper
    {
        public RestClient _restClient;
        public RestRequest _restRequest;
        public string _baseUrl = ConfigurationManager.AppSettings["APIBaseUrl"].ToString();
        //public string _token = ConfigurationManager.AppSettings["Token"].ToString();
        public static string token_UserName = "None";
        public static string token_Password = "None";
        public static string Brand = "B2C_1_signin_signia_ropc";
        public static int firstTime = 0;
        public static string accessToken;
        public static string username;
        public static string password;
        public static string Brand_policy = "B2C_1_signin_signia_ropc";
        //B2C_1_ROPC

        public RestClient SetUrl(string resourceUrl)
        {
            var Url = Path.Combine(_baseUrl + resourceUrl);
            _restClient = new RestClient(Url);
            return _restClient;
        }

        public string getAccessToken_WSA_Time()
        {

            int getTime = Int32.Parse(DateTime.Now.ToString("HHmmss"));
            if (firstTime == 0)
            {
                firstTime = Int32.Parse(DateTime.Now.ToString("HHmmss"));
                accessToken = getAccessToken_WSA();
            }
            if ((getTime - firstTime > 500) || !username.Contains(token_UserName) || !password.Contains(token_Password) || !Brand.Contains(Brand_policy))
            {
                firstTime = Int32.Parse(DateTime.Now.ToString("HHmmss"));
                accessToken = getAccessToken_WSA();
            }
            Logger.LogPass("Access token "+accessToken);
            return accessToken;
        }
        public string getAccessToken_WSA()
        {
            string Token="";
            try
            {
                Token= getToken_WSA();
            }catch(Exception e1)
            {
                try
                {
                    Thread.Sleep(15000);
                    Token = getToken_WSA();
                }
                catch (Exception e2)
                {
                    Logger.LogFail("Access Token not generated");
                }
            }
            accessToken = Token;
            return Token;
        }
      
        public string getToken_WSA()
        {
            //B2C_1_ROPC
            Brand_policy =  Brand;
            
            string accessURL = "https://stgb2crexaad.b2clogin.com/stgb2crexaad.onmicrosoft.com/oauth2/v2.0/token?p="+ Brand_policy;
            string grant_type = "password";
            string scope = "openid offline_access";
            string client_id = "8a9ac541-4a5b-4a42-aa34-113aca609156";// "76563765-b14e-42fd-bfe9-609d931876b5";
            username = "system.admin@wsa.com";///"system.admin@wsa.com";// ConfigurationManager.AppSettings["APIUserName"].ToString(); fitxpadmin@wsa.com
            password = "Wsa@1234";// ConfigurationManager.AppSettings["APIPassword"].ToString(); 567
            if (!token_UserName.Contains("None"))
            {
                 username = token_UserName;
            }
            if (!token_Password.Contains("None"))
            {
                password = token_Password;
            }
            string response_type = "code id_token";

            var _restClient2 = new RestClient(accessURL);
            var _restRequest2 = new RestRequest(Method.POST);
            _restRequest2.AddParameter("application/x-www-form-urlencoded", $"grant_type={grant_type}&scope={scope}&client_id={client_id}&username={username}&password={password}&response_type={response_type}&p=B2C_1_ROPC", ParameterType.RequestBody);
           
            _restRequest2.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            IRestResponse response = _restClient2.Execute(_restRequest2);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                Console.WriteLine("Access Token cannot obtain, process terminate");
                Assert.Fail("Token not generated");
                //Logger.LogFail("Access Token not generated");
                return null;
            }
            var jsonObj = JObject.Parse(response.Content);
            string token = jsonObj.GetValue("id_token").ToString();
            //var tokenResponse = JsonConvert.DeserializeObject<TokenValidationResponse>(response.Content);
            //accessToken = token;
            return token;
        }
        public string getToken_WSA_Trial()
        {
            var client = new RestClient("https://stgb2crexaad.b2clogin.com/stgb2crexaad.onmicrosoft.com/oauth2/v2.0/token?p=B2C_1_ROPC");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            //request.AddHeader("Authorization", "0tq4gb7smqbocqizl6duiwisa9mnas8q");
            request.AddParameter("grant_type", "password");
            request.AddParameter("scope", "openid offline_access");
            request.AddParameter("client_id", "76563765-b14e-42fd-bfe9-609d931876b5");
            request.AddParameter("username", "system.admin@wsa.com");
            request.AddParameter("password", "Wsa@1234");
            request.AddParameter("response_type", "code id_token");
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            var jsonObj = JObject.Parse(response.Content);
            string token = jsonObj.GetValue("id_token").ToString();
            return token;
        }

        /*

        public RestRequest CreateRequest(string jsonString, Method method)
        {
            _restRequest = new RestRequest(method);
            _restRequest.AddHeader("Authorization", "Bearer " + getAccessToken_Widex());
            _restRequest.AddHeader("accept", "application/json");
            _restRequest.AddParameter("application/json", jsonString, ParameterType.RequestBody);
            return _restRequest;
        }

        public RestRequest create_Get_Delete_Request(Method method)
        {
            _restRequest = new RestRequest(method);
            _restRequest.AddHeader("Authorization", "Bearer " + getAccessToken_Widex());
            _restRequest.AddHeader("accept", "application/json");
            return _restRequest;
        }

        public RestRequest CreatePostRequest(string jsonString)
        {
            _restRequest = new RestRequest(Method.POST);
            _restRequest.AddHeader("Authorization", "Bearer " + getAccessToken_Widex());
            _restRequest.AddHeader("accept", "application/json");
            _restRequest.AddParameter("application/json", jsonString, ParameterType.RequestBody);
            return _restRequest;
        }

        /// <summary>
        /// Method for creating a PUT request using json payload information
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public RestRequest createPutRequest(string jsonString)
        {
            _restRequest = new RestRequest(Method.PUT);
            _restRequest.AddHeader("Authorization", "Bearer " + getAccessToken_Widex());
            _restRequest.AddHeader("accept", "application/json");
            _restRequest.AddParameter("application/json", jsonString, ParameterType.RequestBody);
            return _restRequest;
        }

        /// <summary>
        /// Method for creating a GET request
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public RestRequest createGetRequest()
        {
            _restRequest = new RestRequest(Method.GET);
            _restRequest.AddHeader("Authorization", "Bearer " + getAccessToken_Widex());
            _restRequest.AddHeader("accept", "application/json");
            return _restRequest;
        }

        /// <summary>
        /// Method for creating a DELETE request
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public RestRequest createDeleteRequest()
        {
            _restRequest = new RestRequest(Method.DELETE);
            _restRequest.AddHeader("Authorization", "Bearer " + getAccessToken_Widex());
            _restRequest.AddHeader("accept", "application/json");
            return _restRequest;
        }

        public IRestResponse GetResponse(RestClient restClient, RestRequest restRequest)
        {
            return restClient.Execute(restRequest);
        }

        public DTO GetContent<DTO>(IRestResponse response)
        {
            var content = response.Content;
            DTO deseiralizedObject = Newtonsoft.Json.JsonConvert.DeserializeObject<DTO>(content);
            return deseiralizedObject;
        }

        /// <summary>
        /// Method to get POST response 
        /// </summary>
        /// <param name="restClient"></param>
        /// <param name="request"></param>
        /// <returns>POST Response contents</returns>
        public IRestResponse getPostResponse(RestClient restClient, RestRequest request)
        {
            return restClient.Post(request);
        }

        /// <summary>
        /// Method to get PUT response
        /// </summary>
        /// <param name="restClient"></param>
        /// <param name="request"></param>
        /// <returns>PUT response contents</returns>
        public IRestResponse getPutResponse(RestClient restClient, RestRequest request)
        {
            return restClient.Put(request);
        }

        /// <summary>
        /// Method to get GET response
        /// </summary>
        /// <param name="restClient"></param>
        /// <param name="request"></param>
        /// <returns>GET response contents</returns>
        public IRestResponse getGETResponse(RestClient restClient, RestRequest request)
        {
            return restClient.Get(request);
        }

        /// <summary>
        /// Method to get DELETE response
        /// </summary>
        /// <param name="restClient"></param>
        /// <param name="request"></param>
        /// <returns>DELETE response contents</returns>
        public IRestResponse getDELETEResponse(RestClient restClient, RestRequest request)
        {
            return restClient.Delete(request);
        }
        */
    }


}

