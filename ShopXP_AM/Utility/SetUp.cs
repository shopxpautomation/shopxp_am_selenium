﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Text;

namespace ShopXP_AM.Utility
{
    [TestClass]
    public class SetUp
    {

        public static string strfunctionExecutionStatus = null;
        public static IWebDriver driver;
        //public static APILauncher launcher;
        public static int totalCount = 0;
        public static int passCount = 0;
        public static int failCount = 0;
        public static string CurrentRunFolderName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        public static string filePath = @"C:/" + ConfigurationManager.AppSettings["FolderName"] + "/" + Environment.MachineName + "/" + CurrentRunFolderName;
        public static string Snap_filePath = @"C:/" + ConfigurationManager.AppSettings["FolderName"] + "/" + Environment.MachineName + "/" + CurrentRunFolderName+"/Snapshot";

        public static string testName;
        public static string url = ConfigurationManager.AppSettings["ProductURL_IDS"];
        public static string Application = "IDS";

        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext Test)
        {
           
            Logger.initializeReport();
            Directory.CreateDirectory(Snap_filePath);
            //DataHandling.readDataCSV();
           
            
        }

        

        public static void initializeBrowser()
            {
            if (Application.Contains("IDS"))
            {
                url = ConfigurationManager.AppSettings["ProductURL_IDS"];

            }
            else if (Application.Contains("AM"))
            {
                url = ConfigurationManager.AppSettings["ProductURL_AM"];
            }
            string browserType = ConfigurationManager.AppSettings["Browser"];
            strfunctionExecutionStatus = null;
            totalCount++;
           //  launcher = new APILauncher(true);
          //  launcher.Start();
            switch (browserType)
            {
                case "chrome":
                    var options = new ChromeOptions();
                    options.AddAdditionalCapability("useAutomationExtension", false);
                    options.AddArgument("--start-maximized");
                    options.AddArgument("--disable-notifications");

                  //  driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), options);
                    driver = new ChromeDriver("C:\\Avengers\\ShopXP");
                    driver.Navigate().GoToUrl(url);
                    driver.Manage().Window.Maximize();
                    
                   
                    //Browserstack - 
                    /*ChromeOptions capability = new OpenQA.Selenium.Chrome.ChromeOptions();
                    capability.AddAdditionalCapability("os_version", "10", true);
                    capability.AddAdditionalCapability("resolution", "1920x1080", true);
                    capability.AddAdditionalCapability("browser", "Chrome", true);
                    capability.AddAdditionalCapability("browser_version", "latest", true);
                    capability.AddAdditionalCapability("os", "Windows", true);
                    capability.AddAdditionalCapability("browserstack.user", USERNAME, true);
                    capability.AddAdditionalCapability("browserstack.key", AUTOMATE_KEY, true);                    
                    driver = new RemoteWebDriver(new Uri("https://hub-cloud.browserstack.com/wd/hub/"), capability);
                    context["WEB_DRIVER"] = driver;
                    driver.Navigate().GoToUrl(Constant.ProductURL);
                    driver.Manage().Window.Maximize();*/
                    break;

                case "firefox":
                    var driverDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(driverDir, "geckodriver.exe");
                    service.FirefoxBinaryPath = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
                    service.HideCommandPromptWindow = true;
                    service.SuppressInitialDiagnosticInformation = true;
                    driver = new FirefoxDriver(service);
                    
                    driver.Manage().Window.Maximize();
                    driver.Navigate().GoToUrl(url);

                    //Browserstack
                    /*FirefoxOptions capability = new OpenQA.Selenium.Firefox.FirefoxOptions();
                    capability.AddAdditionalCapability("os_version", "10", true);
                    capability.AddAdditionalCapability("resolution", "1920x1080", true);
                    capability.AddAdditionalCapability("browser", "Firefox", true);
                    capability.AddAdditionalCapability("browser_version", "latest", true);
                    capability.AddAdditionalCapability("os", "Windows", true);
                    capability.AddAdditionalCapability("name", "Regression Test", true); // test name
                    capability.AddAdditionalCapability("build", "ShopXP Build Number 1", true); // CI/CD job or build name
                    capability.AddAdditionalCapability("browserstack.user", USERNAME, true);
                    capability.AddAdditionalCapability("browserstack.key", AUTOMATE_KEY, true);
                    driver = new RemoteWebDriver(new Uri("https://hub-cloud.browserstack.com/wd/hub/"), capability);
                    context["WEB_DRIVER"] = driver;
                    driver.Navigate().GoToUrl(url);
                    driver.Manage().Window.Maximize();*/
                    break;

                case "ie":
                    var ieOptions = new InternetExplorerOptions();
                    ieOptions.AddAdditionalCapability("useAutomationExtension", false);
                    driver = new InternetExplorerDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), ieOptions);
                   
                    driver.Manage().Window.Maximize();
                    driver.Navigate().GoToUrl(url);
                    break;

                case "edge":
                    var edgeOptions = new EdgeOptions();
                    driver = new EdgeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), edgeOptions);
                  
                    driver.Navigate().GoToUrl(url);
                    break;

                default:
                    break;
            }
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            
        }

       public static void NavigateToURL(string site)
        {
            string url2="";
            if (site.Contains("IDS"))
            {
                url2 = ConfigurationManager.AppSettings["ProductURL_IDS"];

            }
            else if (site.Contains("AM"))
            {
                url2 = ConfigurationManager.AppSettings["ProductURL_AM"];
            }
            driver.Navigate().GoToUrl(url2);

        }




       
        public static IWebDriver TestInitialize(string testname)
        {
            initializeBrowser();
            testName = testname;
            //testName = TestContext.TestName.ToString();
            Logger.CreateTest(testname);
            return driver;
        }

        
        public static void testCleanUp(string testResult)
        {
            string s = Logger.test.Status.ToString();
            //var testResult = TestContext.CurrentTestOutcome.ToString();
            try
            {
                if (Equals(testResult, "Failed") || (testResult == "Error") || (Logger.test.Status.ToString() == "Fail"))

                {
                    Logger.LogFailSnapShot(driver);
                    failCount++;
                }
                else
                {
                    passCount++;
                    Logger.LogPass("Test End with Pass");
                }
                driver.Quit();
            }
            catch(Exception e)
            {
                driver.Quit();
                Assert.Fail("Test Failed");
            }
            

            
        }


        [AssemblyCleanup]
        public static void tearDownReport()
        {
            Logger.extent.Flush();
            Utility.sendEMail();
            Utility.executeBatchFile("AM_Test.bat");
           // launcher.Stop();
            driver.Quit();
        }

    }
}
