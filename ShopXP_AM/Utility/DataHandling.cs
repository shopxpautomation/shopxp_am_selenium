﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;

namespace ShopXP_AM.Utility
{
    class DataHandling
    {

        public string DataFileName = ConfigurationManager.AppSettings["DataFileName"];
        public static string projectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName;
        public static Dictionary<String, String> dataList;
        public static void readDataCSV()
        {
            try
            {
                string fn = ConfigurationManager.AppSettings["DataFileName"];
                using (var reader = new StreamReader(@projectDirectory + "\\TestData\\" + fn))
                {

                    dataList = new Dictionary<String, String>();


                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');
                        dataList.Add(values[0], values[1]);
                    }
                }
            }catch(Exception e)
            {
                Assert.Fail("Failed read data csv file");
            }
            
        }

        public static string  get(string DataName)
        {
            return dataList[DataName];
        }



        }
}
