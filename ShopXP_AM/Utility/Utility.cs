﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using static ShopXP_AM.Utility.Logger;

namespace ShopXP_AM.Utility
{
    class Utility
    {
        public static string filePath = @"C:/" + ConfigurationManager.AppSettings["FolderName"] + "/" + Environment.MachineName + "/" + SetUp.CurrentRunFolderName;
        /*
        public static string getXMLData(string fileName, string searchKey, string environmentType)
        {
            string searchValue = string.Empty;
            if (!String.IsNullOrWhiteSpace(fileName) && !String.IsNullOrWhiteSpace(searchKey) && !String.IsNullOrWhiteSpace(environmentType))
            {
                string xmlPath = Directory.GetCurrentDirectory() + "\\" + fileName;
                if (File.Exists(xmlPath))
                {
                    var doc = XDocument.Load(xmlPath);
                    var result = doc.Descendants("Environment")
                                    .Where(e => ((String)e.Attribute("type"))
                                    .Equals(environmentType))
                                    .Select(z => z.Elements("Key")
                                    .Where(x => ((String)x.Attribute("id"))
                                    .Equals(searchKey, StringComparison.CurrentCultureIgnoreCase))
                                    .Select(y => (String)y.Attribute("value"))
                                    .FirstOrDefault());
                    if (result != null)
                        searchValue = result.ToList().FirstOrDefault();
                }
            }
            return searchValue;
        }
        */

        public static void sendEMail()
        {
            try
            {
                SmtpClient SmtpServer;
                List<MailAddress> to = new List<MailAddress>();
                string[] toList = ConfigurationManager.AppSettings["EmailIDs"].Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string emailId in toList)
                {
                    to.Add(new MailAddress(emailId));
                }

                MailMessage mail = new MailMessage();
                Attachment extentReport;
                string from = "vinay.kv.ext@wsa.com";
                mail.From = new MailAddress(from);
                mail.Body = "Dear Team, <br/> <br/> Our test suite has just finished its execution.<br/>Failure case's will be analyzed by us and will post out the observation. Please find the attachment for the detailed test run report and the results below. <br/>";
                if (SetUp.url.ToString().ToLower().Contains("uat"))
                {
                    mail.Body = "Dear Team, <br/> <br/> Our test suite has just finished its execution.<br/>Failure case's will be analyzed by us and will post out the observation. Please find the attachment for the detailed test run report and the results below. <br/><br/> And also, You can find the detailed test run report in the following link - https://uat-ecom-docservice.sivantos.com/eCommerceAutomationReports/UAT/ <br/><br/>";
                }

                if (SetUp.url.ToString().ToLower().Contains("pprod"))
                {
                    mail.Body = "Dear Team, <br/> <br/> Our test suite has just finished its execution.<br/>Failure case's will be analyzed by us and will post out the observation. Please find the attachment for the detailed test run report and the results below. <br/><br/> And also, You can find the detailed test run report in the following link - https://uat-ecom-docservice.sivantos.com/eCommerceAutomationReports/PProd/ <br/><br/>";
                }

                if (SetUp.url.ToString().ToLower().Contains("tst"))
                {
                    mail.Body = "Dear Team, <br/> <br/> Our test suite has just finished its execution.<br/>Failure case's will be analyzed by us and will post out the observation. Please find the attachment for the detailed test run report and the results below. <br/><br/> And also, You can find the detailed test run report in the following link - https://uat-ecom-docservice.sivantos.com/AccountManagementAutomationReports/Test/"+ SetUp.CurrentRunFolderName+"< br/><br/>";
                }

                if (SetUp.url.ToString().ToLower().Contains("https://oe"))
                {
                    mail.Body = "Dear Team, <br/> <br/> Our test suite has just finished its execution.<br/>Failure case's will be analyzed by us and will post out the observation. Please find the attachment for the detailed test run report and the results below. <br/><br/> And also, You can find the detailed test run report in the following link - https://uat-ecom-docservice.sivantos.com/eCommerceAutomationReports/Production/ <br/><br/>";
                }
                to.ForEach(entry => mail.To.Add(entry));
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.Subject = "Account Managment Automation Report : " + ConfigurationManager.AppSettings["Suite"] + " - " + DateTime.Now.ToShortDateString();
                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                StringBuilder builder = new StringBuilder();
                builder.AppendLine(Logger.startNode("table", "1"));
                builder.AppendLine(Logger.startNode("tr"));
                builder.AppendLine(Logger.getNode(TagType.th, "Result"));
                builder.AppendLine(Logger.getNode(TagType.th, "Count"));
                builder.AppendLine(Logger.closeNode("tr"));

                double rate = SetUp.passCount * 100 / SetUp.totalCount;

                builder.AppendLine(getRow("Passed", SetUp.passCount.ToString()));
                builder.AppendLine(getRow("Failed", SetUp.failCount.ToString()));
                builder.AppendLine(getRow("Total", SetUp.totalCount.ToString()));
                builder.AppendLine(getRow("Pass %", rate.ToString()));

                builder.AppendLine(Logger.closeNode("table"));
                mail.Body = mail.Body + builder.ToString();
                mail.IsBodyHtml = true;
                mail.Body = mail.Body + "<br/>This email was sent automatically. Please do not reply.<br/><br/>Regards,<br/>Automation Team.<br/> <br/>";

                if (File.Exists(filePath + "\\index.html"))
                {
                    extentReport = new Attachment(filePath + "\\index.html");
                    mail.Attachments.Add(extentReport);
                }
                else
                {
                    Console.WriteLine("ERROR!!!!! Report not found in the specified path...!");
                }

                SmtpServer = new SmtpClient("smtpgate-emea.audiology-solutions.net");
                SmtpServer.Send(mail);
                SmtpServer.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void executeBatchFile(string fileName)
        {
            Process process = null;
            try
            {
                string folderPath = @"C:/" + ConfigurationManager.AppSettings["FolderName"] + "/" + Environment.MachineName;
                process = new Process();
                process.StartInfo.WorkingDirectory = folderPath;
                process.StartInfo.FileName = fileName;
                process.StartInfo.CreateNoWindow = false;
                //https://github.com/dotnet/runtime/issues/28005
                process.StartInfo.UseShellExecute = true;
                process.Start();
                process.WaitForExit();
                process.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception Occurred :{0},{1}", ex.Message, ex.StackTrace.ToString());
            }
        }
    }
}
