﻿

using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using ShopXP_AM.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;
using Sikuli4Net.sikuli_UTIL;
using Sikuli4Net.sikuli_REST;

namespace ShopXP_AM
{
    
    public class SikuliOperations
    {
        IWebDriver driver;
        public WebDriverWait wait;
        IJavaScriptExecutor javaScriptExecutor;

        public static string projectDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName;
        string path = projectDirectory + "\\TestData\\SikuliImage\\";

        public void verifyImageExists(string image)
        {
            try
            {
                APILauncher launcher = new APILauncher(true);
                launcher.Start();
                Pattern img = new Pattern(@path+ "Capture.png");
                
                
                //Pattern img = new Pattern("D:/>ShopXP/SeleniumProject/ShopXP_AM/ShopXP_AM/TestData/SikuliImage/Capture.png");
                Screen src = new Screen();
                Thread.Sleep(10000);
                
                src.Click(img);
                src.Find(img, true);
                src.Exists(img, 60);
                Logger.LogPass(image + "Image dispalyed");
                launcher.Stop();
                
            }
            catch (Exception e)
            {
                
                Logger.LogFail("Failed: " + image + " Image not dispalyed");
            }
        }

             
    }
}