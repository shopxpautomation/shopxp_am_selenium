
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopXP_AM.Utility;
using System.Threading;
using System.Configuration;
using ShopXP_AM.PageOperations;
using OpenQA.Selenium;
using System;

namespace ShopXP_AM
{
    [TestClass]
    public class IDS_Regression_Tests: SetUp
    {
        public  TestContext TestContext { get; set; }
        IDSpageOperation Operation;
        IDS_Roles_pageOperation rolesOps;
        AM_User_pageOperation userOps;

         [TestInitialize]
        public void initialize()
        {
            string testname = TestContext.TestName.ToString();
            driver =TestInitialize(testname);
            Operation  = new IDSpageOperation(driver);
            rolesOps = new IDS_Roles_pageOperation(driver);
            userOps = new AM_User_pageOperation(driver);
            SetUp.Application = "IDS";
        }


        [TestCleanup]
        public void cleanup()
        {
            var testResult = TestContext.CurrentTestOutcome.ToString();
            testCleanUp(testResult);
        }


        [TestMethod]
        [TestCategory("Regression_IDS")]
        public void IDS_VerifyLoginPage()
        {
            Operation.verifyIDSLoginPage();
            Operation.verifyLanguageChange_IDSLoginPage();
            Operation.verifyWhenLoginFails();
            string User1 = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            userOps.createPendingUser("440001584", User1);
            Operation.Login("Automation_PendingUser_2202132858@wsa.com", "Access@123");
            Operation.Logou_IDS();
        }


        [TestMethod]
        [TestCategory("Regression_IDS")]
        public void IDS_VerifyApplicationTab()
        {
            Operation.Login();
            Operation.selectBrand("Widex");
            Operation.verify_ApplicationTab();
            Operation.addNewApplication("AUT_Test");
            Operation.CancelAddingNewApp();
            Operation.editApplication();
            Operation.AddSameApplication();
            Operation.deleteApplication("AUT_Test_Update");
        }

        [TestMethod]
        [TestCategory("Regression_IDS")]
        public void IDS_VerifyScopeForApplication()
        {
            try
            {
                Operation.Login();
                Operation.selectBrand("Widex");
                Operation.addNewApplication("AUT_Test");
                Operation.addScopeToApplication("Test_Scope", "AUT_Test");
                Operation.verifyScopeAddedToApplication("Test_Scope", "AUT_Test");
                Operation.deleteScope("Test_Scope");
                Operation.deleteApplication("AUT_Test");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Logger.LogFail("Failed due to Exception " + e.Message);
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        [TestCategory("Regression_IDS")]
        public void IDS_VerifyPermissionScopeTab()
        {
            try
            {
                Operation.Login();
                Operation.selectBrand("Widex");
                Operation.verify_ScopeTab();
                Operation.addNewApplication("AUT_Test");
                Operation.addScopeToApplication("TestScope", "AUT_Test");
                Operation.CancelAddingNewScope("AUT_Test");
                Operation.AddSameScope("TestScope", "AUT_Test");
                Operation.editScope("TestScope");
                Operation.deleteScope("TestScope_Updated");
                Operation.deleteApplication("AUT_Test");
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Logger.LogFail("Failed due to Exception "+e.Message);
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        [TestCategory("Regression_IDS")]
        public void IDS_VerifyStrategyTab()
        {
            try
            {
                Operation.Login();
                Operation.selectBrand("Widex");
                Operation.verify_StrategyTab();
                Operation.addNewApplication("AUT_Test");
                Operation.addScopeToApplication("TestScope", "AUT_Test");
                Operation.addNewStrategy("Test_Strategy", "AUT_Test");
                Operation.CancelAddingNewStrategy("AUT_Test");
                //Operation.AddSameStrategy("Test_Strategy", "AUT_Test");
                Operation.editStrategy("Test_Strategy");
                //Operation.Verify_deleteScopeWithStrategy("TestScope");
                Operation.deleteStrategy("TestStrategy_Updated");
                Operation.deleteScope("TestScope");
                Operation.deleteApplication("AUT_Test");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Logger.LogFail("Failed due to Exception " + e.Message);
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        [TestCategory("Regression_IDS")]
        public void IDS_VerifyUsersTab()
        {
            Operation.Login();
            Operation.selectBrand("Widex");
            Operation.verify_UserTab();
            string User = "User" + DateTime.Now.ToString("HHmmssff") + "@gmail.com";
            Operation.createUserValidation(User);
            //Operation.cancelAddingUser();
            Operation.deleteUser(User);
        }


        [TestMethod]
        [TestCategory("Regression_IDS")]
        public void IDS_VerifyCompanyTab()
        {
            Operation.Login();
            Operation.selectBrand("Widex");
            Operation.verify_CompanyTab();
            string CustomerNumber = "Customer1_" + DateTime.Now.ToString("DDHHmmssff");
            string CustomerNumber2 = "Customer2_" + DateTime.Now.ToString("DDHHmmssff");
            Operation.AddNewCompany(CustomerNumber);
            Operation.AddNewCompany(CustomerNumber2);
            Operation.CancelAddingNewCompany(CustomerNumber);
            Operation.editCompany(CustomerNumber);
            Operation.deleteComapany(CustomerNumber2);
            Operation.deleteComapany(CustomerNumber);
        }

        [TestMethod]
        [TestCategory("Regression_IDS")]
        public void IDS_VerifyTaskTab()
        {
            Operation.Login();
            Operation.selectBrand("Widex");
            Operation.verify_TaskTab();
           
        }

    }
}