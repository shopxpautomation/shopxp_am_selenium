
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopXP_AM.Utility;
using System.Threading;
using System.Configuration;
using ShopXP_AM.PageOperations;
using OpenQA.Selenium;
using System;

namespace ShopXP_AM
{
    [TestClass]
    public class IDS_Roles_Tests : SetUp
    {
        public  TestContext TestContext { get; set; }
        IDSpageOperation Operation;
        IDS_Roles_pageOperation rolesOps;

         [TestInitialize]
        public void initialize()
        {
            string testname = TestContext.TestName.ToString();
            driver =TestInitialize(testname);
            Operation  = new IDSpageOperation(driver);
            rolesOps = new IDS_Roles_pageOperation(driver);
            SetUp.Application = "IDS";
        }


        [TestCleanup]
        public void cleanup()
        {
            var testResult = TestContext.CurrentTestOutcome.ToString();
            testCleanUp(testResult);
        }


       

   




        [TestMethod]
        [TestCategory("Regression_IDS")]
        public void SAM30_IDS_ViewRoles()
        {
            Operation.Login();
            Operation.selectBrand("Widex");
            rolesOps.openRolesTab();
            //rolesOps.verifyRolePageNavigation();
            rolesOps.verifySearchRole();
        }


        [TestMethod]
        [TestCategory("Regression_IDS")]
        public void SAM31_IDS_CreateRole()
        {
            Operation.Login();
            Operation.selectBrand("Widex");
            rolesOps.openRolesTab();
            
            rolesOps.CreateRolePageValidation();
            string random = DateTime.Now.ToString("HHmmssff");
            string roleName = "Test_" + random;
            rolesOps.CreateNewRole(roleName,"Test Automation", "DAM Signia");
            rolesOps.SearchRole(roleName);
            rolesOps.VerifyCreatedNewRole(roleName, "Test Automation", "DAM Signia");
            //create role with existing role name having issue that needs to be created
        }

        [TestMethod]
        [TestCategory("Regression_IDS")]
        public void SAM32_33_IDS_EditAndDeleteRole()
        {
            Operation.Login();
            Operation.selectBrand("Widex");
            rolesOps.openRolesTab();
            string random = DateTime.Now.ToString("HHmmssff");
            string roleName = "Test_" + random;
            rolesOps.CreateNewRole(roleName, "Test Automation", "DAM Signia");
            rolesOps.SearchRole(roleName);
            rolesOps.VerifyEditRole(roleName, roleName + "_Updated", "Test Automation", "Test Automation updated");
            rolesOps.VerifyDeleteRole(roleName + "_Updated");
        }

        [TestMethod]
        [TestCategory("Regression_IDS")]
        public void SAM52_IDS_CreateUserWithRole()
        {
            Operation.Login();
            Operation.selectBrand("Widex");
            rolesOps.openUserTab();
            //verify Create User with role belongs to same application
            string User = "User" + DateTime.Now.ToString("HHmmssff") + "@gmail.com";
            Operation.createUserValidation(User);
        }

        [TestMethod]
        [TestCategory("Regression_IDS")]
        public void SAM235_IDS_Admin_HideCompany()
        {
            //Green Space
            Operation.Login();
            Operation.selectBrand("Widex");
            Operation.verifyCompanyTab(true,true);

            //Blue Space
            Operation.NavigateTestBlueSpace();
           
           // Operation.Login2("system.admin@widex.com", "Widex@1234");
           // Operation.verifyCompanyTab(true, true);
        }

        [TestMethod]
      //  [TestCategory("Regression_IDS")] //Ignore If Failed
        public void SAM235_IDS_LocalAdmin_HideCompany()
        {
            //Green Space
            Operation.Login("testmultipleCompany@gmail.com", "Access@123");
            Operation.selectBrand("Widex");
            Operation.verifyCompanyTab(false, false);

            //Blue Space
            Operation.NavigateTestBlueSpace();
            Operation.Login2("localadmin.india@widex.com", "Access@123");
            Operation.verifyCompanyTab(true, false);

        }

        [TestMethod]
        [TestCategory("Regression_IDS")] //Ignore If Failed
        public void SAM4_IDS_DontShowClinicData()
        {
            driver.Navigate().GoToUrl("https://tst-id.widex.com");
            Thread.Sleep(8000);
            Operation.validateClinicData();
        }

    }
}