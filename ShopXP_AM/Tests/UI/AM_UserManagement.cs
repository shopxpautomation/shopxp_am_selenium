
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopXP_AM.Utility;
using System.Threading;
using System.Configuration;
using ShopXP_AM.PageOperations;
using OpenQA.Selenium;
using System;

namespace ShopXP_AM
{
    [TestClass]
    public class AM_UserManagement : SetUp
    {
        public  TestContext TestContext { get; set; }
        IDSpageOperation Operation;
        AM_User_pageOperation userOps;
        PageOperationBase action;

         [TestInitialize]
        public void initialize()
        {
            SetUp.Application = "AM";
            string testname = TestContext.TestName.ToString();
            driver =TestInitialize(testname);
            Operation  = new IDSpageOperation(driver);
            userOps = new AM_User_pageOperation(driver);
            action = new PageOperationBase(driver);
        }
        [TestCleanup]
        public void cleanup()
        {
            var testResult = TestContext.CurrentTestOutcome.ToString();
            testCleanUp(testResult);
        }
        

        [TestMethod]
        [TestCategory("Regression_AM")]
        public void SAM105_AM_ListofCompanyUser()
        {
            string adminWithMultipleUsers = "Automation_Admin_0301140320@wsa.com";
            Operation.Login_AM(adminWithMultipleUsers, "Access@123");
            userOps.verifyManageUserPage();
            userOps.verifyUserSearch(adminWithMultipleUsers);
            userOps.verifyUserFilter();
            userOps.userListOrderingVerification();
            //userOps.verifyPanigationInUserList();
        }

        [TestMethod]
        [TestCategory("Regression_AM")]
        public void SAM24_AM_AvaialbleAddress_Multiple()
        {
            string adminWithMultipleUsers = "testmultipleCompany@gmail.com";
            Operation.Login_AM(adminWithMultipleUsers, "Access@123");
            userOps.verifyAvaialbeAddress("Multiple");
        }

        [TestMethod]
        [TestCategory("Regression_AM")]
        public void SAM24_AM_AvaialbleAddress_Single()
        {
            string adminWithMultipleUsers = "Automation_Admin_0301140533@wsa.com";
            Operation.Login_AM(adminWithMultipleUsers, "Access@123");
            userOps.verifyAvaialbeAddress("Single");
        }

        [TestMethod]
        [TestCategory("Regression_AM")]
        public void SAM21_AM_CreateUser()
        {
            string user = "Automation_UK_Admin1@wsa.com";
            Operation.Login_AM(user, "Access@123");
            userOps.verifyCheckEmailPopUp();
            userOps.verifyCreateUserFlow();

            string user1 = "Automation_Admin_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            userOps.createUser(user1,"Active", "Default");
            userOps.verifyUserExist(user1, "Active");
            userOps.clearSearchInUserGrid();
           // userOps.verifyUserExist(user1, "");
            //userOps.DeleteUser(user1);

            string user2 = "Automation_Admin_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            userOps.createUser(user2, "InActive", "Default");
            userOps.verifyUserExist(user2, "");
           // userOps.DeleteUser(user2);
        }

        [TestMethod]
        [TestCategory("Regression_AM")]
        public void SAM22_AM_DeleteUser()
        {
            string User = "Automation_Admin_0301140533@wsa.com";
            Operation.Login_AM(User, "Access@123");
            string user1 = "Automation_Admin_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            userOps.createUser(user1, "Active", "Business Admin");
            userOps.verifyUserExist(user1, "Active");
            userOps.verifyDeleteUser(user1);
            userOps.DeleteUser(user1);
        }

        [TestMethod]
        [TestCategory("Regression_AM")]
        public void SAM20_AM_EditUser()
        {
            string User = "Automation_Admin_0301140320@wsa.com";
            Operation.Login_AM(User, "Access@123");
           
            string user1 = "Automation_Admin_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            userOps.createUser(user1, "Active", "Business Admin");
            userOps.verifyUserExist(user1, "Active");
            userOps.verifyEditUserPage(user1);
            userOps.verifyUserExist(user1, "Active");
            userOps.editUserAndVerify(user1, "Active", "Business Admin");
            userOps.verifyUserExist(user1, "");
            userOps.DeleteUser(user1);
        }

        [TestMethod]
        [TestCategory("Regression_AM")]
        public void SAM100_AM_VerifyApproveUser()
        {
            try
            {
                Logger.LogInfo("Test Appove User ");

                SetUp.NavigateToURL("AM");
                string User1 = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
                userOps.createPendingUser("64735634", User1);
                SetUp.NavigateToURL("AM");
                string User2 = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
                userOps.createPendingUser("64735634", User2);
                SetUp.NavigateToURL("AM");
                Operation.Login_AM("Automation_UK_Admin1@wsa.com", "Access@123");
                userOps.verifyApproveUserFlow(User1);
                userOps.approveUser(User1, "Active");
                userOps.approveUser(User2, "Inactive");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.InnerException);
                Logger.LogFail("Failed due to Exception " + e.Message);
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        //[TestCategory("Regression_AM")]
        public void SAM100_AM_VerifyAlreadyApprovedUser()
        {
            Logger.LogInfo("Test Appove already Approved User");

            SetUp.NavigateToURL("AM");
            string User1 = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            userOps.createPendingUser("64735634", User1);
           
            SetUp.NavigateToURL("AM");
            Operation.Login_AM("Automation_UK_Admin1@wsa.com", "Access@123");
            userOps.verifyApproveUserFlow(User1);

            action.openNewWindow();
            SetUp.NavigateToURL("AM");
            //userOps.approveUser(User1, "Active");
            //userOps.verifyAlreadyProcesedUser();
        }


        [TestMethod]
        [TestCategory("Regression_AM")]
        public void SAM38_AM_VerifyDeclineUser()
        {
            Logger.LogInfo("Test Decline User ");
            SetUp.NavigateToURL("AM");
            string User1 = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            userOps.createPendingUser("64735634", User1);
           
            SetUp.NavigateToURL("AM");
            Operation.Login_AM("Automation_UK_Admin1@wsa.com", "Access@123");
            userOps.verifyApproveUserFlow(User1);
            userOps.declineUser(User1);
        }

        [TestMethod]
       // [TestCategory("Regression_AM")] //Ignore If Failed
        public void SAM38_AM_VerifyAlreadyDeclinedUser()
        {
            Logger.LogInfo("Test Decline Already declined User ");
            SetUp.NavigateToURL("AM");
            string User1 = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            userOps.createPendingUser("64735634", User1);

            SetUp.NavigateToURL("AM");
            Operation.Login_AM("Automation_UK_Admin1@wsa.com", "Access@123");
            userOps.verifyApproveUserFlow(User1);
            Thread.Sleep(5000);
            action.openNewWindow();
            SetUp.NavigateToURL("AM");
            //userOps.declineUser(User1);
            //userOps.verifyAlreadyProcesedUser();
        }

        [TestMethod]
        [TestCategory("Regression_AM")]
        public void SAM267_AM_OnlyRolesbelongsToApplication()
        {
            string adminWithMultipleUsers = "Automation_Admin_0301140320@wsa.com";
            Operation.Login_AM(adminWithMultipleUsers, "Access@123");
            userOps.verifyRolesInManageUsers();
        }

        }
}