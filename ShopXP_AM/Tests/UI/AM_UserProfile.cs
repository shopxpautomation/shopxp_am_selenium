
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopXP_AM.Utility;
using System.Threading;
using System.Configuration;
using ShopXP_AM.PageOperations;
using OpenQA.Selenium;
using System;

namespace ShopXP_AM
{
    [TestClass]
    public class AM_UserProfile : SetUp
    {
        public  TestContext TestContext { get; set; }
        IDSpageOperation Operation;
        AM_User_pageOperation userOps;
        AM_Profile_pageOperation profile;

         [TestInitialize]
        public void initialize()
        {
            SetUp.Application = "AM";
            string testname = TestContext.TestName.ToString();
            driver =TestInitialize(testname);
            Operation  = new IDSpageOperation(driver);
            userOps = new AM_User_pageOperation(driver);
            profile = new AM_Profile_pageOperation(driver);
        }
        [TestCleanup]
        public void cleanup()
        {
            var testResult = TestContext.CurrentTestOutcome.ToString();
            testCleanUp(testResult);
        }
        


        [TestMethod]
        [TestCategory("Regression_AM")]
        public void SAM25_AM_ViewUserProfile()
        {
            string User = "testmultipleCompany@gmail.com";
            Operation.Login_AM(User, "Access@123");
            profile.navigateToUserProfile();
            profile.verifyProfilePage();
        }

        [TestMethod]
        [TestCategory("Regression_AM")]
        public void SAM25_AM_EditUserProfile()
        {
            string User = "Automation_UK_Admin2@wsa.com";
            Operation.Login_AM(User, "Access@123");
            profile.navigateToUserProfile();
            profile.editProfilePage();
        }

        [TestMethod]
        [TestCategory("Regression_AM")]
        public void SAM6_AM_UpdatePasswordProfile()
        {
            string User = "Automation_User_0701093207@wsa.com";
            Operation.Login_AM(User, "Access@123");
            profile.navigateToUserProfile();
         
            profile.verifyChangePasswordProfile();
            profile.changePasswordProfile();
        }


        [TestMethod]
        [TestCategory("Regression_AM")]
        public void SAM57_AM_AccessDeniedPage()
        {
            string User = "Automation_PendingUser_2202132858@wsa.com";
            Operation.Login_AM(User, "Access@123");
            profile.verifyAccessDenied();
        }

    }
}