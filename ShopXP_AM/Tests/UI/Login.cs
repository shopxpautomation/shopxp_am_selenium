
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopXP_AM.Utility;
using System.Threading;
using System.Configuration;
using ShopXP_AM.PageOperations;
using OpenQA.Selenium;
using System.Collections.Generic;
using System;
using System.Collections;

namespace ShopXP_AM
{
    [TestClass]
    public class Login : SetUp
    {
        public  TestContext TestContext { get; set; }
        IDSpageOperation Operation;
       
        [TestMethod]
        public void CreateCompany()
        {

            reverseUsingStack();
        }





        public static void reverseUsingStack()
        {
            string name = "asfsdfsdfs";
            Stack<char> store = new Stack<char>();
            foreach (char nam in name)
            {
               
                store.Push(nam);
            }
            //char[] rev = new char[name.Length];
            string rev = "";
            foreach(char nam2 in store)
            {
                rev = rev + nam2;
            }


        }

        public static void fibonociSeries()
        {
            int n1 = 0, n2 = 1, n3, i, number;
            Console.Write("Enter the number of elements: ");
            number = int.Parse(Console.ReadLine());
            Console.Write(n1 + " " + n2 + " "); //printing 0 and 1    
            for (i = 2; i < number; ++i) //loop starts from 2 because 0 and 1 are already printed    
            {
                n3 = n1 + n2;
                Console.Write(n3 + " ");
                n1 = n2;
                n2 = n3;
            }
        }

        public void removepairIntegers()
        {
            /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution */

            string list = Console.ReadLine();
            list.Replace(',', ' ');
            string[] listArray = list.Split(' ');
            List<int> numList = new List<int>();
            int k = 0;
            foreach (string n in listArray)
            {
                numList[k]= Int32.Parse(n);
                k++;
            }

            for (int i = 0; i < numList.Count; i++)
            {
                for (int j = i; j < numList.Count; j++)
                {

                    if (numList[i] == numList[j])
                    {
                        numList.Remove(i);
                        numList.Remove(j);
                    }
                }
            }

        }


        //
        public static void reverseByN()
        {
            int[] A = { 12, 23, 1, 13, 13, 44, 1, 2 };
            int k = 2;


            for (int i = 0; i < A.Length; i += k)
            {
                int left = i;
                // in case right larger than A.length
                int right = Math.Min(i + k - 1, A.Length - 1);

                // reverse sub array
                while (left < right)
                {
                    int temp = A[left];
                    A[left] = A[right];
                    A[right] = temp;

                    left++;
                    right--;
                }
            }
        }




        //Given a number in an array form. Write a program to move all zeros to the end.

        public static void moveAllZeroAtTheEnd()
        {
            int[] num = { -1, 0, 0, 2, -5, 1, 0, 9, -8 };
            int j = num.Length - 1;
            int k = 0;
            int[] newNum = new int[num.Length];
            for (int i = 0; i < num.Length; i++)
            {
                if (num[i] == 0)
                {
                    newNum[j] = num[i];
                    j--;
                }
                else
                {
                    newNum[k] = num[i];
                   k++;
                }
            }
            Console.WriteLine(newNum.ToString());

        }




        //In a given string containing 1s and 0s, find the number of 1s.
        public static void find1Sand0s()
        {
            string num = "10111011101000";
            char[] cnum = num.ToCharArray();
            int zeroCount = 0;
            int onecount = 0;
            foreach(char n in cnum)
            {
                if (n.ToString().Contains("0"))
                {
                    zeroCount++;
                }else
                {
                    onecount++;
                }
            }
        }

        //Write a code to convert a string such as aaabbccdaa to a3b2c2d1a2.
        public static void countofcharInString()
        {
            string name = "VianyaaPoojashree";
            char[] cname = name.ToCharArray();
            Dictionary<string, int> words = new Dictionary<string, int>();
            foreach(char ch in cname)
            {
                if (words.ContainsKey(ch.ToString()))
                {
                    int count = words[ch.ToString()];
                    words[ch.ToString()]= count + 1;
                }else
                {
                    words.Add(ch.ToString(),  1);
                }
            }
            Console.WriteLine(words);
        }





        //Write a code to reverse a string without changing the position of the special characters.

        public static void reverseStringWithoutChangingspecial()
        {
            string name = "Viany@Poojashree";
            char[] cname = name.ToCharArray();
            for (int i = 0, j = name.Length - 1; i < j; i++, j--)
            {
                if (!char.IsLetter(name[i]))
                    i++;
                else if (!char.IsLetter(name[j]))
                    j--;
                else
                {
                    char c = cname[i];
                    cname[i] = cname[j];
                    cname[j] = c;
                }
                
            }
            string rev = new string(cname);
        }

        //find prime number  in list
        public static void findPrimeNumber()
        {
            //int num = 623520;
            int[] nums = { 23, 42, 13, 11, 65, 33, 20 };
            foreach(int num in nums)
            {
                int num2 = num;
                bool prime = true;
                for (int i = 2; i < num / 2; i++)
                {
                    int rem = num % i;
                    if (rem == 0)
                    {
                        prime = false;
                        break;
                    }
                }
                if (prime == true)
                {
                    Console.WriteLine(num2);
                }
            }

            

        }




        //Write a code to group the positive and negative number
        public static void groupositiveAndNegative()
        {
            int[] num = { -1, -3, -4, 2, -5, 1, -5, 9, -8 };
            int j = num.Length-1;
            int k = 0;
            int[] newNum = new int[num.Length];
            for (int i = 0; i < num.Length; i++)
            {
                if (num[i] < 0)
                {

                    newNum[k] = num[i];
                    k++;
                }
                else{
                    newNum[j] = num[i];
                    j--;
                }
            }
            Console.WriteLine(newNum.ToString());

        }



        //find second largest in array 

        public static void secondLargeInArray()
        {
            int[] nums = { 110, 120, 134, 122, 67, 342, 10 };

            int largest = nums[0];
            int secondLArgest = 0;
            int thrirdlarge = 0;
            for (int i = 1; i < nums.Length; i++)
            {
                if (largest < nums[i])
                {
                    thrirdlarge = secondLArgest;
                    secondLArgest = largest;
                    largest = nums[i];
                    
                }else
                {
                    if (secondLArgest < nums[i])
                    {
                        thrirdlarge = secondLArgest;
                        secondLArgest = nums[i];
                    }else
                    {
                        if (thrirdlarge < nums[i])
                        {
                            thrirdlarge = nums[i];
                        }
                    }
                }
            }
            int fir = largest;
            int sec = secondLArgest;
            int third = thrirdlarge;
        }


        //Given a sentence as a string, reverse each of the words without using inbuilt functions.

        public static void reverseEachWord()
        {
            string text = "I went to Bangalore";
            string[] word = text.Split(' ');
            for(int k = 0; k < word.Length; k++)
            {
                char[] chars = word[k].ToCharArray();
               
                for (int i = 0, j = chars.Length - 1; i < j; i++, j--)
                {
                    char c = chars[i];
                    chars[i] = chars[j];
                    chars[j] = c;
                }
                Console.WriteLine(chars.ToString() + " ");
            }
        }







        //Check if the parentheses are balanced in the given string.

        public static void parenthesesBalenced()
        {
            string name = "v(asdex(s(ss)a)AA)";
            int countOpen = 0;
            int countClose = 0;
            foreach(char par in name)
            {
                if (par.ToString().Contains("("))
                {
                     countOpen++;
                }
                if (par.ToString().Contains(")"))
                {
                    countClose++;
                }
            }
            if (countClose == countOpen)
            {
                Console.WriteLine("Balenced");
            }
            else{
                Console.WriteLine("Not Balenced");
            }

        }



        //Given two strings, find their union.
        public static void twoStringUnion()
        {
            string name1 = "vinay";
            string name2 = "Pooja";
           
            HashSet<char> join = new HashSet<char>();
            foreach(char n1 in name1)
            {
                join.Add(n1);
            }
            foreach (char n2 in name2)
            {
                join.Add(n2);
            }
            string union = join.ToString();

        }




        //Given a string, remove the consecutively repeated characters. For example - aabbbcabcbb to cabc.
        public static void removeRepeatedCharInString()
        {
            string name = "aabbbcabcbb";
            string newName = "";
            foreach(char text in name)
            {
                if (!newName.Contains(text.ToString()))
                {
                    newName = newName +text.ToString();
                }

            }
            Console.WriteLine(newName);

        }



        //Convert the given binary number into decimal.
        public static void binaryToDecimal()
        {
            int num = 1211;
            int dec_value = 0;

            // Initializing base1
            // value to 1, i.e 2^0
            int base1 = 1;

            int temp = num;
            while (temp > 0)
            {
                int last_digit = temp % 10;
                temp = temp / 10;

                dec_value += last_digit * base1;

                base1 = base1 * 2;
            }
    }



        //Given an integer, check whether it is a palindrome
        public static void checkIntPolindrom()
        {
            int num = 14541;
            int num2 = num;
            
            int rev = 0;
            while (num2> 0)
            {
               int rem = num2 % 10;
                rev = rev * 10 + rem;
                num2 = num2 / 10;
            }
            if (num == rev)
            {
            }

        }

        //Given an array, subtract a number M from every odd index and N from every even index. 
        public static void arrayIndex()
        {
            int[] num = { 1, 2, 6, 2, 3, 8, 9 };
            int[] newArr = new int[num.Length];
            int M = 2;
            int N = 3;
            for (int i = 0; i < num.Length; i=i+2)
            { 
                    newArr[i] = num[i] - N;
            }

            for (int i = 1; i < num.Length; i = i + 2)
            {
                newArr[i] = num[i] - M;
            }
            newArr.ToString();
            
        }



        //Find all magic triplets in a given integer array �arr� of size n. A magic triplet is a group of three numbers whose sum is zero. Note that magic triplets may or may not be made of consecutive numbers in arr
        public static void zero3subarray()
        {
            int[] arr = { 1, 2, 4, 6, -1, 2, -4, 3 };
            int[] answer = new int[3];
            // Sorting is only necessary for avoiding duplicates in the answer.
            Array.Sort(arr);

            int n = arr.Length;
            for (int index_1 = 0; index_1 < n; index_1++)
            {
                for (int index_2 = index_1 + 1; index_2 < n; index_2++)
                {
                    for (int index_3 = index_2 + 1; index_3 < n; index_3++)
                    {
                        int sum = arr[index_1] + arr[index_2] + arr[index_3];
                        if (sum == 0)
                        {
                            answer[0] = arr[index_1];
                            answer[1] = arr[index_2];
                            answer[2] = arr[index_3];
                            
                        }
                    }
                }
            }
            answer.ToString();
        }


       // Given K sorted arrays �arr�, of size N each, merge them into a new sorted array �res.
        public static void sortKsizeArray()
        {
            int[,] num = { { 2, 6, 12, 34 },
                  { 1, 9, 20, 1000 },
                  { 23, 34, 90, 2000 } };
            int K = 4;
            int n = 3;
            int l = 0;
            int[] output = new int[K * n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < K; j++)
                {
                    output[l] = num[i, j];
                    l++;
                }
            }
            System.Array.Sort(output);
        }


        //Given an array of integers, find any non-empty subarray whose elements add up to zero
        public static void zeroSubArray()
        {
            int[] num = { 1, 2, 4, -4, 1, 2 };
            int[] res = new int[2];
            int sum = 0;

            for (int i = 0; i < num.Length; i++)
            {
                sum = 0;
                for (int j = i; j < num.Length; j++)
                {
                    sum += num[j];
                    if (sum == 0)
                    {
                        res[0] = i;
                        res[1] = j;
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            int[] arr = { 78, 55, 45, 98, 13 };
            int temp;
            for (int j = 0; j <= arr.Length - 2; j++)
            {
                for (int i = 0; i <= arr.Length - 2; i++)
                {
                    if (arr[i] > arr[i + 1])
                    {
                        temp = arr[i + 1];
                        arr[i + 1] = arr[i];
                        arr[i] = temp;
                    }
                }
            }
            Console.WriteLine("Sorted:");
            foreach (int p in arr)
                Console.Write(p + " ");
            Console.Read();
        }





    }
}