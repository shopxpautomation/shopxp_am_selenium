
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopXP_AM.Utility;
using System.Threading;
using System.Configuration;
using ShopXP_AM.PageOperations;
using OpenQA.Selenium;
using System.Net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace ShopXP_AM
{
    [TestClass]
    public class IDS_ExternalAPI : SetUp
    {
        public TestContext TestContext { get; set; }
        IDS_API_External_GET externalAPI;
        IDS_API_CreateAndUpdateUser userAPI;
        PageOperationBase action;
        IDSpageOperation Operation;
        AM_User_pageOperation AMUser;

        [TestInitialize]
        public void initialize()
        {
            string testname = TestContext.TestName.ToString();
            driver = TestInitialize(testname);
            externalAPI = new IDS_API_External_GET();
            action = new PageOperationBase(driver);
            userAPI = new IDS_API_CreateAndUpdateUser();
            Operation = new IDSpageOperation(driver);
            AMUser = new AM_User_pageOperation(driver);

            RestClientHelper.token_UserName = "None";
            RestClientHelper.token_Password = "None";
            IDS_API_External_GET.newToken = true;
            RestClientHelper.Brand = "B2C_1_ROPC";
            SetUp.Application = "IDS";
        }
        [TestCleanup]
        public void cleanup()
        {
            var testResult = TestContext.CurrentTestOutcome.ToString();
            testCleanUp(testResult);
        }


        [TestMethod]
        [TestCategory("API")]
        public void SAM46_API_getShippingAddress_Postive()
        {
            string singleChildCompanyID = "6761f269-0d1d-4ed9-83f7-fa7f4b1909c8";
            string multipleChildCompanyID = "3490e7c0-75dc-431d-9256-6b8e2f2f642c";

            Logger.LogPass("Get Shipping Address API");

            IDS_API_External_GET.newToken = true;
            RestClientHelper.token_UserName = "India.localadmin.ext@wsa.com";
            RestClientHelper.token_Password = "Access@123";
            RestClientHelper.Brand = "B2C_1_signin_signia_ropc";
            var singleChild = externalAPI.getShippingAddress("0", "4", "");
            //action.AssertEqual(singleChild.GetValue("Count").ToString(), "7");
            //action.AssertEqual(singleChild.GetValue("Taken").ToString(), "4");
            Assert.IsTrue(singleChild.ToString().Contains("Id"));
            Assert.IsTrue(singleChild.ToString().Contains("Name"));
            Assert.IsTrue(singleChild.ToString().Contains("LocalCustomerNumber"));
            Assert.IsTrue(singleChild.ToString().Contains("ShippingAddress"));
            Assert.IsTrue(singleChild.ToString().Contains("City"));
            Assert.IsTrue(singleChild.ToString().Contains("State"));
            Assert.IsTrue(singleChild.ToString().Contains("ZipCode"));
            Assert.IsTrue(singleChild.ToString().Contains("Country"));


            IDS_API_External_GET.newToken = true;
            RestClientHelper.token_UserName = "testmultipleCompany@gmail.com";
            RestClientHelper.token_Password = "Access@123";
            var MultipleChild = externalAPI.getShippingAddress("3", "20", "");
            action.isTrue(MultipleChild.ContainsKey("Count"), "Users Count " + MultipleChild.GetValue("Count"));
            action.AssertEqual(MultipleChild.GetValue("Taken").ToString(), "20");
            action.AssertEqual(MultipleChild.GetValue("Skipped").ToString(), "3");

            var SearchName = externalAPI.getShippingAddress("3", "20", "Comapny Child Automation 1");
            action.AssertEqual(SearchName.GetValue("Count").ToString(), "17");
            action.AssertEqual(SearchName.GetValue("Taken").ToString(), "14");
            action.AssertEqual(SearchName.GetValue("Skipped").ToString(), "3");

            var SearchCustomerNumber = externalAPI.getShippingAddress("0", "20", "14585466");
            action.AssertEqual(SearchCustomerNumber.GetValue("Count").ToString(), "1");

            var SearchCity = externalAPI.getShippingAddress("0", "20", "Bnagalore");
            action.isTrue(SearchCity.ContainsKey("Count"), "Users Count " + SearchCity.GetValue("Count"));

        }

        [TestMethod]
        [TestCategory("API")]
        public void SAM46_API_getShippingAddress_Negative()
        {


            Logger.LogPass("Get Roles by AppID API");
            RestClientHelper.token_UserName = "testmultipleCompany@gmail.com";
            RestClientHelper.token_Password = "Access@123";

            var response2 = externalAPI.getShippingAddress_Negative("-2", "4");
            action.isTrue(response2.StatusCode.Equals(HttpStatusCode.BadRequest), "Error mesage displayed as expected for wrong application ID");

            var response3 = externalAPI.getShippingAddress_Negative("2", "-4");
            action.isTrue(response3.StatusCode.Equals(HttpStatusCode.BadRequest), "Error mesage displayed as expected for wrong application ID");

            var response4 = externalAPI.getShippingAddress_Negative("2", "2050");
            action.isTrue(response4.StatusCode.Equals(HttpStatusCode.BadRequest), "Error mesage displayed as expected for wrong application ID");
        }

        [TestMethod]
        [TestCategory("API")]
        public void SAM101_API_getRolesByAppID_Positive()
        {
            string AppID1 = "d09257a8-7a67-46a5-8323-8a38f36d8451";
            string AppID2 = "512f28b5-fc51-408e-9236-1193c196ecf7";

            Logger.LogPass("Get Roles by AppID API");
            var response1 = externalAPI.getRolesByAppID(AppID1, "0", "4");
            action.isTrue(response1.ContainsKey("Count"), "Roles count " + response1.GetValue("Count"));
            action.AssertEqual(response1.GetValue("Taken").ToString(), "4");
            Assert.IsTrue(response1.ToString().Contains("Id"));
            Assert.IsTrue(response1.ToString().Contains("Name"));
            Assert.IsTrue(response1.ToString().Contains("Description"));
            Assert.IsTrue(response1.ToString().Contains("ApplicationId"));
            Assert.IsTrue(response1.ToString().Contains("ScopeId"));
            Assert.IsTrue(response1.ToString().Contains("Alias"));
            Assert.IsTrue(response1.ToString().Contains("IsDefault"));
            Assert.IsTrue(response1.ToString().Contains("ScopeName"));

            var response2 = externalAPI.getRolesByAppID(AppID2, "0", "5");
            action.isTrue(response2.ContainsKey("Count"), "Users Count " + response2.GetValue("Count"));
            action.AssertEqual(response2.GetValue("Taken").ToString(), "5");

        }

        [TestMethod]
        [TestCategory("API")]
        public void SAM101_API_getRolesByAppID_Negative()
        {
            string WrongAppID = "e6c369d3-1bd0-4831-82d9-fsftw345351";
            string AppID2 = "42c640d1-d392-4142-b04a-251a86f891e1";
            string AppID3 = "";

            Logger.LogPass("Get Roles by AppID API");
            var response1 = externalAPI.getRolesByAppID_Negative(WrongAppID, "0", "4");
            action.isTrue(response1.StatusCode.Equals(HttpStatusCode.BadRequest), "Error mesage displayed as expected for wrong application ID");

            var response2 = externalAPI.getRolesByAppID_Negative(AppID2, "-2", "4");
            action.isTrue(response2.StatusCode.Equals(HttpStatusCode.BadRequest), "Error mesage displayed as expected for wrong application ID");

            var response3 = externalAPI.getRolesByAppID_Negative(AppID2, "2", "-4");
            action.isTrue(response3.StatusCode.Equals(HttpStatusCode.BadRequest), "Error mesage displayed as expected for wrong application ID");

            var response4 = externalAPI.getRolesByAppID_Negative(AppID2, "2", "2050");
            action.isTrue(response4.StatusCode.Equals(HttpStatusCode.BadRequest), "Error mesage displayed as expected for wrong application ID");

            var response5 = externalAPI.getRolesByAppID_Negative(AppID3, "0", "4");
            action.isTrue(response1.StatusCode.Equals(HttpStatusCode.BadRequest), "Error mesage displayed as expected for wrong application ID");
        }

        [TestMethod]
        [TestCategory("API")]
        public void SAM101_API_getUserByEmail_Positive()
        {
            string email1 = "vinay.kv2.ext@wsa.com";
            string email2 = "India.localadmin.ext@wsa.com";
            string appId1 = "d09257a8-7a67-46a5-8323-8a38f36d8451";
            string appId2 = "506339d5-9095-412a-bf6e-9107eaf05846";

            Logger.LogPass("Get User by Email");
            var response1 = externalAPI.getUserByEmail(email1, "");
            action.AssertEqual(response1.GetValue("Email").ToString(), email1);
            JArray items1 = (JArray)response1["Scopes"];
            action.AssertEqual(items1.Count, 6);
            Assert.IsTrue(response1.ToString().Contains("Id"));
            Assert.IsTrue(response1.ToString().Contains("Email"));
            Assert.IsTrue(response1.ToString().Contains("GivenName"));
            Assert.IsTrue(response1.ToString().Contains("SurName"));
            Assert.IsTrue(response1.ToString().Contains("Country"));
            Assert.IsTrue(response1.ToString().Contains("Language"));
            Assert.IsTrue(response1.ToString().Contains("Active"));
            Assert.IsTrue(response1.ToString().Contains("Company"));
            Assert.IsTrue(response1.ToString().Contains("Scopes"));
            Assert.IsTrue(response1.ToString().Contains("Brands"));


            var response2 = externalAPI.getUserByEmail(email1, appId1);
            action.AssertEqual(response2.GetValue("Email").ToString(), email1);
            JArray items2 = (JArray)response2["Scopes"];
            action.AssertEqual(4, items2.Count);
           

            var response3 = externalAPI.getUserByEmail(email1, appId2);
            action.AssertEqual(response3.GetValue("Email").ToString(), email1);
            Assert.IsFalse(response3.ContainsKey("Scopes"));


            string userID = response2.GetValue("Id").ToString();
            var response5 = externalAPI.getUserByEmail(userID,"");
            action.AssertEqual(response3.GetValue("Email").ToString(), email1);
           

            RestClientHelper.token_UserName = "India.localadmin.ext@wsa.com";
            RestClientHelper.token_Password = "J?PSnj2z";
            var response4 = externalAPI.getUserByEmail(email2, "");
            action.AssertEqual(response4.GetValue("Email").ToString(), email2);
        }

        [TestMethod]
        [TestCategory("API")]
        public void SAM101_API_getUserByEmail_Negative()
        {

            string email1 = "vinay.kv1.ext@wsa.com";
            string email2 = "test1234";
            string email3 = "India.localadmin.ext@wsa.com";

            Logger.LogPass("Get User by Email");

            var response1 = externalAPI.getUserByEmail_Negative(email1, "");
            action.isTrue(response1.StatusCode.Equals(HttpStatusCode.NotFound), "Error mesage displayed as expected for email ID not found");
            action.isTrue(response1.StatusDescription.Contains("User [vinay.kv1.ext@wsa.com] not found!"), response1.StatusDescription);

            var response2 = externalAPI.getUserByEmail_Negative(email2, "");
            action.isTrue(response2.StatusCode.Equals(HttpStatusCode.NotFound), "Error mesage displayed as expected for email invalid");
            action.isTrue(response2.StatusDescription.Contains("User [test1234] not found!"), response2.StatusDescription);
        }

        [TestMethod]
        [TestCategory("API")]
        public void SAM122_API_createAndUpdateUserWithApprove_Positive()
        {
            Logger.LogInfo("Create User with Approve");
            string password = "Access@123";

            //This script cover create , update and delete user API 

            #region Create Users
            //Parent Company 1 Admin and User creation
            string parentCompany1 = "64735634";// "985643542";
            Logger.LogInfo("Create Local Admin Widex Brand Parent Company 1");
            string adminEmail_PC1 = "Automation_Admin_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response1 = userAPI.createUserAndApprove("LocalAdmin", adminEmail_PC1, "GBR", true, parentCompany1,
                new List<string> { "31a9e13a-037b-4247-bde2-33fca8522595" });
            action.isTrue(response1.StatusCode.Equals(HttpStatusCode.Created), "User Created " + adminEmail_PC1);
            var getUser_response1 = externalAPI.getUserByEmail(adminEmail_PC1, "");
            action.AssertEqual(getUser_response1.GetValue("Email").ToString(), adminEmail_PC1);
            action.AssertEqual(getUser_response1.GetValue("Brands").ToString(), "Widex");
            string adminEmail_PC1_Id = getUser_response1.GetValue("Id").ToString();            //
            //userAPI.ChangePasswordAdmin(adminEmail_PC1_Id, "Access@123", "No");
            
            //Reset Password
            Logger.LogInfo("Reset Password for Local Admin");
            Operation.Login();
            Operation.selectBrand("Widex");
            Operation.openUserTab();
            Operation.updatePasswordForCustomer(adminEmail_PC1, password);
            
            /*
            Logger.LogInfo("Create User1 and Verify for Parent company 1");
            RestClientHelper.token_UserName = adminEmail_PC1;
            RestClientHelper.token_Password = password;
            IDS_API_External_GET.newToken = true;
            string user1_Email = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response2 = userAPI.createUserAndApprove("User", user1_Email, "GBR", true, parentCompany1,
                new List<string> { "88177646-61b8-4016-becb-f10ad1ca88b9" });
            action.isTrue(response2.StatusCode.Equals(HttpStatusCode.Created), "User Created " + user1_Email);
            var getUser_response2 = externalAPI.getUserByEmail(user1_Email, "");
            action.AssertEqual(getUser_response2.GetValue("Email").ToString(), user1_Email);
            Assert.AreEqual(getUser_response2.GetValue("Active").ToString(), "True");
            action.isTrue(getUser_response2.GetValue("Active").ToString().Contains("True"), "User is Active");
            string user1_ID = getUser_response2.GetValue("Id").ToString();
            //userAPI.ChangePasswordAdmin(user1_ID, "Access@123", "No");
             Operation.updatePasswordForCustomer(user1_Email, password);

            //Child Company 1 Admin and User Creation
            string childCompany1 = "3463453";// "835624523";
            Logger.LogInfo("Create Local Admin Widex Brand Child Company 1");
            string adminEmail_CC1 = "Automation_Admin_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response3 = userAPI.createUserAndApprove("LocalAdmin", adminEmail_CC1, "GBR", true, childCompany1,
                new List<string> { "31a9e13a-037b-4247-bde2-33fca8522595" });
            action.isTrue(response3.StatusCode.Equals(HttpStatusCode.Created), "User Created " + adminEmail_CC1);
            var getUser_response3 = externalAPI.getUserByEmail(adminEmail_CC1, "");
            action.AssertEqual(getUser_response3.GetValue("Email").ToString(), adminEmail_CC1);
            action.AssertEqual(getUser_response3.GetValue("Brands").ToString(), "Widex");
            string adminEmail_CC1_Id = getUser_response3.GetValue("Id").ToString();

            //Reset Password
            //userAPI.ChangePasswordAdmin(adminEmail_CC1_Id, "Access@123", "No");
            Logger.LogInfo("Reset Password for Local Admin 2");
             Operation.updatePasswordForCustomer(adminEmail_CC1, password);

            Logger.LogInfo("Create User2 and Verify for Child company 1");
            string user2_Email = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response4 = userAPI.createUserAndApprove("User", user2_Email, "GBR", false, childCompany1,
                new List<string> { "88177646-61b8-4016-becb-f10ad1ca88b9", "06cefeed-11f0-47a1-84f8-45bcd683788d" });
            action.isTrue(response4.StatusCode.Equals(HttpStatusCode.Created), "User Created " + user2_Email);
            var getUser_response4 = externalAPI.getUserByEmail(user2_Email, "");
            action.AssertEqual(getUser_response4.GetValue("Email").ToString(), user2_Email);
            Assert.IsFalse(getUser_response4.ContainsKey("Active"));
            string user2_Id = getUser_response4.GetValue("Id").ToString();
            userAPI.ChangePasswordAdmin(user2_Id, "Access@123", "No");


            //Parent Company 2 Admin and User creation

            string parentCompany2 = "546346346";
            string adminEmail_PC2 = "Automation_UK_Admin3@wsa.com";
            /*
            Logger.LogInfo("Create Local Admin Widex Brand Parent Company 2");
            RestClientHelper.token_UserName = "None";
            RestClientHelper.token_Password = "None";
            IDS_API_External_GET.newToken = true;
            string adminEmail_PC2 = "Automation_Admin_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response5 = userAPI.createUserAndApprove("LocalAdmin", adminEmail_PC2, "GRB", true, parentCompany2,
                new List<string> { "31a9e13a-037b-4247-bde2-33fca8522595" });
            action.isTrue(response5.StatusCode.Equals(HttpStatusCode.Created), "User Created " + adminEmail_PC2);

            var getUser_respons5 = externalAPI.getUserByEmail(adminEmail_PC2, "");
            action.AssertEqual(getUser_respons5.GetValue("Email").ToString(), adminEmail_PC2);
            action.AssertEqual(getUser_respons5.GetValue("Brands").ToString(), "Widex");
            string adminEmail_PC2_Id = getUser_respons5.GetValue("Id").ToString(); 
            
            
            //

            //Reset Password
            userAPI.ChangePasswordAdmin(adminEmail_PC2_Id, "Access@123", "No");
            //Logger.LogInfo("Reset Password for Local Admin 2");
            //Operation.updatePasswordForCustomer(adminEmail_PC2, password);
            

            Logger.LogInfo("Create User3 and Verify for Parent company 2");
            RestClientHelper.token_UserName = "Automation_UK_Admin3@wsa.com";
            RestClientHelper.token_Password = password;
            IDS_API_External_GET.newToken = true;
            string user3_Email = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response6 = userAPI.createUserAndApprove("User", user3_Email, "GBR", true, parentCompany2,
                new List<string> { "88177646-61b8-4016-becb-f10ad1ca88b9" });
            action.isTrue(response6.StatusCode.Equals(HttpStatusCode.Created), "User Created " + user3_Email);
            var getUser_response6 = externalAPI.getUserByEmail(user3_Email, "");
            action.AssertEqual(getUser_response6.GetValue("Email").ToString(), user3_Email);
            Assert.AreEqual(getUser_response6.GetValue("Active").ToString(), "True");
            action.isTrue(getUser_response6.GetValue("Active").ToString().Contains("True"), "User is Active");
            string user3_Email_Id = getUser_response6.GetValue("Id").ToString();
            userAPI.ChangePasswordAdmin(user3_Email_Id, "Access@123", "No");
           */

            /*
            //Widex Parent Company 1 Admin and User creation
            string parentCompany3 = "94563453";
            Logger.LogInfo("Create Local Admin Widex Brand");
            RestClientHelper.token_UserName = "None";
            RestClientHelper.token_Password = "None";
            RestClientHelper.Brand = "B2C_1_ROPC";//widex


            Logger.LogInfo("Create Local Admin Widex Brand Parent Company 1");
            string adminEmail_PC3 = "Automation_Admin_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response7 = userAPI.createUserAndApprove("LocalAdmin", adminEmail_PC3, "DNK", true, parentCompany3,
                 new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb", "a649e425-5170-46aa-b782-dd05a3842ddc" });
            action.isTrue(response7.StatusCode.Equals(HttpStatusCode.Created), "User Created " + adminEmail_PC3);
            var getUser_response7 = externalAPI.getUserByEmail(adminEmail_PC3, "");
            action.AssertEqual(getUser_response7.GetValue("Email").ToString(), adminEmail_PC3);
            action.AssertEqual(getUser_response7.GetValue("Brands").ToString(), "Widex");
            string adminEmail_PC3_Id = getUser_response7.GetValue("Id").ToString();

            Logger.LogInfo("Reset Password for widex Local Admin");
            Operation.selectBrand("Widex");
            Operation.openUserTab();
            Operation.updatePasswordForCustomer(adminEmail_PC3, password);

            Logger.LogInfo("Create User3 and Verify for Parent company 2");
            RestClientHelper.token_UserName = adminEmail_PC3;
            RestClientHelper.token_Password = password;
            IDS_API_External_GET.newToken = true;
            string user4_Email = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response8 = userAPI.createUserAndApprove("User", user4_Email, "DNK", true, parentCompany3,
                new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(response8.StatusCode.Equals(HttpStatusCode.Created), "User Created " + user4_Email);
            var getUser_response8 = externalAPI.getUserByEmail(user4_Email, "");
            action.AssertEqual(getUser_response8.GetValue("Email").ToString(), user4_Email);
            Assert.AreEqual(getUser_response8.GetValue("Active").ToString(), "True");
            action.isTrue(getUser_response8.GetValue("Active").ToString().Contains("True"), "User is Active");
            string user4_Email_Id = getUser_response8.GetValue("Id").ToString();
            
            #endregion

            #region Update User 
            RestClientHelper.token_UserName = adminEmail_PC1;
            RestClientHelper.token_Password = password;
            IDS_API_External_GET.newToken = true;
            Logger.LogInfo("Test Update User Self");
            var response9 = userAPI.UpdateUserWithApprove(adminEmail_PC1_Id, "LocalAdmin", adminEmail_PC1, "GBR", false, parentCompany1,
              new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(response9.StatusCode.Equals(HttpStatusCode.NoContent), "User updated " + adminEmail_PC1);
            var getUser_response9 = externalAPI.getUserByEmail(adminEmail_PC1, "");
            action.AssertEqual(getUser_response1.GetValue("Email").ToString(), adminEmail_PC1);
           
            Logger.LogInfo("Test Update API for User in same company ");
            var response10 = userAPI.UpdateUserWithApprove(user1_ID, "LocalAdmin", user1_Email, "GBR", true, parentCompany1,
             new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(response10.StatusCode.Equals(HttpStatusCode.NoContent), "User updated " + user1_Email);
            var getUser_response10 = externalAPI.getUserByEmail(adminEmail_PC1, "");
            action.isTrue(getUser_response10.ToString().Contains("Local Admin"), "User get updated succesfully");

            Logger.LogInfo("Test Update API for admin in child company ");
            var response11 = userAPI.UpdateUserWithApprove(adminEmail_CC1_Id, "LocalAdmin", adminEmail_CC1, "GBR", true, childCompany1,
             new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb", "06cefeed-11f0-47a1-84f8-45bcd683788d" });
            action.isTrue(response11.StatusCode.Equals(HttpStatusCode.NoContent), "User updated " + adminEmail_CC1_Id);
            var getUser_response11 = externalAPI.getUserByEmail(adminEmail_CC1, "");
            action.isTrue(getUser_response11.ToString().Contains("06cefeed-11f0-47a1-84f8-45bcd683788d"), "Roles get updated succesfully");

            Logger.LogInfo("Test Update API for User in child company ");
            var response12 = userAPI.UpdateUserWithApprove(user2_Id, "User", user2_Email, "GBR", true, childCompany1,
             new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(response12.StatusCode.Equals(HttpStatusCode.NoContent), "User updated " + user2_Email);
            var getUser_response12 = externalAPI.getUserByEmail(user2_Email, "");
            */
            #endregion


            /*
            #region Delete User 
            RestClientHelper.token_UserName = adminEmail_PC1;
            RestClientHelper.token_Password = password;
            IDS_API_External_GET.newToken = true;


            Logger.LogInfo("Test Delete API for User in same company ");
            var response14 = userAPI.deleteUser(user1_ID);
            action.isTrue(response14.StatusCode.Equals(HttpStatusCode.NoContent), "User Deleted succesfully: " + user1_Email);
            var getUser_response14 = externalAPI.getUserByEmail_Negative(user1_Email, "");
            action.isTrue(getUser_response14.StatusCode.Equals(HttpStatusCode.NotFound), "Error mesage displayed as expected for email ID not found");
            action.isTrue(getUser_response14.StatusDescription.Contains(" not found!"), getUser_response14.StatusDescription);

            Logger.LogInfo("Test Delete API for admin in child company ");
            var response15 = userAPI.deleteUser(adminEmail_CC1_Id);
            action.isTrue(response15.StatusCode.Equals(HttpStatusCode.NoContent), "User Deleted succesfully: " + adminEmail_CC1_Id);
            var getUser_response15 = externalAPI.getUserByEmail_Negative(adminEmail_CC1, "");
            action.isTrue(getUser_response15.StatusCode.Equals(HttpStatusCode.NotFound), "Error mesage displayed as expected for email ID not found");
            action.isTrue(getUser_response15.StatusDescription.Contains(" not found!"), getUser_response15.StatusDescription);

            Logger.LogInfo("Test Delete API for User in child company ");
            var response16 = userAPI.deleteUser(user2_Id);
            action.isTrue(response16.StatusCode.Equals(HttpStatusCode.NoContent), "User Deleted succesfully: " + user2_Email);
            var getUser_response16 = externalAPI.getUserByEmail_Negative(user2_Email, "");
            action.isTrue(getUser_response16.StatusCode.Equals(HttpStatusCode.NotFound), "Error mesage displayed as expected for email ID not found");
            action.isTrue(getUser_response16.StatusDescription.Contains(" not found!"), getUser_response16.StatusDescription);

            Logger.LogInfo("Test Delete User Self");
            var response13 = userAPI.deleteUser(adminEmail_PC1_Id);
            action.isTrue(response13.StatusCode.Equals(HttpStatusCode.NoContent), "User Deleted succesfully:" + adminEmail_PC1);
            var getUser_response13 = externalAPI.getUserByEmail_Negative(adminEmail_PC1, "");
            action.isTrue(getUser_response13.StatusCode.Equals(HttpStatusCode.NotFound), "Error mesage displayed as expected for email ID not found");
            action.isTrue(getUser_response13.StatusDescription.Contains(" not found!"), getUser_response13.StatusDescription);

            #endregion
            */



        }

        [TestMethod]
        [TestCategory("API")]
        public void SAM122_API_createUserWithApprove_Negative()
        {
            Logger.LogInfo("Create User with Approve External API");
            string password = "Access@123";
            string local_admin = "Automation_Admin_2811124037@wsa.com";
            //
            Logger.LogInfo("Test API with existing email ID");
            string newEmail = "Automation_Admin_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            RestClientHelper.token_UserName = local_admin;
            RestClientHelper.token_Password = password;
            var response1 = userAPI.createUserAndApprove("LocalAdmin", local_admin, "IND", true, "C23001067",
                new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response1.StatusCode.Equals(HttpStatusCode.Created), "Error Message displayed for existing email ID ");
            action.isTrue(response1.StatusDescription.Contains("A User with (Automation_Admin_2811124037@wsa.com) already exists!"), response1.StatusDescription);

            Logger.LogInfo("Test API without email ID");
            var response2 = userAPI.createUserAndApprove("LocalAdmin", "", "IND", true, "C23001067",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response2.StatusCode.Equals(HttpStatusCode.Created), "Error Message displayed for empty email ID ");
            action.isTrue(response2.StatusDescription.Contains("email or username is required"), response2.StatusDescription);

            Logger.LogInfo("Test API with company belong to another hirarcy ");
            var response2_1 = userAPI.createUserAndApprove("LocalAdmin", newEmail, "IND", true, "C23001066",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response2_1.StatusCode.Equals(HttpStatusCode.Created), "Error Message displayed for company not belongs to parent  ");
            action.isTrue(response2_1.StatusDescription.Contains("to this company:[C23001066]"), response2_1.StatusDescription);


            Logger.LogInfo("Test API with Country not belong to Local Admin");
            var response3 = userAPI.createUserAndApprove("LocalAdmin", newEmail, "DEU", true, "C23001067",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response3.StatusCode.Equals(HttpStatusCode.Created), "Error Message displayed for Country not belong to Local Admin");
            action.isTrue(response3.StatusDescription.Contains("Company [C23001067] not found!"), response3.StatusDescription);

            Logger.LogInfo("Test API without Country");
            var response4 = userAPI.createUserAndApprove("LocalAdmin", newEmail, "", true, "C23001067",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response4.StatusCode.Equals(HttpStatusCode.Created), "Error Message displayed for EMPTY Country ");
            action.isTrue(response4.StatusDescription.Contains("Neither Country nor Company can be empty"), response4.StatusDescription);

            Logger.LogInfo("Test API without Company");
            var response5 = userAPI.createUserAndApprove("LocalAdmin", newEmail, "IND", true, "",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response5.StatusCode.Equals(HttpStatusCode.Created), "Error Message displayed for EMPTY Company ");
            action.isTrue(response5.StatusDescription.Contains("Neither Country nor Company can be empty"), response5.StatusDescription);

            Logger.LogInfo("Test API with Company not belong to Local Admin");
            var response6 = userAPI.createUserAndApprove("LocalAdmin", newEmail, "IND", true, "618600",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response6.StatusCode.Equals(HttpStatusCode.Created), "Error Message displayed for Company not belongs to Local admin ");
            action.isTrue(response6.StatusDescription.Contains("Company [618600] not found!"), response6.StatusDescription);

            /*
            Logger.LogInfo("Test API with empty Role ID");
            var response7 = userAPI.createUserAndApprove("LocalAdmin", newEmail, "IND", true, "C23001067",
               new List<string> { "" });
            action.isTrue(!response7.StatusCode.Equals(HttpStatusCode.Created), "Error Message displayed for empty Role ID");
            action.isTrue(response7.StatusDescription.Contains("Roles cannot be empty"), response7.StatusDescription);
            */
            Logger.LogInfo("Test API with Wrong Role ID");
            var response8 = userAPI.createUserAndApprove("LocalAdmin", newEmail, "IND", true, "C23001067",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8ab" });
            action.isTrue(!response8.StatusCode.Equals(HttpStatusCode.Created), "Error Message displayed for wrong Role ID");
            action.isTrue(response8.StatusDescription.Contains("Role [a55d5738-6b9d-4517-8dc3-632f9191c8ab] not found!"), response8.StatusDescription);

            Logger.LogInfo("Test API with multiple roles beongs to same application");
            var response9 = userAPI.createUserAndApprove("LocalAdmin", newEmail, "IND", true, "C23001067",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb", "31a9e13a-037b-4247-bde2-33fca8522595" });
            action.isTrue(!response9.StatusCode.Equals(HttpStatusCode.Created), "Error Message displayed for multiple roles beongs to same application");
            action.isTrue(response9.StatusDescription.Contains("Only one role per application is allowed for user."), response9.StatusDescription);
        }

        [TestMethod]
        [TestCategory("API")]
        public void SAM126_API_ReturnAllTheUser_Positive()
        {
            Logger.LogInfo("Return All the User API ");
            var myDict = new Dictionary<string, string>
            {
              { "skip", "0" },
               { "take", "300" },
                 { "customerNumbers", "" },
                  { "roleIds", "" },
                   { "status", "" },
                    { "search", "" }
            };
            Logger.LogInfo("Get users from signia local admin");
            IDS_API_External_GET.newToken = true;
            RestClientHelper.token_UserName = "Automation_Admin_0301140320@wsa.com";
            RestClientHelper.token_Password = "Access@123";
            RestClientHelper.Brand = "B2C_1_ROPC";
            //myDict["appId"] = "d09257a8-7a67-46a5-8323-8a38f36d8451";
            var response1 = externalAPI.ReturnAllTheUser(myDict);
            action.isTrue(response1.ContainsKey("Count"), "Users Count " + response1.GetValue("Count"));
            Assert.IsTrue(response1.ToString().Contains("Id"));
            Assert.IsTrue(response1.ToString().Contains("Email"));
            Assert.IsTrue(response1.ToString().Contains("Name"));
            Assert.IsTrue(response1.ToString().Contains("RoleId"));
            Assert.IsTrue(response1.ToString().Contains("RoleName"));
            Assert.IsTrue(response1.ToString().Contains("Status"));
            Assert.IsTrue(response1.ToString().Contains("CompanyId"));
            Assert.IsTrue(response1.ToString().Contains("CompanyName"));
            Assert.IsTrue(response1.ToString().Contains("LocalCustomerNumber"));

            Logger.LogInfo("Filter users by AppID");
            //myDict["appId"] = "512f28b5-fc51-408e-9236-1193c196ecf7";
            var response2 = externalAPI.ReturnAllTheUser(myDict);
            action.isTrue(response2.ContainsKey("Count"), "Users Count " + response2.GetValue("Count"));

            Logger.LogInfo("Filter users by Customer Number");
            myDict["customerNumbers"] = "72352342";
            var response3 = externalAPI.ReturnAllTheUser(myDict);
            action.isTrue(response3.ToString().Contains( "72352342"),"Customer Number");
            


            myDict["customerNumbers"] = "";

            Logger.LogInfo("Filter users by Role ID");
            myDict["roleIds"] = "a55d5738-6b9d-4517-8dc3-632f9191c8eb";
            var response4 = externalAPI.ReturnAllTheUser(myDict);
            //action.AssertEqual(action.countNoOfItemsInString(response4.ToString(), "a55d5738-6b9d-4517-8dc3-632f9191c8eb"), response4.GetValue("Count").ToString(), "Count");
            myDict["roleIds"] = "";

            Logger.LogInfo("Filter users by Status InActive");
            myDict["status"] = "0";
            var response5 = externalAPI.ReturnAllTheUser(myDict);
            action.isTrue(response5.ContainsKey("Count"), "Users Count " + response5.GetValue("Count"));


            Logger.LogInfo("Filter users by Status Active");
            myDict["status"] = "1";
            var response6 = externalAPI.ReturnAllTheUser(myDict);
            action.isTrue(response6.ContainsKey("Count"), "Users Count " + response6.GetValue("Count"));
            myDict["status"] = "";

            Logger.LogInfo("Filter users by Status Pending");
            myDict["status"] = "2";
            var response6_1 = externalAPI.ReturnAllTheUser(myDict);
            action.isTrue(response6_1.ContainsKey("Count"), "Users Count " + response6_1.GetValue("Count"));
            myDict["status"] = "";

            Logger.LogInfo("Filter users by email search");
            myDict["search"] = "Automation_User";
            var response7 = externalAPI.ReturnAllTheUser(myDict);
            action.AssertEqual(action.countNoOfItemsInString(response7.ToString(), "Automation_User"), response7.GetValue("Count").ToString(), "Count");

            Logger.LogInfo("Filter users by User name search");
            myDict["search"] = "user AUT";
            var response8 = externalAPI.ReturnAllTheUser(myDict);
            action.AssertEqual(action.countNoOfItemsInString(response8.ToString(), "user AUT"), response8.GetValue("Count").ToString(), "Count");
            myDict["search"] = "";

            //SAM 261 - Verify API Returns multiple roles
            IDS_API_External_GET.newToken = true;
            RestClientHelper.token_UserName = "testmultipleRolesparent@wsa.com";
            RestClientHelper.token_Password = "Access@123";
            var response9 = externalAPI.ReturnAllTheUser(myDict);
            
            action.isTrue(response9.ContainsKey("Count"), "Users Count " + response9.GetValue("Count"));
            //ction.isTrue(response9.GetValue("Count").ToString().Contains("1"), "1 User returnd");
            //action.AssertEqual(action.countNoOfItemsInString(response9.ToString(), "RoleId"), "3", "Role ID Count 2");
            //action.AssertEqual(action.countNoOfItemsInString(response9.ToString(), "RoleName"), "2", "RoleName Count 2");
        }


        [TestMethod]
        [TestCategory("API")]
        public void API_ReturnAllAndDeleteUser()
        {
            //string existingUser = "Automation_Admin_0301154640@wsa.com;Automation_PendingUser_2202132858@wsa.com;Automation_UK_Admin3@wsa.com;Automation_UK_Admin3@wsa.com;Automation_Admin_2811124037@wsa.com;Automation_Admin_2811124037@wsa.com;Automation_Admin_0301140320@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_Admin_0301140320@wsa.com;Automation_User_0301140437@wsa.com;Automation_Admin_0301140543@wsa.com;Automation_User_0301140617@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_User_0301140437@wsa.com;Automation_User_0301140437@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_User_0301140437@wsa.com;Automation_UK_Admin2@wsa.com;Automation_PendingUser_1602102604@wsa.com;Automation_Admin_1201154632@wsa.com;314b0c86-0752-49b9-b1a0-54b6abf09c73;8d43e8fd-390c-48e6-8d84-998d7db1ea93;b06ed345-53ff-4055-89db-162f01371778;Automation_Admin_1201171009@wsa.com;af5d9507-2b0f-4c19-a4fb-b8b9b41923c4;Automation_UK_Admin1@wsa.com;Automation_User_1602094739@wsa.com;Automation_User_1801095606@wsa.com;Automation_User_1801095715@wsa.com;Automation_Admin_1801095453@wsa.com;Automation_Admin_1801095643@wsa.com;Automation_Admin_1801093000@wsa.com;Automation_User_1801092918@wsa.com;Automation_User_0701093207@wsa.com;Automation_PendingUser_2202132858@wsa.com;";
            string existingUser = "Automation_Admin_0301154640@wsa.com;Automation_PendingUser_2202132858@wsa.com;Automation_UK_Admin3@wsa.com;Automation_UK_Admin3@wsa.com;Automation_Admin_2811124037@wsa.com;Automation_Admin_2811124037@wsa.com;Automation_Admin_0301140320@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_Admin_0301140320@wsa.com;Automation_User_0301140437@wsa.com;Automation_Admin_0301140543@wsa.com;Automation_User_0301140617@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_User_0301140437@wsa.com;Automation_User_0301140437@wsa.com;Automation_Admin_0301140533@wsa.com;Automation_User_0301140437@wsa.com;Automation_UK_Admin2@wsa.com;Automation_PendingUser_1602102604@wsa.com;Automation_Admin_1201154632@wsa.com;314b0c86-0752-49b9-b1a0-54b6abf09c73;8d43e8fd-390c-48e6-8d84-998d7db1ea93;b06ed345-53ff-4055-89db-162f01371778;Automation_Admin_1201171009@wsa.com;af5d9507-2b0f-4c19-a4fb-b8b9b41923c4;Automation_UK_Admin1@wsa.com;Automation_User_1602094739@wsa.com;Automation_User_1801095606@wsa.com;Automation_User_1801095715@wsa.com;Automation_Admin_1801095453@wsa.com;Automation_Admin_1801095643@wsa.com;Automation_Admin_1801093000@wsa.com;Automation_User_1801092918@wsa.com;Automation_User_0701093207@wsa.com;Automation_PendingUser_2202132858@wsa.com;796134dd-36da-4b12-bf89-2a6b54b0936e;db2fbc42-2615-49b9-a3aa-0ad61c5ddab7;05949b53-6380-4ec7-9358-8dc1b54d8234;727f577a-d14d-4f61-8462-27e8117858dd;1f8ed7e0-c89b-40ad-9298-e9bcea1b67de;05949b53-6380-4ec7-9358-8dc1b54d8234;8e435bed-44af-4116-ad52-576216ae1b25;";
            Logger.LogInfo("Return All the User API ");
            var myDict = new Dictionary<string, string>
            {
              { "skip", "0" },
               { "take", "1319" },
                 { "customerNumbers", "" },
                  { "roleIds", "" },
                   { "status", "" },
                    { "search", "" }
            };
            Logger.LogInfo("Get users from signia local admin");
            IDS_API_External_GET.newToken = true;
            RestClientHelper.token_UserName = "system.admin@wsa.com";
            RestClientHelper.token_Password = "Wsa@1234";
            RestClientHelper.Brand = "B2C_1_ROPC";
            //myDict["appId"] = "d09257a8-7a67-46a5-8323-8a38f36d8451";
            var response1 = externalAPI.ReturnAllTheUser(myDict);
            JArray items1 = (JArray)response1["Items"];
            int count = 1;
            foreach (JObject content in items1.Children<JObject>())
            {
               string id =  content.GetValue("Id").ToString();
                string email="";
                try
                {
                    email = content.GetValue("Email").ToString();
                }
                catch
                {
                continue;
                }
              

                if (email.Contains("Automation"))
                {
                    if (existingUser.Contains(id) || existingUser.Contains(email))
                    {
                        //skip
                    }
                    else
                    {
                        var response14 = userAPI.deleteUser(id, "No");
                        action.isTrue(response14.StatusCode.Equals(HttpStatusCode.NoContent), "User Deleted succesfully: " + email);
                        count++;
                    }
                }
            }
        }



        [TestMethod]
        [TestCategory("API")]
        public void SAM126_API_ReturnAllTheUser_Negative()
        {

            var myDict = new Dictionary<string, string>
            {
              { "skip", "0" },
               { "take", "10" },
                { "appId", "" },
                 { "customerNumbers", "" },
                  { "roleIds", "" },
                   { "status", "" },
                    { "search", "" }
            };
            Logger.LogInfo("Login as signia local admin");
            IDS_API_External_GET.newToken = true;
            RestClientHelper.token_UserName = "Signia.localadmin1.india@wsa.com";
            RestClientHelper.token_Password = "Access@123";

            Logger.LogInfo("Test API with wrong App ID");
            myDict["appId"] = "d09257a8-7a67-46a5-8323-8a38f36d8455";
            var response1 = externalAPI.ReturnAllTheUser_Negative(myDict);
            action.isTrue(response1.StatusCode.Equals(HttpStatusCode.OK), "Zero items return for Wrong AppId");
            JArray items0 = (JArray)JObject.Parse(response1.Content)["Items"];
            action.AssertEqual(0, items0.Count, "Number of items ");

          

            Logger.LogInfo("Test API with invalid roleID");
            myDict["appId"] = "d09257a8-7a67-46a5-8323-8a38f36d8451";
            myDict["roleIds"] = "53252323";
            var response2 = externalAPI.ReturnAllTheUser_Negative(myDict);
            action.isTrue(!response2.StatusCode.Equals(HttpStatusCode.OK), "Error Message displayed for invalid role ID");
            action.isTrue(response2.StatusDescription.Contains("Passed RoleId:[53252323] is not a valid Guid"), response2.StatusDescription);
            myDict["roleIds"] = "";

            Logger.LogInfo("Test API with wrong roleID");
            myDict["roleIds"] = "c9912c1f-9223-405a-beb7-1b0baec2cb82";
            var response3 = externalAPI.ReturnAllTheUser_Negative(myDict);
            action.isTrue(response3.StatusCode.Equals(HttpStatusCode.OK), "Zero items return for wrong Role ID");
            JArray items1 = (JArray)JObject.Parse(response3.Content)["Items"];
            action.AssertEqual(0, items1.Count, "Number of items ");
            myDict["roleIds"] = "";

            Logger.LogInfo("Test API with wrong status");
            myDict["status"] = "4";
            var response4 = externalAPI.ReturnAllTheUser_Negative(myDict);
            action.isTrue(response4.StatusCode.Equals(HttpStatusCode.OK), "Zero items return for wrong Status");
            JArray items2 = (JArray)JObject.Parse(response4.Content)["Items"];
            action.AssertEqual(0, items2.Count, "Number of items ");
            myDict["status"] = "";

            Logger.LogInfo("Test API with wrong customer number");
            myDict["customerNumbers"] = "4634634";
            var response5 = externalAPI.ReturnAllTheUser_Negative(myDict);
            action.isTrue(response5.StatusCode.Equals(HttpStatusCode.OK), "Zero items return for wrong Customer Number");
            JArray items3 = (JArray)JObject.Parse(response5.Content)["Items"];
            action.AssertEqual(0, items3.Count, "Number of items ");
            myDict["customerNumbers"] = "";

            Logger.LogInfo("Test API with Not existing email ID");
            myDict["search"] = "test@gmail.com";
            var response6 = externalAPI.ReturnAllTheUser_Negative(myDict);
            action.isTrue(response6.StatusCode.Equals(HttpStatusCode.OK), "Zero items return for not existing email ID");
            JArray items4 = (JArray)JObject.Parse(response6.Content)["Items"];
            action.AssertEqual(0, items4.Count, "Number of items ");

            Logger.LogInfo("Test API with Not existing Customer Name");
            myDict["search"] = "Test API";
            var response7 = externalAPI.ReturnAllTheUser_Negative(myDict);
            action.isTrue(response7.StatusCode.Equals(HttpStatusCode.OK), "Zero items return for not existing Customer Name");
            JArray items5 = (JArray)JObject.Parse(response7.Content)["Items"];
            action.AssertEqual(0, items5.Count, "Number of items ");
            myDict["search"] = "";

            Logger.LogInfo("Login as signia user");
            IDS_API_External_GET.newToken = true;
            RestClientHelper.token_UserName = "Signia.Company1_user1.india@wsa.com";
            RestClientHelper.token_Password = "Access@123";
            var response8 = externalAPI.ReturnAllTheUser_Negative(myDict);
            action.isTrue(!response8.StatusCode.Equals(HttpStatusCode.OK), "User not having access to get users as expected");
            action.isTrue(response8.Content.ToString().Contains("You don't have access to this action method!"), response8.Content.ToString());
        }

        [TestMethod]

        public void SAM_122_createUserWithApprove_Debug()
        {
            Logger.LogInfo("Create User with Approve");
            string password = "Access@123";
            string parentCompany1 = "72352342";// "985643542";
            string adminEmail_PC1 = "";
            
                //Parent Company 1 Admin and User creation

                Logger.LogInfo("Create Local Admin Signia Brand Parent Company 1");
                adminEmail_PC1 = "Automation_Admin_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
                var response1 = userAPI.createUserAndApprove("LocalAdmin", adminEmail_PC1, "IND", true, parentCompany1,
                    new List<string> { "31a9e13a-037b-4247-bde2-33fca8522595" });
                action.isTrue(response1.StatusCode.Equals(HttpStatusCode.Created), "User Created " + adminEmail_PC1);
                var getUser_response1 = externalAPI.getUserByEmail(adminEmail_PC1, "");
                action.AssertEqual(getUser_response1.GetValue("Email").ToString(), adminEmail_PC1);
                action.AssertEqual(getUser_response1.GetValue("Brands").ToString(), "Widex");
                string adminEmail_PC1_Id = getUser_response1.GetValue("Id").ToString();            //
            
            //Reset Password
            Logger.LogInfo("Reset Password for Local Admin");
            Operation.Login();
            Operation.selectBrand("Widex");
            Operation.openUserTab();
            Operation.updatePasswordForCustomer(adminEmail_PC1, password);


            Logger.LogInfo("Create User1 and Verify for Parent company 1");
            RestClientHelper.token_UserName = adminEmail_PC1;
            RestClientHelper.token_Password = password;
            IDS_API_External_GET.newToken = true;
            string user1_Email = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response2 = userAPI.createUserAndApprove("User", user1_Email, "IND", true, parentCompany1,
                new List<string> { "06cefeed-11f0-47a1-84f8-45bcd683788d" });
            action.isTrue(response2.StatusCode.Equals(HttpStatusCode.Created), "User Created " + user1_Email);
            var getUser_response2 = externalAPI.getUserByEmail(user1_Email, "");
            action.AssertEqual(getUser_response2.GetValue("Email").ToString(), user1_Email);
            Assert.AreEqual(getUser_response2.GetValue("Active").ToString(), "True");
            action.isTrue(getUser_response2.GetValue("Active").ToString().Contains("True"), "User is Active");


            string user2_Email = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response3 = userAPI.createUserAndApprove("User", user2_Email, "IND", true, parentCompany1,
                new List<string> { "adac02c9-2c6b-4ab8-a43b-7e689ef45123" });
            action.isTrue(response3.StatusCode.Equals(HttpStatusCode.Created), "User Created " + user2_Email);
            var getUser_response3 = externalAPI.getUserByEmail(user2_Email, "");
            action.AssertEqual(getUser_response3.GetValue("Email").ToString(), user2_Email);
            Assert.AreEqual(getUser_response3.GetValue("Active").ToString(), "True");
            action.isTrue(getUser_response3.GetValue("Active").ToString().Contains("True"), "User is Active");

            string user3_Email = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response4 = userAPI.createUserAndApprove("User", user3_Email, "IND", true, parentCompany1,
                new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(response4.StatusCode.Equals(HttpStatusCode.Created), "User Created " + user3_Email);
            var getUser_response4 = externalAPI.getUserByEmail(user3_Email, "");
            action.AssertEqual(getUser_response4.GetValue("Email").ToString(), user3_Email);
            Assert.AreEqual(getUser_response4.GetValue("Active").ToString(), "True");
            action.isTrue(getUser_response4.GetValue("Active").ToString().Contains("True"), "User is Active");

        }


        [TestMethod]
        [TestCategory("API")]
        public void SAM131_API_UpdateUserWithApprove_Negative()
        {
            Logger.LogInfo("Update User with Approve");
            string password = "Access@123";


            //Parent Company 1 Admin and User creation
            RestClientHelper.token_UserName = "Automation_Admin_0301140533@wsa.com";
            RestClientHelper.token_Password = password;
            IDS_API_External_GET.newToken = true;

            Logger.LogInfo("Verify Local admin can't update local admin of parent company");
            var response1 = userAPI.UpdateUserWithApprove("db2fbc42-2615-49b9-a3aa-0ad61c5ddab7", "LocalAdmin", "Automation_Admin_0301140320@wsa.com", "IND", true, "72352342",
                new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response1.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for Local admin can't update local admin of parent company");
            action.isTrue(response1.StatusDescription.Contains("You aren't authorized to update the user"), response1.StatusDescription);

            Logger.LogInfo("Verify Local admin can't update User of parent company");
            var response2 = userAPI.UpdateUserWithApprove("05949b53-6380-4ec7-9358-8dc1b54d8234", "User", "Automation_User_0301140437@wsa.com", "IND", true, "72352342",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response2.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for Local admin can't update user of parent company");
            action.isTrue(response2.StatusDescription.Contains("You aren't authorized to update the user"), response2.StatusDescription);

            Logger.LogInfo("Verify Local admin can't update local admin of another company");
            var response3 = userAPI.UpdateUserWithApprove("727f577a-d14d-4f61-8462-27e8117858dd", "LocalAdmin", "Automation_Admin_0301140543@wsa.com", "IND", true, "45623244",
                new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response3.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for Local admin can't update local admin of another company");
            action.isTrue(response3.StatusDescription.Contains("Company [45623244] not found"), response3.StatusDescription);

            Logger.LogInfo("Verify Local admin can't update User of another company");
            var response4 = userAPI.UpdateUserWithApprove("1f8ed7e0-c89b-40ad-9298-e9bcea1b67de", "User", "Automation_User_0301140617@wsa.com", "IND", true, "45623244",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response4.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for Local admin can't update user of another company");
            action.isTrue(response4.StatusDescription.Contains("Company [45623244] not found"), response4.StatusDescription);

            Logger.LogInfo("Verify user can't update email ID");
            var response5 = userAPI.UpdateUserWithApprove("71a870fc-d30e-437b-b4b1-214dc6893c35", "LocalAdmin", "updateEmail@wsa.com", "IND", true, "72352342",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response5.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for update email ID");
            action.isTrue(response5.StatusDescription.Contains("Email is not editable"), response5.StatusDescription);

            Logger.LogInfo("Verify Update user API for wrong userID");
            var response6 = userAPI.UpdateUserWithApprove("71a870fc-d30e-437b-b4b1-214dc6893c30", "LocalAdmin", "Automation_Admin_0301140533@wsa.com", "IND", true, "72352342",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response6.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for wrong user ID");
            action.isTrue(response6.StatusDescription.Contains("User [71a870fc-d30e-437b-b4b1-214dc6893c30] not found"), response6.StatusDescription);

            Logger.LogInfo("Verify Update user API for without email ID");
            var response7 = userAPI.UpdateUserWithApprove("71a870fc-d30e-437b-b4b1-214dc6893c35", "LocalAdmin", "", "IND", true, "72352342",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response7.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for empty email ID");
            //action.isTrue(response7.StatusDescription.Contains(" Email is not editable"), response7.StatusDescription);

            Logger.LogInfo("Verify Update user API for without County");
            var response8 = userAPI.UpdateUserWithApprove("71a870fc-d30e-437b-b4b1-214dc6893c35", "LocalAdmin", "Automation_Admin_0301140533@wsa.com", "", true, "72352342",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response8.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for empty county");
            action.isTrue(response8.StatusDescription.Contains("Neither Country nor Company can be empty"), response8.StatusDescription);

            Logger.LogInfo("Verify Update user API for County where company not belong To");
            var response9 = userAPI.UpdateUserWithApprove("71a870fc-d30e-437b-b4b1-214dc6893c35", "LocalAdmin", "Automation_Admin_0301140533@wsa.com", "CAN", true, "72352342",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response9.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for different company");
            action.isTrue(response9.StatusDescription.Contains("Company [72352342] not found"), response9.StatusDescription);

            Logger.LogInfo("Verify Update user API for company field empty");
            var response10 = userAPI.UpdateUserWithApprove("71a870fc-d30e-437b-b4b1-214dc6893c35", "LocalAdmin", "Automation_Admin_0301140533@wsa.com", "IND", true, "",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response10.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for empty company");
            action.isTrue(response10.StatusDescription.Contains("Neither Country nor Company can be empty"), response10.StatusDescription);

            Logger.LogInfo("Verify Update user API for company not belong to local admin");
            var response11 = userAPI.UpdateUserWithApprove("71a870fc-d30e-437b-b4b1-214dc6893c35", "LocalAdmin", "Automation_Admin_0301140533@wsa.com", "IND", true, "45623244",
               new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response11.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for company not belong to local admin");
            action.isTrue(response11.StatusDescription.Contains("Company [45623244] not found"), response11.StatusDescription);

            Logger.LogInfo("Verify Update user API for empty Role ID");
            var response12 = userAPI.UpdateUserWithApprove("71a870fc-d30e-437b-b4b1-214dc6893c35", "LocalAdmin", "Automation_Admin_0301140533@wsa.com", "IND", true, "72352342",
               new List<string> { "" });
            action.isTrue(!response12.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for empty Role ID");
           // action.isTrue(response12.StatusDescription.Contains("Roles cannot be empty"), response12.StatusDescription);

            RestClientHelper.token_UserName = "Automation_User_0301140437@wsa.com";
            RestClientHelper.token_Password = password;
            IDS_API_External_GET.newToken = true;

            Logger.LogInfo("Verify User can't update ");
            var response13 = userAPI.UpdateUserWithApprove("05949b53-6380-4ec7-9358-8dc1b54d8234", "LocalAdmin", "Automation_User_0301140437@wsa.com", "IND", true, "72352342",
                new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(!response13.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for User can't update self");
            action.isTrue(response13.Content.ToString().Contains("You don't have access to this action method"), response13.StatusDescription);

        }


        [TestMethod]
        [TestCategory("API")]
        public void SAM133_API_DeleteUser_Negative()
        {
            Logger.LogInfo("Delete User by ID");
            string password = "Access@123";


            //Parent Company 1 Admin and User creation
            RestClientHelper.token_UserName = "Automation_Admin_0301140533@wsa.com";
            RestClientHelper.token_Password = password;
            IDS_API_External_GET.newToken = true;

            Logger.LogInfo("Verify Local admin can't Delete local admin of parent company");
            var response1 = userAPI.deleteUser("db2fbc42-2615-49b9-a3aa-0ad61c5ddab7");
            action.isTrue(!response1.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for Local admin can't Delete local admin of parent company");
            action.isTrue(response1.StatusDescription.Contains("You aren't authorized to delete the user"), response1.StatusDescription);

            Logger.LogInfo("Verify Local admin can't Delete User of parent company");
            var response2 = userAPI.deleteUser("05949b53-6380-4ec7-9358-8dc1b54d8234");
            action.isTrue(!response2.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for Local admin can't Delete user of parent company");
            action.isTrue(response2.StatusDescription.Contains("You aren't authorized to delete the user"), response2.StatusDescription);

            Logger.LogInfo("Verify Local admin can't Delete local admin of another company");
            var response3 = userAPI.deleteUser("727f577a-d14d-4f61-8462-27e8117858dd");
            action.isTrue(!response3.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for Local admin can't Delete local admin of another company");
            action.isTrue(response3.StatusDescription.Contains("You aren't authorized to delete the user"), response3.StatusDescription);

            Logger.LogInfo("Verify Local admin can't Delete User of another company");
            var response4 = userAPI.deleteUser("1f8ed7e0-c89b-40ad-9298-e9bcea1b67de");
            action.isTrue(!response4.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for Local admin can't Delete user of another company");
            action.isTrue(response4.StatusDescription.Contains("You aren't authorized to delete the user"), response4.StatusDescription);

            RestClientHelper.token_UserName = "Automation_User_0301140437@wsa.com";
            RestClientHelper.token_Password = password;
            IDS_API_External_GET.newToken = true;
            Logger.LogInfo("Verify User can't Delete self ");
            var response13 = userAPI.deleteUser("05949b53-6380-4ec7-9358-8dc1b54d8234");
            action.isTrue(!response13.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for User can't delete self");
            action.isTrue(response13.Content.ToString().Contains("You don't have access to this action method"), response13.StatusDescription);

        }


        [TestMethod]
        [TestCategory("API")]
        public void SAM132_API_UpdateUserProfile_Positive()
        {
            Logger.LogInfo("Update User profile");
            string admin = "Automation_UK_Admin2@wsa.com";
            RestClientHelper.token_UserName = admin;
            RestClientHelper.token_Password = "Access@123";
            string admin_ID = "796134dd-36da-4b12-bf89-2a6b54b0936e";


            Logger.LogInfo("Update admin profile");
            var response1 = userAPI.UpdateUserProfile(admin_ID, "Automation", "company_admin", "tester_admin", "2424242421","en");
            action.isTrue(response1.StatusCode.Equals(HttpStatusCode.NoContent), "User profile updated ");
            var getUser_response1 = externalAPI.getUserByEmail(admin, "");
            action.AssertEqual(getUser_response1.GetValue("GivenName").ToString(), "Automation");
            action.AssertEqual(getUser_response1.GetValue("SurName").ToString(), "company_admin");
            action.AssertEqual(getUser_response1.GetValue("JobTitle").ToString(), "tester_admin");
            action.AssertEqual(getUser_response1.GetValue("PhoneMobile").ToString(), "2424242421");
            action.AssertEqual(getUser_response1.GetValue("Language").ToString(), "en");
            var response2 = userAPI.UpdateUserProfile(admin_ID, "test", "test", "test", "0000000", "fr");

            string user = "Automation_PendingUser_1602102604@wsa.com";
            RestClientHelper.token_UserName = user;
            RestClientHelper.token_Password = "Access@123";
            string user_ID = "db278f44-fb63-4ead-8097-e9f8ef14baba";

            Logger.LogInfo("Update user profile");
            var response3 = userAPI.UpdateUserProfile(user_ID, "Automation", "company_user", "tester_user", "734633344", "en");
            action.isTrue(response3.StatusCode.Equals(HttpStatusCode.NoContent), "User profile updated ");
            var getUser_response2 = externalAPI.getUserByEmail(user, "");
            action.AssertEqual(getUser_response2.GetValue("GivenName").ToString(), "Automation");
            action.AssertEqual(getUser_response2.GetValue("SurName").ToString(), "company_user");
            action.AssertEqual(getUser_response2.GetValue("JobTitle").ToString(), "tester_user");
            action.AssertEqual(getUser_response2.GetValue("PhoneMobile").ToString(), "734633344");
            action.AssertEqual(getUser_response2.GetValue("Language").ToString(), "en");
            var response4 = userAPI.UpdateUserProfile(user_ID, "test", "test", "test", "0000000", "fr");
        }


        [TestMethod]
        [TestCategory("API")]
        public void SAM132_API_UpdateUserProfile_Negative()
        {
            Logger.LogInfo("Update User profile API");
            string admin = "Automation_Admin_1201154632@wsa.com";
            RestClientHelper.token_UserName = admin;
            RestClientHelper.token_Password = "Access@123";
            string admin_ID = "91587ba0-6437-44d1-9475-81fde142b6c1";

            Logger.LogInfo("Language Format Incorrect");
            var response1 = userAPI.UpdateUserProfile(admin_ID, "Automation", "company_admin", "tester_admin", "2424242421", "english");
            action.isTrue(!response1.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for Language format");
            action.isTrue(response1.StatusDescription.Contains("Invalid language code: [english]"), response1.StatusDescription);

            Logger.LogInfo("Try to update another user profile in same company");
            var response2 = userAPI.UpdateUserProfile("314b0c86-0752-49b9-b1a0-54b6abf09c73", "Automation", "company_admin", "tester_admin", "2424242421", "en");
            action.isTrue(!response2.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for Language format");
            action.isTrue(response2.StatusDescription.Contains("You don't have access to update this user"), response2.StatusDescription);

            Logger.LogInfo("Try to update local admin profile in child company");
            var response3 = userAPI.UpdateUserProfile("8d43e8fd-390c-48e6-8d84-998d7db1ea93", "Automation", "company_admin", "tester_admin", "2424242421", "en");
            action.isTrue(!response3.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for Language format");
            action.isTrue(response3.StatusDescription.Contains("You don't have access to update this user"), response3.StatusDescription);

            Logger.LogInfo("Try to update local admin profile in different hierachy");
            var response4 = userAPI.UpdateUserProfile("b06ed345-53ff-4055-89db-162f01371778", "Automation", "company_admin", "tester_admin", "2424242421", "en");
            action.isTrue(!response4.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for Language format");
            action.isTrue(response4.StatusDescription.Contains("You don't have access to update this user"), response4.StatusDescription);

            Logger.LogInfo("Try to update user profile which already deleted");

            RestClientHelper.token_UserName = "Automation_Admin_1201171009@wsa.com";
            RestClientHelper.token_Password = "Access@123";
            var response5 = userAPI.UpdateUserProfile("af5d9507-2b0f-4c19-a4fb-b8b9b41923c4", "Automation", "company_admin", "tester_admin", "2424242421", "en");
            action.isTrue(!response5.StatusCode.Equals(HttpStatusCode.NoContent), "Error Message displayed for Language format");
            action.isTrue(response5.StatusDescription.Contains("You don't have access to update this user:[af5d9507-2b0f-4c19-a4fb-b8b9b41923c4] details"), response5.StatusDescription);

        }

        [TestMethod]
        [TestCategory("API")]
        public void SAM141_API_ChangePasswordSelf_Positive()
        {
            Logger.LogInfo("Update User Password Positive Tests");
            Logger.LogInfo("Verify User can update password for self");
            string user1 = "Automation_User_1602094739@wsa.com";
            RestClientHelper.token_UserName = user1;
            RestClientHelper.token_Password = "Access@123";
            var response1 = userAPI.ChangePasswordSelf("Access@123");
            action.isTrue(response1.StatusCode.Equals(HttpStatusCode.NoContent), "User Password updated " + user1);
            
            Logger.LogInfo("Verify local admin can update password for self");
            string admin = "Automation_UK_Admin1@wsa.com";
            RestClientHelper.token_UserName = admin;
            RestClientHelper.token_Password = "Access@123";
            var response2 = userAPI.ChangePasswordSelf("Access@123");
            action.isTrue(response2.StatusCode.Equals(HttpStatusCode.NoContent), "Admin Password updated " + admin);
        }

        [TestMethod]
        [TestCategory("API")]
        public void SAM141_API_ChangePasswordAdmin_Positive()
        {

            string admin = "Automation_Admin_1201154632@wsa.com";
            RestClientHelper.token_UserName = admin;
            RestClientHelper.token_Password = "Access@123";
            Logger.LogInfo("Verify Local Admin can Set Password for User in the same company");
            var response3 = userAPI.ChangePasswordAdmin("8e435bed-44af-4116-ad52-576216ae1b25", "Access@1234","Yes");
            action.isTrue(response3.StatusCode.Equals(HttpStatusCode.NoContent), "Admin updated Password for Automation_User_1801095606@wsa.com");
            /*
            Logger.LogInfo("Verify Local Admin can Set Password for User in the child company");
            var response4 = userAPI.ChangePasswordAdmin("864b9320-c2b7-4972-a40d-de97bc73b13e", "Access@1234", "No");
            action.isTrue(response4.StatusCode.Equals(HttpStatusCode.NoContent), "Admin updated Password for Automation_User_1801095715@wsa.com");

            Logger.LogInfo("Verify Local Admin can Set Password for Local Admin in the same company");
            var response5 = userAPI.ChangePasswordAdmin("b1bf1dd4-34a5-48e9-ac8e-ca855514599e", "Access@1234", "No");
            action.isTrue(response5.StatusCode.Equals(HttpStatusCode.NoContent), "Admin updated Password for Automation_Admin_1801095453@wsa.com");

            Logger.LogInfo("Verify Local Admin can Set Password for Local Admin in the child company");
            var response6 = userAPI.ChangePasswordAdmin("ddb17df1-31db-4466-90a1-966b9c1b8a3e", "Access@1234", "No");
            action.isTrue(response6.StatusCode.Equals(HttpStatusCode.NoContent), "Admin updated Password for Automation_Admin_1801095643@wsa.com");
            */
            
        }

        [TestMethod]
        [TestCategory("API")]
        public void SAM141_API_ChangePassword_Negative()
        {
            Logger.LogInfo("Update User Password Negative Tests");


            string admin = "Automation_Admin_1801093000@wsa.com";
            RestClientHelper.token_UserName = admin;
            RestClientHelper.token_Password = "Access@123";

            Logger.LogInfo("Verify Password can't be updated when new password not met the requirment");
            var response1 = userAPI.ChangePasswordSelf("access1234");
            action.isTrue(!response1.StatusCode.Equals(HttpStatusCode.NoContent), "User Password not updated when not met the requirment ");
            action.isTrue(response1.StatusDescription.Contains("Password is not strong! A strong password would contain 8-16 characters"), response1.StatusDescription);


            Logger.LogInfo("Verify Local Admin cannot Set Password for User of parent company");
            var response2 = userAPI.ChangePasswordAdmin("864b9320-c2b7-4972-a40d-de97bc73b13e","Access@1234","No");
            action.isTrue(!response2.StatusCode.Equals(HttpStatusCode.NoContent), "User Password not updated as expected ");
            action.isTrue(response2.StatusDescription.Contains("You don't have access to set the password for the user"), response2.StatusDescription);

           
            Logger.LogInfo("Verify User cannot update Password if the profile already deleted");
            var response6 = userAPI.ChangePasswordAdmin("5bd1a648-c67c-41e6-bb61-174464f839b5", "Access@1234", "No");
            action.isTrue(!response6.StatusCode.Equals(HttpStatusCode.NoContent), "User Password not updated as expected ");
            action.isTrue(response6.StatusDescription.Contains("User [5bd1a648-c67c-41e6-bb61-174464f839b5] not found"), response6.StatusDescription);
        }
      

        [TestMethod]
        [TestCategory("API")]
        public void SAM128_API_ApproveCustomer()
        {
            Logger.LogInfo("Test Appove User API");

            SetUp.NavigateToURL("AM");
            string User1 = "Automation_PendingUser_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            AMUser.createPendingUser("64735634", User1);
            SetUp.NavigateToURL("AM");
            string User2 = "Automation_PendingUser_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            AMUser.createPendingUser("3463453", User2);
           

            RestClientHelper.token_UserName = "Automation_UK_Admin2@wsa.com";
            RestClientHelper.token_Password = "Access@123";

            Logger.LogInfo("Test Appove User from Parent company");
            var getUser_response1 = externalAPI.getUserByEmail(User1, "");
            string user1_Id = getUser_response1.GetValue("Id").ToString();
            var response1 = userAPI.approveUser(user1_Id);
            action.isTrue(response1.StatusCode.Equals(HttpStatusCode.Forbidden), "User not Approved " + User1);
            action.isTrue(response1.StatusDescription.ToString().Contains("You aren't authorized to approve the user"), ""+ response1.StatusDescription.ToString());

             Logger.LogInfo("Test Decline User API with wrong user ID");
            var response4 = userAPI.approveUser("345235235", "No");
            action.isTrue(response4.StatusCode.Equals(HttpStatusCode.BadRequest), "Invalid user ID");
           
            RestClientHelper.token_UserName = "Automation_User_1602094739@wsa.com";
            RestClientHelper.token_Password = "Access@123";
            Logger.LogInfo("Test Appove User from User login");
            var getUser_response3 = externalAPI.getUserByEmail(User2, "");
            string user3_Id = getUser_response1.GetValue("Id").ToString();
            var response3 = userAPI.approveUser(user3_Id);
            action.isTrue(response3.StatusCode.Equals(HttpStatusCode.Forbidden), "User not Approved " + User1);
            action.isTrue(response3.Content.ToString().Contains("You don't have access to this action method"), "" + response3.StatusDescription.ToString());

            RestClientHelper.token_UserName = "Automation_UK_Admin1@wsa.com";
            RestClientHelper.token_Password = "Access@123";

            Logger.LogInfo("Appove User from same company");
            var getUser_response4 = externalAPI.getUserByEmail(User1, "");
            string user4_Id = getUser_response1.GetValue("Id").ToString();
            var response5 = userAPI.approveUser(user4_Id);
            action.isTrue(response5.StatusCode.Equals(HttpStatusCode.NoContent), "User Approved " + User1);

            Logger.LogInfo("Appove User from child company");
            var getUser_response5 = externalAPI.getUserByEmail(User2, "");
            string user5_Id = getUser_response5.GetValue("Id").ToString();
            var response6 = userAPI.approveUser(user5_Id, "No");
            action.isTrue(response6.StatusCode.Equals(HttpStatusCode.NoContent), "User Approved " + User2);

            Logger.LogInfo("Test Appove Already Approved User");
            var response7 = userAPI.approveUser(user4_Id);
            action.isTrue(response7.StatusCode.Equals(HttpStatusCode.BadRequest), "User already approved ");
            action.isTrue(response7.Content.ToString().Contains("Request has already been handled by Local Administrator"), "" + response7.StatusDescription.ToString());

            Logger.LogInfo("Test Appove Already declined User");
            var response8 = userAPI.approveUser("27358830-cee4-4155-8bc6-a9c33a78e54e");
            action.isTrue(response8.StatusCode.Equals(HttpStatusCode.NotFound), "User already declined ");
            action.isTrue(response8.Content.ToString().Contains("not found!"), "" + response8.StatusDescription.ToString());

            Logger.LogInfo("Test Appove deleted User");
            var response9 = userAPI.approveUser("ca05b6d4-db3a-4abe-bb91-1f6d3b7b6fa2");
            action.isTrue(response9.StatusCode.Equals(HttpStatusCode.NotFound), "User already deleted");
            action.isTrue(response9.Content.ToString().Contains("User [ca05b6d4-db3a-4abe-bb91-1f6d3b7b6fa2] not found"), "" + response9.StatusDescription.ToString());

            Logger.LogInfo("Test Appove Not existing User");
            var response10 = userAPI.approveUser("ca05b6d4-db3a-4abe-bb91-1f6d3b7b6fa1");
            action.isTrue(response10.StatusCode.Equals(HttpStatusCode.NotFound), "User not exist ");
            action.isTrue(response10.Content.ToString().Contains("not found"), "" + response10.StatusDescription.ToString());

        }


        [TestMethod]
        [TestCategory("API")]
        public void SAM150_API_DeclineCustomer()
        {
            Logger.LogInfo("Test Decline User API");
            SetUp.NavigateToURL("AM");
            string User1 = "Automation_PendingUser_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            AMUser.createPendingUser("64735634", User1);
            SetUp.NavigateToURL("AM");
            string User2 = "Automation_PendingUser_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            AMUser.createPendingUser("3463453", User2);


            RestClientHelper.token_UserName = "Automation_UK_Admin2@wsa.com";
            RestClientHelper.token_Password = "Access@123";

            Logger.LogInfo("Test Decline User from Parent company");
            var getUser_response1 = externalAPI.getUserByEmail(User1, "");
            string user1_Id = getUser_response1.GetValue("Id").ToString();
            var response1 = userAPI.declineUser(user1_Id);
            action.isTrue(response1.StatusCode.Equals(HttpStatusCode.Forbidden), "User not declined " + User1);
            action.isTrue(response1.StatusDescription.ToString().Contains("You aren't authorized to decline the user"), "" + response1.StatusDescription.ToString());

            Logger.LogInfo("Test Decline User API with wrong user ID");
            var response4 = userAPI.declineUser("345235235", "No");
            action.isTrue(response4.StatusCode.Equals(HttpStatusCode.BadRequest), "Invalid user ID");

            RestClientHelper.token_UserName = "Automation_User_1602094739@wsa.com";
            RestClientHelper.token_Password = "Access@123";
            Logger.LogInfo("Test Decline User from User login");
            var getUser_response3 = externalAPI.getUserByEmail(User2, "");
            string user3_Id = getUser_response1.GetValue("Id").ToString();
            var response3 = userAPI.declineUser(user3_Id);
            action.isTrue(response3.StatusCode.Equals(HttpStatusCode.Forbidden), "User not declined " + User1);
            //action.isTrue(response3.Content.ToString().Contains("You aren't authorized to decline the user"), "" + response3.StatusDescription.ToString());

            RestClientHelper.token_UserName = "Automation_UK_Admin1@wsa.com";
            RestClientHelper.token_Password = "Access@123";

            Logger.LogInfo("Decline User from same company");
            var getUser_response4 = externalAPI.getUserByEmail(User1, "");
            string user4_Id = getUser_response1.GetValue("Id").ToString();
            var response5 = userAPI.declineUser(user4_Id);
            action.isTrue(response5.StatusCode.Equals(HttpStatusCode.NoContent), "User Declined " + User1);
            var getUser_response1_4 = externalAPI.getUserByEmail_Negative(User1, "");
            action.isTrue(getUser_response1_4.StatusCode.Equals(HttpStatusCode.NotFound), "User Deleted after Decline ");

            Logger.LogInfo("Decline User from child company");
            var getUser_response5 = externalAPI.getUserByEmail(User2, "");
            string user5_Id = getUser_response5.GetValue("Id").ToString();
            var response6 = userAPI.declineUser(user5_Id, "No");
            action.isTrue(response6.StatusCode.Equals(HttpStatusCode.NoContent), "User Declined " + User2);
            var getUser_response5_1 = externalAPI.getUserByEmail_Negative(User2, "");
            action.isTrue(getUser_response5_1.StatusCode.Equals(HttpStatusCode.NotFound), "User Deleted after Decline ");

            Logger.LogInfo("Test Decline Already declined User");
            var response8 = userAPI.approveUser(user4_Id);
            action.isTrue(response8.StatusCode.Equals(HttpStatusCode.NotFound), "User already declined ");
            action.isTrue(response8.Content.ToString().Contains(" not found!"), "" + response8.StatusDescription.ToString());

           
            Logger.LogInfo("Test Decline deleted User");
            var response9 = userAPI.declineUser("ca05b6d4-db3a-4abe-bb91-1f6d3b7b6fa2");
            action.isTrue(response9.StatusCode.Equals(HttpStatusCode.NotFound), "User already deleted");
            action.isTrue(response9.Content.ToString().Contains("User [ca05b6d4-db3a-4abe-bb91-1f6d3b7b6fa2] not found"), "" + response9.StatusDescription.ToString());

            Logger.LogInfo("Test Decline Not existing User");
            var response10 = userAPI.declineUser("ca05b6d4-db3a-4abe-bb91-1f6d3b7b6fa1");
            action.isTrue(response10.StatusCode.Equals(HttpStatusCode.NotFound), "User not exist ");
            action.isTrue(response10.Content.ToString().Contains("not found"), "" + response10.StatusDescription.ToString());

        }


        [TestMethod]
        public void CreateUsers()
        {
            Logger.LogInfo("Update User Password Negative Tests");

            Logger.LogInfo("Create User with Approve");
            string password = "Access@123";

            //This script cover create , update and delete user API 

            //Parent Company 1 Admin and User creation
            string parentCompany1 = "58977999";// "985643542";
            Logger.LogInfo("Create Local Admin Widex Brand Parent Company 1");
            string adminEmail_PC1 = "Automation_Admin_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response1 = userAPI.createUserAndApprove("LocalAdmin", adminEmail_PC1, "IND", true, parentCompany1,
                new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(response1.StatusCode.Equals(HttpStatusCode.Created), "User Created " + adminEmail_PC1);
            var getUser_response1 = externalAPI.getUserByEmail(adminEmail_PC1, "");
            action.AssertEqual(getUser_response1.GetValue("Email").ToString(), adminEmail_PC1);
            action.AssertEqual(getUser_response1.GetValue("Brands").ToString(), "Widex");
            string adminEmail_PC1_Id = getUser_response1.GetValue("Id").ToString();            //

            //Reset Password
            Logger.LogInfo("Reset Password for Local Admin");
            Operation.Login();
            Operation.selectBrand("Widex");
            Operation.openUserTab();
            Operation.updatePasswordForCustomer(adminEmail_PC1, password);


            Logger.LogInfo("Create User1 and Verify for Parent company 1");
            RestClientHelper.token_UserName = adminEmail_PC1;
            RestClientHelper.token_Password = password;
            IDS_API_External_GET.newToken = true;
            string user1_Email = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response2 = userAPI.createUserAndApprove("User", user1_Email, "IND", true, parentCompany1,
                new List<string> { "06cefeed-11f0-47a1-84f8-45bcd683788d" });
            action.isTrue(response2.StatusCode.Equals(HttpStatusCode.Created), "User Created " + user1_Email);
            var getUser_response2 = externalAPI.getUserByEmail(user1_Email, "");
            action.AssertEqual(getUser_response2.GetValue("Email").ToString(), user1_Email);
            Assert.AreEqual(getUser_response2.GetValue("Active").ToString(), "True");
            action.isTrue(getUser_response2.GetValue("Active").ToString().Contains("True"), "User is Active");
            string user1_ID = getUser_response2.GetValue("Id").ToString();
            Logger.LogInfo("Reset Password for Local Admin");
            Operation.updatePasswordForCustomer(user1_Email, password);

            //Child Company 1 Admin and User Creation
            string childCompany1 = "83454252";// "835624523";
            Logger.LogInfo("Create Local Admin Widex Brand Child Company 1");
            string adminEmail_CC1 = "Automation_Admin_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response3 = userAPI.createUserAndApprove("LocalAdmin", adminEmail_CC1, "IND", true, childCompany1,
                new List<string> { "06cefeed-11f0-47a1-84f8-45bcd683788d" });
            action.isTrue(response3.StatusCode.Equals(HttpStatusCode.Created), "User Created " + adminEmail_CC1);
            var getUser_response3 = externalAPI.getUserByEmail(adminEmail_CC1, "");
            action.AssertEqual(getUser_response3.GetValue("Email").ToString(), adminEmail_CC1);
            action.AssertEqual(getUser_response3.GetValue("Brands").ToString(), "Widex");
            string adminEmail_CC1_Id = getUser_response3.GetValue("Id").ToString();

            //Reset Password
            Logger.LogInfo("Reset Password for Local Admin 1");
            Operation.updatePasswordForCustomer(adminEmail_CC1, password);

            Logger.LogInfo("Create User2 and Verify for Child company 1");
            string user2_Email = "Automation_User_" + DateTime.Now.ToString("ddMMHHmmss") + "@wsa.com";
            var response4 = userAPI.createUserAndApprove("User", user2_Email, "IND", false, childCompany1,
                new List<string> { "a55d5738-6b9d-4517-8dc3-632f9191c8eb" });
            action.isTrue(response4.StatusCode.Equals(HttpStatusCode.Created), "User Created " + user2_Email);
            var getUser_response4 = externalAPI.getUserByEmail(user2_Email, "");
            action.AssertEqual(getUser_response4.GetValue("Email").ToString(), user2_Email);
            Assert.IsFalse(getUser_response4.ContainsKey("Active"));
            string user2_Id = getUser_response4.GetValue("Id").ToString();
        }


     
        public void SAM256_API_GetCulture()
        {
            RestClientHelper.token_UserName = "shop_testuserfr@widex.com";
            RestClientHelper.token_Password = "Access@123";

            Logger.LogInfo("Get Language List by Token");
            var response1 = externalAPI.getCulture();
            action.isTrue(response1.GetValue("succeeded").ToString().Equals(true), "Get All list of language success");
            Assert.IsTrue(response1.ContainsKey("data"));
            Assert.IsTrue(response1.ContainsKey("languageTag"));
            Assert.IsTrue(response1.ContainsKey("languageName"));
            Assert.IsTrue(response1.ContainsKey("isDefault"));
        }




        }

    }
