﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Configuration;
using System.IO;
using Newtonsoft.Json.Linq;
using ShopXP_AM.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;

namespace ShopXP_AM
{
    public class IDS_GET_ShippingAddress
    {
        public RestClient _restClient;
        public RestRequest _restRequest;
        string baseUrl = ConfigurationManager.AppSettings["APIBaseUrl"].ToString();
        
        RestClientHelper restHelp = new RestClientHelper();


        public void setHeader()
        {
            _restRequest.AddHeader("Authorization", "Bearer " + restHelp.getAccessToken());
            _restRequest.AddHeader("Ocp-Apim-Subscription-Key", "28d3f066e2d7479ea0e67c6af2b8499d");
            _restRequest.AddHeader("X-Space", "Widex");
        }

        public JObject getShippingAddress(string CompanyID,string skip,string take,string search)
        {
            string resourceUrl = "/ids-extmgt-query/v1/api/external/query/company/"+CompanyID+"/children/shippingaddresses?";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.GET);
            setHeader();
            _restRequest.AddParameter("skip", skip);
            _restRequest.AddParameter("take", take);
            if (search != null)
            {
                _restRequest.AddParameter("search", search);
            }
            Logger.LogPass("Method Type  GET");
            IRestResponse response = _restClient.Execute(_restRequest);
            var jsonObj = JObject.Parse(response.Content);
            Logger.LogPass("Status Code :" + response.StatusCode);
           
            Assert.IsTrue(response.StatusCode.Equals(HttpStatusCode.OK), "The Post request is not successfully processed");
            Logger.LogPass("Response :" + jsonObj.ToString());
            string count = jsonObj.GetValue("Count").ToString();
            return jsonObj;
        }


    }
}
