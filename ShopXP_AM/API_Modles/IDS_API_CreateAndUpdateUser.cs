﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Configuration;
using System.IO;
using Newtonsoft.Json.Linq;
using ShopXP_AM.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using Newtonsoft.Json;
using System.Runtime.InteropServices;

namespace ShopXP_AM
{
    public class IDS_API_CreateAndUpdateUser
    {
        public RestClient _restClient;
        public RestRequest _restRequest;
        string baseUrl = ConfigurationManager.AppSettings["APIBaseUrl"].ToString();
        
      
     


        RestClientHelper restHelp = new RestClientHelper();


        public void setHeader([Optional]string newToken)
        {
            string accessToken;
            if (newToken == null)
            {
                newToken = "Yes";
            }
           if (newToken.Contains("No"))
           {
                 accessToken = RestClientHelper.accessToken;
            }
            else
            {
                 accessToken = restHelp.getAccessToken_WSA();
            }

           
           
            _restRequest.AddHeader("Authorization", "Bearer " + accessToken);
            _restRequest.AddHeader("Ocp-Apim-Subscription-Key", "28d3f066e2d7479ea0e67c6af2b8499d");
            //_restRequest.AddHeader("Content-Type", "application/json");
        }

        public void setPostBody(string userID,string userType, string email, string country, Boolean active, string company, List<string> roles)
        {
            string givenName;
            string surName;
            List<string> scope;
            List<string> role;
            if (userType.Contains("LocalAdmin"))
            {
                givenName = "Local Admin";
                surName = "AUT";
                scope = new List<string> { "28", "2","25" };
            }
            else
            { 
                givenName = "user";
                surName = "AUT";
                scope = new List<string> { "28"};
            }

            var body = new
            {
                Id = "",
                Email = email,
                GivenName = givenName,
                SurName = surName,
                Country = country,
                PhoneMobile = "8626251235",
                Language = "en",
                Active = active,
                Company = company,
                Scopes = scope,
                Roles = roles
            };
            _restRequest.AddJsonBody(body);
           // string jsonString = JsonConvert.SerializeObject(body);
           // _restRequest.AddParameter("application/json", jsonString, ParameterType.RequestBody);
        }


        public void setPostBody_UpdateProfile(string ID,string givenName,string surName,string title,string number, string language)
        {
            var body = new
            {
                Id = ID,
                GivenName = givenName,
                SurName = surName,
                JobTitle = title,
                PhoneMobile = number,
                Language = language
            };
            _restRequest.AddJsonBody(body);
        }



        public IRestResponse createUserAndApprove(string userType,string email,string country, Boolean active,string company, List<string> roles)
        {
            string resourceUrl = "/ids-extmgt-command/v1/api/external/commands/user/approved";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.POST);
            setHeader();
            setPostBody("",userType, email,country,active,company,roles);

            Logger.LogPass("Method Type  POST");
            IRestResponse response = _restClient.Execute(_restRequest);
            string msg = response.StatusDescription;
            string msg2 = response.Content.ToString();
            
            Logger.LogPass("Status Code :" + response.StatusCode);
            //Assert.IsTrue(response.StatusCode.Equals(HttpStatusCode.Created), "The GET request is not successfully processed");
            //Logger.LogPass("Response :" + msg2);
            return response;
        }

        public IRestResponse UpdateUserWithApprove(string userID, string userType, string email, string country, Boolean active, string company, List<string> roles)
        {
            string resourceUrl = "/ids-extmgt-command/v1/api/external/commands/user/"+ userID + "/approved";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.PATCH);
            setHeader();
            setPostBody(userID, userType, email, country, active, company, roles);

            Logger.LogPass("Method Type  Patch");
            IRestResponse response = _restClient.Execute(_restRequest);
            string msg = response.StatusDescription;
            string msg2 = response.Content.ToString();

            Logger.LogPass("Status Code :" + response.StatusCode);
            //Assert.IsTrue(response.StatusCode.Equals(HttpStatusCode.Created), "The GET request is not successfully processed");
            //Logger.LogPass("Response :" + msg2);
            return response;
        }

        public IRestResponse deleteUser(string userID,[Optional] string newtoken)
        {
            string resourceUrl = "/ids-extmgt-command/v1/api/external/commands/user/" + userID;
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.DELETE);
            setHeader(newtoken);
           
            Logger.LogPass("Method Type  Delete");
            IRestResponse response = _restClient.Execute(_restRequest);
            string msg = response.StatusDescription;
            string msg2 = response.Content.ToString();

            Logger.LogPass("Status Code :" + response.StatusCode);
            //Assert.IsTrue(response.StatusCode.Equals(HttpStatusCode.Created), "The GET request is not successfully processed");
            //Logger.LogPass("Response :" + msg2);
            return response;
        }

        public IRestResponse UpdateUserProfile(string userID, string givenName, string surName, string title, string number, string language)
        {
            string resourceUrl = "/ids-extmgt-command/v1/api/external/commands/user/" + userID;
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.PATCH);
            setHeader();
            setPostBody_UpdateProfile(userID, givenName, surName, title, number, language);

            Logger.LogPass("Method Type PATCH");
            IRestResponse response = _restClient.Execute(_restRequest);
            string msg = response.StatusDescription;
            string msg2 = response.Content.ToString();

            Logger.LogPass("Status Code :" + response.StatusCode);
            //Assert.IsTrue(response.StatusCode.Equals(HttpStatusCode.Created), "The GET request is not successfully processed");
            //Logger.LogPass("Response :" + msg2);
            return response;
        }

        public IRestResponse ChangePasswordSelf(string newPassword)
        {
            string resourceUrl = "/ids-extmgt-command/v1/api/external/commands/user/setpassword";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.PUT);
            setHeader();
            var body = new
            {
                Password = newPassword
            };
            _restRequest.AddJsonBody(body);
            Logger.LogPass("Method Type PUT");
            IRestResponse response = _restClient.Execute(_restRequest);
            string msg = response.StatusDescription;
            string msg2 = response.Content.ToString();

            Logger.LogPass("Status Code :" + response.StatusCode);
            return response;
        }

        public IRestResponse ChangePasswordAdmin(string userId, string newPassword, [Optional]string newtoken)
        {
            string resourceUrl = "/ids-extmgt-command/v1/api/external/commands/user/"+userId+"/setpassword";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.PUT);
            setHeader(newtoken);
            var body = new
            {
                Password = newPassword
            };
            _restRequest.AddJsonBody(body);
            Logger.LogPass("Method Type PUT");
            IRestResponse response = _restClient.Execute(_restRequest);
            string msg = response.StatusDescription;
            string msg2 = response.Content.ToString();

            Logger.LogPass("Status Code :" + response.StatusCode);
            return response;
        }

        public IRestResponse approveUser(string userId,[Optional] string token)
        {
            string resourceUrl = "/ids-extmgt-command/v1/api/external/commands/user/" + userId + "/approve";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.PUT);
            setHeader(token);
            
            Logger.LogPass("Method Type PUT");
            IRestResponse response = _restClient.Execute(_restRequest);
            string msg = response.StatusDescription;
            string msg2 = response.Content.ToString();

            Logger.LogPass("Status Code :" + response.StatusCode);
            //Assert.IsTrue(response.StatusCode.Equals(HttpStatusCode.Created), "The GET request is not successfully processed");
            //Logger.LogPass("Response :" + msg2);
            return response;
        }

        public IRestResponse declineUser(string userId, [Optional] string token)
        {
            string resourceUrl = "/ids-extmgt-command/v1/api/external/commands/user/" + userId + "/decline";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.PUT);
            setHeader(token);

            Logger.LogPass("Method Type PUT");
            IRestResponse response = _restClient.Execute(_restRequest);
            string msg = response.StatusDescription;
            string msg2 = response.Content.ToString();

            Logger.LogPass("Status Code :" + response.StatusCode);
            //Assert.IsTrue(response.StatusCode.Equals(HttpStatusCode.Created), "The GET request is not successfully processed");
            //Logger.LogPass("Response :" + msg2);
            return response;
        }


    }
}
