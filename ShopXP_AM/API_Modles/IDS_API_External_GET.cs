﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Configuration;
using System.IO;
using Newtonsoft.Json.Linq;
using ShopXP_AM.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;

namespace ShopXP_AM
{
    public class IDS_API_External_GET
    {
        public RestClient _restClient;
        public RestRequest _restRequest;
        string baseUrl = ConfigurationManager.AppSettings["APIBaseUrl"].ToString();
        static int firstTime = 0;
        public static Boolean newToken =false;





        RestClientHelper restHelp = new RestClientHelper();


        public void setHeader()
        {
            
            string token= RestClientHelper.accessToken;
            int getTime = Int32.Parse(DateTime.Now.ToString("HHmmss"));
            if (firstTime == 0 ||(getTime - firstTime > 500)|| newToken==true)
            {
                firstTime = Int32.Parse(DateTime.Now.ToString("HHmmss"));
                token = restHelp.getAccessToken_WSA();
                newToken = false;
            }
            
             //token = restHelp.getAccessToken_WSA();
            _restRequest.AddHeader("Authorization", "Bearer " + token);
            _restRequest.AddHeader("Ocp-Apim-Subscription-Key", "28d3f066e2d7479ea0e67c6af2b8499d");
            _restRequest.AddHeader("X-Space", "Widex");
        }

        public void setHeader2()
        {

            string token = RestClientHelper.accessToken;
            int getTime = Int32.Parse(DateTime.Now.ToString("HHmmss"));
            if (firstTime == 0 || (getTime - firstTime > 500) || newToken == true)
            {
                firstTime = Int32.Parse(DateTime.Now.ToString("HHmmss"));
                token = restHelp.getAccessToken_WSA();
                newToken = false;
            }

            //token = restHelp.getAccessToken_WSA();
            _restRequest.AddHeader("Authorization", "Bearer " + token);
           
           
        }

        public JObject getShippingAddress(string skip,string take,string search)
        {
            string resourceUrl = "/ids-extmgt-query/v1/api/external/query/companies/children/shippingaddresses?";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.GET);
            setHeader();
            _restRequest.AddParameter("skip", skip);
            _restRequest.AddParameter("take", take);
            if (search != null)
            {
                _restRequest.AddParameter("search", search);
            }
            Logger.LogPass("Method Type  GET");
            IRestResponse response = _restClient.Execute(_restRequest);
            var jsonObj = JObject.Parse(response.Content);
            Logger.LogPass("Status Code :" + response.StatusCode);
           
            Assert.IsTrue(response.StatusCode.Equals(HttpStatusCode.OK), "The GET request is not successfully processed");
            Logger.LogPass("Response :" + jsonObj.ToString());
            string count = jsonObj.GetValue("Count").ToString();
            return jsonObj;
        }

        public IRestResponse getShippingAddress_Negative( string skip, string take)
        {
            string resourceUrl = "/ids-extmgt-query/v1/api/external/query/companies/children/shippingaddresses?";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.GET);
            setHeader();
            _restRequest.AddParameter("skip", skip);
            _restRequest.AddParameter("take", take);
           
            Logger.LogPass("Method Type  GET");
            IRestResponse response = _restClient.Execute(_restRequest);
          
            return response;
        }


        public JObject getRolesByAppID(string AppID, string skip, string take)
        {
            string resourceUrl = "/ids-extmgt-query/v1/api/external/query/roles/getbyappid/" + AppID + "?";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.GET);
            setHeader();
            _restRequest.AddParameter("skip", skip);
            _restRequest.AddParameter("take", take);
            Logger.LogPass("Method Type  GET");
            IRestResponse response = _restClient.Execute(_restRequest);
            var jsonObj = JObject.Parse(response.Content);
            Logger.LogPass("Status Code :" + response.StatusCode);

            Assert.IsTrue(response.StatusCode.Equals(HttpStatusCode.OK), "The GET request is not successfully processed");
            Logger.LogPass("Response :" + jsonObj.ToString());
            string count = jsonObj.GetValue("Count").ToString();
            return jsonObj;
        }

        public IRestResponse getRolesByAppID_Negative(string AppID, string skip, string take)
        {
            string resourceUrl = "/ids-extmgt-query/v1/api/external/query/roles/getbyappid/" + AppID + "?";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.GET);
            setHeader();
            _restRequest.AddParameter("skip", skip);
            _restRequest.AddParameter("take", take);
            Logger.LogPass("Method Type  GET");
            IRestResponse response = _restClient.Execute(_restRequest);
           
            return response;
        }

        public JObject getUserByEmail(string email, string appId)
        {
            string resourceUrl = "/ids-extmgt-query/v1/api/external/query/users/get?";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.GET);
            setHeader();
            _restRequest.AddParameter("id", email);
            _restRequest.AddParameter("appId", appId);
            Logger.LogPass("Method Type  GET");
            IRestResponse response = _restClient.Execute(_restRequest);
            var jsonObj = JObject.Parse(response.Content);
            Logger.LogPass("Status Code :" + response.StatusCode);

            Assert.IsTrue(response.StatusCode.Equals(HttpStatusCode.OK), "The GET request is not successfully processed");
            Logger.LogPass("Response :" + jsonObj.ToString());
            return jsonObj;
        }

        public IRestResponse getUserByEmail_Negative(string email, string appId)
        {
            string resourceUrl = "/ids-extmgt-query/v1/api/external/query/users/get?";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.GET);
            setHeader();
            _restRequest.AddParameter("id", email);
            _restRequest.AddParameter("appId", appId);
            Logger.LogPass("Method Type  GET");
            IRestResponse response = _restClient.Execute(_restRequest);
            
            Logger.LogPass("Status Code :" + response.StatusCode);

            //Assert.IsTrue(response.StatusCode.Equals(HttpStatusCode.OK), "The GET request is not successfully processed");
           
            return response;
        }

        public JObject ReturnAllTheUser(Dictionary<string,string> par)
        {
            string resourceUrl = "/ids-extmgt-query/v1/api/external/query/users/getonfields?";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.GET);
            Logger.LogPass("Method Type  GET");
           
           
            _restRequest.AddParameter("skip", par["skip"]);
            _restRequest.AddParameter("take", par["take"]);
            //_restRequest.AddParameter("appId", par["appId"]);
            _restRequest.AddParameter("customerNumbers", par["customerNumbers"]);
            _restRequest.AddParameter("roleIds", par["roleIds"]);
            _restRequest.AddParameter("status", par["status"]);
            _restRequest.AddParameter("search", par["search"]);
            Logger.LogPass("Request Parameter :" + string.Join(" ,  ", _restRequest.Parameters));

            setHeader();
            IRestResponse response = _restClient.Execute(_restRequest);
            var jsonObj = JObject.Parse(response.Content);
            Logger.LogPass("Status Code :" + response.StatusCode);

            Assert.IsTrue(response.StatusCode.Equals(HttpStatusCode.OK), "The GET request is not successfully processed");
            Logger.LogPass("Response :" + jsonObj.ToString());
            return jsonObj;
        }

        public IRestResponse ReturnAllTheUser_Negative(Dictionary<string, string> par)
        {
            string resourceUrl = "/ids-extmgt-query/v1/api/external/query/users/getonfields?";
            var Url = Path.Combine(baseUrl + resourceUrl);
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.GET);
            Logger.LogPass("Method Type  GET");
           
            _restRequest.AddParameter("skip", par["skip"]);
            _restRequest.AddParameter("take", par["take"]);
            //_restRequest.AddParameter("appId", par["appId"]);
            _restRequest.AddParameter("customerNumbers", par["customerNumbers"]);
            _restRequest.AddParameter("roleIds", par["roleIds"]);
            _restRequest.AddParameter("status", par["status"]);
            _restRequest.AddParameter("search", par["search"]);
            Logger.LogPass("Request Parameter :" + string.Join(" ,  ", _restRequest.Parameters));

            setHeader();
           
            IRestResponse response = _restClient.Execute(_restRequest);
            string msg = response.StatusDescription;
            string msg2 = response.Content.ToString();
            
            //var jsonObj = JObject.Parse(response.Content);
            Logger.LogPass("Status Code :" + response.StatusCode);

            return response;
        }

        public JObject getCulture()
        {
            string resourceUrl = "https://tst-svc-culture-eu-n-wsa-wa.azurewebsites.net/api/culture/by-market";
            //var Url = Path.Combine(resourceUrl);
            var Url = resourceUrl;
            Logger.LogPass("Request URL :" + Url);
            _restClient = new RestClient(Url);
            _restRequest = new RestRequest(Method.GET);
            _restRequest.AddHeader("Culture-Api-Key", "cdb7b699-20dc-40ef-ae4c-3d6bed56b55d");
            setHeader2();
           
            Logger.LogPass("Method Type  GET");
            IRestResponse response = _restClient.Execute(_restRequest);
            var jsonObj = JObject.Parse(response.Content);
            Logger.LogPass("Status Code :" + response.StatusCode);

            Assert.IsTrue(response.StatusCode.Equals(HttpStatusCode.OK), "The GET request is not successfully processed");
            Logger.LogPass("Response :" + jsonObj.ToString());
            return jsonObj;
        }


    }
}
