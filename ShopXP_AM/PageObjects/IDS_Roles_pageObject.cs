﻿
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using ShopXP_AM.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopXP_AM.PageObjects
{
    public class IDS_Roles_pageObject
    {
      

        public IDS_Roles_pageObject(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//a[text()='Role']")]
        public IWebElement Role_Tab;

        [FindsBy(How = How.XPath, Using = "//a[text()='User']")]
        public IWebElement User_Tab;


        [FindsBy(How = How.XPath, Using = "//a[@id='actionUrl']")]
        public IWebElement Add_Role;

        [FindsBy(How = How.XPath, Using = "//table[@id='tblRoles']//tr")]
        public IList<IWebElement> listOfRoles;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='e.g. Role']")]
        public IWebElement Search_input;

        [FindsBy(How = How.XPath, Using = "//button[text()='Search']")]
        public IWebElement Search_button;

        [FindsBy(How = How.Id, Using = "ApplicationId")]
        public IWebElement ApplicationId_filter;

        [FindsBy(How = How.XPath, Using = "//span[@data-bind='text: PageNumber']")]
        public IWebElement pageNumber;

        [FindsBy(How = How.XPath, Using = "//span[@data-bind='text: TotalPages']")]
        public IWebElement totalPage;

        [FindsBy(How = How.XPath, Using = "//a[text()='Last']//parent::li[1]")]
        public IWebElement Last;

        [FindsBy(How = How.XPath, Using = "//a[text()='First']//parent::li[1]")]
        public IWebElement First;

        [FindsBy(How = How.XPath, Using = "//a[text()='Previous']//parent::li[1]")]
        public IWebElement Previous;

        [FindsBy(How = How.XPath, Using = "//a[text()='Next']//parent::li[1]")]
        public IWebElement Next;

        [FindsBy(How = How.XPath, Using = "//span[@data-toggle='tooltip']")]
        public IWebElement tooltip;

        [FindsBy(How = How.Id, Using = "txtName")]
        public IWebElement RollNAme_txt;

        [FindsBy(How = How.Id, Using = "txtDescription")]
        public IWebElement RollDescription_txt;

        [FindsBy(How = How.Id, Using = "tblDecision")]
        public IWebElement scope_table;

        [FindsBy(How = How.Id, Using = "ddnApplication")]
        public IWebElement application_select;

        [FindsBy(How = How.Id, Using = "btnDelete")]
        public IWebElement delete_Role;

        [FindsBy(How = How.Id, Using = "btnSave")]
        public IWebElement save_Role;

        [FindsBy(How = How.Id, Using = "btnCancel")]
        public IWebElement cancel_Role;

        [FindsBy(How = How.Id, Using = "chkbox1")]
        public IWebElement scopeCheckBox;

        [FindsBy(How = How.XPath, Using = "//label[@for='ddnApplication']//following::label[1]")]
        public IWebElement applicationName_lbl;

        [FindsBy(How = How.XPath, Using = "//div[text()='Are you sure you want to delete?']")]
        public IWebElement deleteRolePopUp;

        [FindsBy(How = How.XPath, Using = "//div[@class='ui-dialog-buttonset']/button[text()='Cancel']")]
        public IWebElement deletePopUp_Cancel;

        [FindsBy(How = How.XPath, Using = "//div[@class='ui-dialog-buttonset']/button[text()='Confirm']")]
        public IWebElement deletePopUp_Confirm;

        [FindsBy(How = How.XPath, Using = "//input[@id='userIdentifier']")]
        public IWebElement email_Input;

        [FindsBy(How = How.XPath, Using = "//button[text()='Continue']")]
        public IWebElement continue_btn;

        [FindsBy(How = How.Id, Using = "txtFirstName")]
        public IWebElement firstName_input;

        [FindsBy(How = How.Id, Using = "ddnCountry")]
        public IWebElement country_select;

        [FindsBy(How = How.Id, Using = "txtLastName")]
        public IWebElement lastName_input;

        [FindsBy(How = How.Id, Using = "ddnLanguage")]
        public IWebElement Language_select;

        [FindsBy(How = How.Id, Using = "lnkAddRole")]
        public IWebElement addRole_lnk;

        [FindsBy(How = How.Id, Using = "lnkAddRole")]
        public IWebElement selectRole_lnk;

        [FindsBy(How = How.Id, Using = "lnkAddCompany")]
        public IWebElement lnkAddCompany;

        [FindsBy(How = How.XPath, Using = "//button[text()='Save']")]
        public IWebElement save_btn;

        [FindsBy(How = How.XPath, Using = "//input[contains(@data-bind,'PartialNameOrEmail')]")]
        public IWebElement user_Search;

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Search')]")]
        public IWebElement Search_btn;


        [FindsBy(How = How.XPath, Using = "//a[contains(@href,'edit')]")]
        public IWebElement edit_user;

        [FindsBy(How = How.XPath, Using = "//span[@class='li-delete-action']")]
        public IWebElement deleteScope;

        [FindsBy(How = How.XPath, Using = "//button[text()='Ok']")]
        public IWebElement ok_btn;


    }
}
