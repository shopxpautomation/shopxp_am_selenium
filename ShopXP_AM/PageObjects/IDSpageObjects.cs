﻿
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using ShopXP_AM.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopXP_AM.PageObjects
{
    public class IDSpageObjects 
    {
      

        public IDSpageObjects(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "email")]
        public IWebElement Username;

        [FindsBy(How = How.Id, Using = "logonIdentifier")]
        public IWebElement Username2;

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement Password;

        [FindsBy(How = How.Id, Using = "next")]
        public IWebElement singIn;

        [FindsBy(How = How.Id, Using = "ddlBrandList")]
        public IWebElement BrandList;

        [FindsBy(How = How.XPath, Using = "//a[text()='Company']")]
        public IWebElement Company_Tab;

        [FindsBy(How = How.Id, Using = "actionUrl")]
        public IWebElement AddCompanyLink;

        [FindsBy(How = How.XPath, Using = "//a[@class='dropdown-toggle pull-left']")]
        public IWebElement ImportCompany;

        [FindsBy(How = How.XPath, Using = "//input[contains(@data-bind,'LocalCustomerNumber')]")]
        public IWebElement CustomerNumber;

        [FindsBy(How = How.XPath, Using = "//button[text()='Continue']")]
        public IWebElement continue_Button;
        
        [FindsBy(How = How.XPath, Using = "//button[text()='Yes']")]
        public IWebElement YES_Button;


        [FindsBy(How = How.Id, Using = "txtName")]
        public IWebElement CompanyName_txt;

        [FindsBy(How = How.Id, Using = "ddnCountry")]
        public IWebElement country_select;

        [FindsBy(How = How.XPath, Using = "(//input[@name='Street'])[2]")]
        public IWebElement Street_txt;

        [FindsBy(How = How.XPath, Using = "(//input[@name='ZipCode'])[2]")]
        public IWebElement ZipCode_txt;

        [FindsBy(How = How.XPath, Using = "(//input[@name='City'])[2]")]
        public IWebElement City_txt;

        [FindsBy(How = How.XPath, Using = "(//input[@name='State'])[2]")]
        public IWebElement State_txt;

        [FindsBy(How = How.XPath, Using = "(//select[@name='Country'])[3]")]
        public IWebElement DomainCountry_select;

        [FindsBy(How = How.Id, Using = "DispenserType")]
        public IWebElement DispenserType_select;

        [FindsBy(How = How.Id, Using = "lnkAssignParentCopany")]
        public IWebElement AssignParentCopany;

        [FindsBy(How = How.XPath, Using = "//input[contains(@placeholder,'customer number')]")]
        public IWebElement searchCompany_txt;

        [FindsBy(How = How.XPath, Using = "//button[text()='Search']")]
        public IWebElement searchCompany_btn;

        [FindsBy(How = How.XPath, Using = "//span[text()='WidexMultipleCompanyParent']//preceding::input[1]")]
        public IWebElement companyselect_checkbox;
        

        [FindsBy(How = How.XPath, Using = "//span[text()='A1 Hearing Aid Centre']//preceding::input[1]")]
        public IWebElement selectParentIndiaCompany;

        [FindsBy(How = How.XPath, Using = "//button[text()='Select']")]
        public IWebElement selectParentCompany;

        [FindsBy(How = How.Id, Using = "DispenserType")]
        public IWebElement DispenserType;

        [FindsBy(How = How.Id, Using = "btnDelete")]
        public IWebElement company_Delete;
        

        [FindsBy(How = How.XPath, Using = "//button[text()='Save']")]
        public IWebElement Save_btn;

        [FindsBy(How = How.XPath, Using = "//button[text()='Ok']")]
        public IWebElement OK_btn;

        [FindsBy(How = How.XPath, Using = "//a[text()='Role']")]
        public IWebElement Role_Tab;

        [FindsBy(How = How.XPath, Using = "//a[text()='User']")]
        public IWebElement User_Tab;

        [FindsBy(How = How.XPath, Using = "//input[contains(@data-bind,'PartialNameOrEmail')]")]
        public IWebElement UserSearch_Input;

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Search')]")]
        public IWebElement UserSearch_btn;

        [FindsBy(How = How.Id, Using = "txtPassword")]
        public IWebElement NewPassword;

        [FindsBy(How = How.Id, Using = "txtConfirmPassword")]
        public IWebElement ConfirmPassword;

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Save')]")]
        public IWebElement UserSave_btn;

        [FindsBy(How = How.XPath, Using = "//div[@class='language-selector__select-label']")]
        public IWebElement language_select;

        [FindsBy(How = How.Id, Using = "forgotPassword")]
        public IWebElement forgotPassword;

        [FindsBy(How = How.Id, Using = "btnCreateUser")]
        public IWebElement createUser;

        [FindsBy(How = How.XPath, Using = "//a[text()='Sign out']")]
        public IWebElement Logout;

        [FindsBy(How = How.XPath, Using = "//a[text()='Application']")]
        public IWebElement Application_Tab;

        [FindsBy(How = How.XPath, Using = "//table[@class='table table-bordered table-striped']")]
        public IWebElement application_Table;

        [FindsBy(How = How.XPath, Using = "//table[@class='table table-bordered table-striped']//td[1]/a/span")]
        public IList<IWebElement> application_List;

        [FindsBy(How = How.XPath, Using = "//table[@class='table table-bordered table-striped']//td[2]/a/span")]
        public IList<IWebElement> applicationScope_List;

        

        [FindsBy(How = How.XPath, Using = "//input[@type='text']")]
        public IWebElement application_input;

        [FindsBy(How = How.XPath, Using = "//button[contains(text(),'Search')]")]
        public IWebElement application_search;

        [FindsBy(How = How.XPath, Using = "//a[text()='Add Application']")]
        public IWebElement add_application;

        [FindsBy(How = How.Id, Using = "ddnStrategy")]
        public IWebElement strategy_Select;

        [FindsBy(How = How.XPath, Using = "//input[@name='Name']")]
        public IWebElement application_Name;

        [FindsBy(How = How.XPath, Using = "//input[@name='Namespace']")]
        public IWebElement application_NameSpace;

        [FindsBy(How = How.XPath, Using = "//button[@name='btnSave']")]
        public IWebElement save_button;

        [FindsBy(How = How.XPath, Using = "//button[@name='btnDelete']")]
        public IWebElement delete_Application;

        [FindsBy(How = How.XPath, Using = "//button[text()='Cancel']")]
        public IWebElement cancel_Button;

        [FindsBy(How = How.Id, Using = "ddnBrand")]
        public IWebElement application_Brand;

        [FindsBy(How = How.Id, Using = "ApplicationId")]
        public IWebElement Application_Select;

        [FindsBy(How = How.XPath, Using = "//a[text()='Permission Scope']")]
        public IWebElement PermissionScope_Tab;

        [FindsBy(How = How.XPath, Using = "//a[text()='Add Permission Scope']")]
        public IWebElement AddPermissionScope;

        [FindsBy(How = How.Id, Using = "txtScopeName")]
        public IWebElement ScopeName_Input;

        [FindsBy(How = How.Id, Using = "txtDescription")]
        public IWebElement ScopeDesc_Input;

        [FindsBy(How = How.Id, Using = "txtNamespace")]
        public IWebElement ScopeNameSpace_Input;

        [FindsBy(How = How.Id, Using = "ddnApplication")]
        public IWebElement Application_Select2;

        [FindsBy(How = How.XPath, Using = "//button[text()='Delete Permission Scope']")]
        public IWebElement Delete_PermissionScope;

        [FindsBy(How = How.XPath, Using = "//input[contains(@placeholder,'Permission Scope')]")]
        public IWebElement scope_search_input;

        [FindsBy(How = How.XPath, Using = "//input[contains(@placeholder,'Permission Scope')]")]
        public IWebElement scope_search_input2;

        [FindsBy(How = How.XPath, Using = "//a[text()='Strategy']")]
        public IWebElement strategy_Tab;

        [FindsBy(How = How.XPath, Using = "//a[text()='Add Strategy']")]
        public IWebElement AddStrategy_Link;

        [FindsBy(How = How.Id, Using = "txtName")]
        public IWebElement strategy_Name;

        [FindsBy(How = How.Id, Using = "txtDescription")]
        public IWebElement strategy_Description;

        [FindsBy(How = How.Id, Using = "tblDecision")]
        public IWebElement Decision_Table;

        [FindsBy(How = How.Id, Using = "btnDelete")]
        public IWebElement delete_Strategy;

        [FindsBy(How = How.Id, Using = "btnAnalyse")]
        public IWebElement Analyze_link;

        [FindsBy(How = How.Id, Using = "dlgAnalyse")]
        public IWebElement Analyze_Table;

        [FindsBy(How = How.XPath, Using = "(//button[@type='button'])[last()]")]
        public IWebElement closeMark;

        [FindsBy(How = How.XPath, Using = "//input[contains(@placeholder,'e-mail')]")]
        public IWebElement userSearchInput;

        [FindsBy(How = How.XPath, Using = "//table[@class='table table-bordered table-striped']//td[2]/a/span")]
        public IList<IWebElement> userList_links;

        [FindsBy(How = How.Id, Using = "Country")]
        public IWebElement county_select;

        [FindsBy(How = How.XPath, Using = "//select[contains(@data-bind,'UserStatus')]")]
        public IWebElement AllUser_select;

        [FindsBy(How = How.XPath, Using = "//a[text()='Add User']")]
        public IWebElement Add_User_Link;

        [FindsBy(How = How.XPath, Using = "//input[contains(@data-bind,'Email, event')]")]
        public IWebElement Email_input;


        [FindsBy(How = How.XPath, Using = "//button[text()='Clear']")]
        public IWebElement clear_button;


        [FindsBy(How = How.XPath, Using = "//button[text()='Remove Brand']")]
        public IWebElement remove_Brand;

        [FindsBy(How = How.XPath, Using = "//button[@name='btnDelete']")]
        public IWebElement Delete_User;

        [FindsBy(How = How.Id, Using = "CompanyType")]
        public IWebElement companyType_Select;

        [FindsBy(How = How.XPath, Using = "//a[text()='Tasks']")]
        public IWebElement Tasks_Tab;

        [FindsBy(How = How.XPath, Using = "//table[@class='table table-bordered table-striped']//td[5]/a")]
        public IWebElement Task_Details;

        [FindsBy(How = How.Id, Using = "//button[text()='Approve']")]
        public IWebElement Approve_Button;

        [FindsBy(How = How.Id, Using = "//button[text()='Decline']")]
        public IWebElement Decline_Button;

        [FindsBy(How = How.Name, Using = "TaskType")]
        public IWebElement TaskType;

        

        //table[@id='tblDecision']//td[6]

    }
}
