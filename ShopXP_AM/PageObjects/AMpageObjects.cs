﻿
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using ShopXP_AM.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopXP_AM.PageObjects
{
    public class AMpageObjects
    {
      

        public AMpageObjects(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Search by user name or email']")]
        public IWebElement userSearch_Input;

        [FindsBy(How = How.Id, Using = "add-user")]
        public IWebElement addUser;

        [FindsBy(How = How.XPath, Using = "//table[@class='user-list-table']")]
        public IWebElement userListTable;

        [FindsBy(How = How.XPath, Using = "//button[@aria-label='clear']")]
        public IWebElement clearSearch;

        [FindsBy(How = How.XPath, Using = "//td[@class='delete-user']/button")]
        public IList<IWebElement> deleteUser;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Search Shop']")]
        public IWebElement searchShops;

        [FindsBy(How = How.Id, Using = "add-user")]
        public IWebElement addUsera;

        [FindsBy(How = How.XPath, Using = "//table[@class='user-list-table']")]
        public IWebElement userListTablea;

        [FindsBy(How = How.XPath, Using = "//table[@class='user-list-table']//tr/td[1]/p")]
        public IList<IWebElement> userNamesColoumn;

        [FindsBy(How = How.XPath, Using = "//p[text()='Role']//following::div[1]//p[@class='echo-typo--body checkbox-label']")]
        public IList<IWebElement> ListOfRoles;

        

        [FindsBy(How = How.XPath, Using = "//button[@aria-label='clear']")]
        public IWebElement searchClear;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'shop-filter-options ')]//input")]
        public IWebElement shopFiler;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'total-users-label')]/p")]
        public IWebElement totalUser;

        [FindsBy(How = How.XPath, Using = "//table[@class='user-list-table']//tr/td[3]/p")]
        public IList<IWebElement> RoleColoumn;

        [FindsBy(How = How.XPath, Using = "//table[@class='user-list-table']//tr/td[4]/p")]
        public IList<IWebElement> statusColoumn;

        [FindsBy(How = How.XPath, Using = "//table[@class='user-list-table']//tr/td[2]/p")]
        public IList<IWebElement> emailColoumn;

        [FindsBy(How = How.XPath, Using = "//table[@class='user-list-table']//tr/td[6]/button")]
        public IList<IWebElement> actionColoumn;


        [FindsBy(How = How.XPath, Using = "//button[text()='1']")]
        public IWebElement page1;

        [FindsBy(How = How.XPath, Using = "//button[text()='3']")]
        public IWebElement page3;

        [FindsBy(How = How.XPath, Using = "//button[text()='2']")]
        public IWebElement page2;

        [FindsBy(How = How.XPath, Using = "//button[text()='5']")]
        public IWebElement page5;

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'first-page')]")]
        public IWebElement firstPage_button;

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'previous-page')]")]
        public IWebElement previousPage_button;

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'next-page')]")]
        public IWebElement nextPage_button;

        [FindsBy(How = How.XPath, Using = "//button[contains(@class,'last-page')]")]
        public IWebElement lastPage_button;

        [FindsBy(How = How.XPath, Using = "(//button[contains(@class,'page-numbers')])[last()]")]
        public IWebElement lastPageNumber;

        [FindsBy(How = How.XPath, Using = "//input[@name='emailIdSearch']")]
        public IWebElement emailIdSearch_Input;

        [FindsBy(How = How.XPath, Using = "//button[text()='Check Email']")]
        public IWebElement checkEmail_btn;

        [FindsBy(How = How.XPath, Using = "//button[text()='Cancel']")]
        public IWebElement Cancel_btn;

        [FindsBy(How = How.XPath, Using = "//button[text()='Save']")]
        public IWebElement Save_btn;

        [FindsBy(How = How.XPath, Using = "//button[text()='Ok']")]
        public IWebElement ok_btn;

        [FindsBy(How = How.XPath, Using = "//input[@name='Addresssearch']")]
        public IWebElement addressSearch_input;

        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Search Shop']")]
        public IWebElement addressSearch_input2;

        [FindsBy(How = How.XPath, Using = "//input[@type='radio']")]
        public IList<IWebElement> address_Radio;

        [FindsBy(How = How.XPath, Using = "//p[text()='Online Shop']//following::button[1]")]
        public IWebElement role_select;

        [FindsBy(How = How.XPath, Using = "//button[text()='Add User']")]
        public IWebElement Add_User;

        [FindsBy(How = How.XPath, Using = "//input[@id='firstname']")]
        public IWebElement firstName;

        [FindsBy(How = How.XPath, Using = "//input[@id='lastname']")]
        public IWebElement lastname;

        [FindsBy(How = How.XPath, Using = "//input[@id='phoneNumber']")]
        public IWebElement phoneNumber;

        [FindsBy(How = How.XPath, Using = "//input[@id='jobtitle']")]
        public IWebElement jobtitle;

        [FindsBy(How = How.XPath, Using = "//div[@id='myuserProfile-language']/button")]
        public IWebElement Language_button;

        [FindsBy(How = How.XPath, Using = "//li[@id='myuserProfile-language-en']")]
        public IWebElement Language_en;

        

        [FindsBy(How = How.XPath, Using = "//input[@class='echo-switch__box']")]
        public IWebElement statusInput;

        [FindsBy(How = How.XPath, Using = "//p[text()='Success']")]
        public IWebElement successMessage;

        [FindsBy(How = How.XPath, Using = "//button[text()='Delete']")]
        public IWebElement Delete_Button;

        [FindsBy(How = How.XPath, Using = "//button[@aria-label='close']")]
        public IWebElement msgBarClose;

        [FindsBy(How = How.Id, Using = "btnCreateUser")]
        public IWebElement loginCreateUser;

        [FindsBy(How = How.Id, Using = "txtFirstName")]
        public IWebElement create_FirstName;

        [FindsBy(How = How.Id, Using = "txtLastName")]
        public IWebElement create_LastName;

        [FindsBy(How = How.Id, Using = "txtEmail")]
        public IWebElement create_email;

        [FindsBy(How = How.Id, Using = "ddlLanguage")]
        public IWebElement create_Laungauge;

        [FindsBy(How = How.Id, Using = "ddlCountry")]
        public IWebElement create_County;

        [FindsBy(How = How.Id, Using = "txtCompany")]
        public IWebElement create_Company;

        [FindsBy(How = How.XPath, Using = "//button[contains(@data-bind,'FilterCompanies')]")]
        public IWebElement create_filterCompanies;

        [FindsBy(How = How.XPath, Using = "//button[contains(@data-bind,'Submit')]")]
        public IWebElement RegisterUser;

        [FindsBy(How = How.XPath, Using = "//div[@class='umf-close']")]
        public IWebElement Close_RegisterSuccess;
        

        [FindsBy(How = How.XPath, Using = "//div[@class='myUserProfile-available-address-options']/ul")]
        public IList<IWebElement> ListOfShipTo;

        [FindsBy(How = How.XPath, Using = "//p[@class='echo-typo--body change-password-text']")]
        public IWebElement changePasswordLink;

        [FindsBy(How = How.Id, Using = "passwordInput")]
        public IWebElement newPassword;

        [FindsBy(How = How.Id, Using = "changePasswordInput")]
        public IWebElement confirmNewPassword;

        [FindsBy(How = How.XPath, Using = "//button[text()='Change Password']")]
        public IWebElement changePassword_Button;

        [FindsBy(How = How.XPath, Using = " //div[@class='echo-dialog__close']//*")]
        public IWebElement changePassword_close;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'echo-tooltip echo-tooltip')]//p")]
        public IWebElement Pending_Tooltip;

        [FindsBy(How = How.XPath, Using = "//button[text()='Approve']")]
        public IWebElement Approve_button;

        [FindsBy(How = How.XPath, Using = "//button[text()='Decline']")]
        public IWebElement Decline_button;

    }
}
