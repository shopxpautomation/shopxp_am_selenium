﻿using ShopXP_AM.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ShopXP_AM.Utility;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace ShopXP_AM.PageOperations
{
    public class AM_Profile_pageOperation : SetUp
    {
        AMpageObjects userobj;
        PageOperationBase action;
        SikuliOperations sikuli;

        public AM_Profile_pageOperation(IWebDriver driver)
        {
            userobj = new AMpageObjects(driver);
            action = new PageOperationBase(driver);
            sikuli = new SikuliOperations();
        }


        public void navigateToUserProfile()
        {
            driver.Navigate().GoToUrl("https://tst-accm.widex.com/profile");
            action.waitForPageLoad();
        }
        public void verifyProfilePage()
        {
            //sikuli.verifyImageExists("Widex_Profile.PNG");
            action.checkElementDisplaybyText("My User Profile");
            action.checkElementDisplaybyText("Personal Information");
            action.checkElementDisplaybyText("Access To Functionalities");
            action.checkElementDisplaybyText("Available Addresses");
            action.checkElementDisplaybyText("Online Shop");
            action.checkElementDisplay(userobj.firstName, "First Name");
            action.checkElementDisplay(userobj.lastname, "Last Name");
            action.checkElementDisplay(userobj.phoneNumber, "Phone Number");
            action.checkElementDisplay(userobj.jobtitle, "Job Title");
            action.checkElementDisplay(userobj.Language_button, "Lauguage");
            action.checkElementDisplay(userobj.changePasswordLink, "Change Password Link");
           
            action.checkElementDisplay(userobj.role_select, "Role select");
            action.isTrue(userobj.role_select.Enabled == false, "Role button disabled");
            action.checkElementDisplay(userobj.Save_btn, "Save Button");
            action.isTrue(userobj.Save_btn.Enabled == false, "Save button disabled");
            action.isTrue(userobj.ListOfShipTo.Count > 1,"Admin contains multiple shipto");
            
            Logger.LogPass("Number of shipto dispalying : " + userobj.ListOfShipTo.Count);
        }

        public void editProfilePage()
        {
            Logger.LogInfo("Edit User Profile");
            action.isTrue(driver.Url.Contains("/profile"), "URL Contains /profile");
            //action.checkElementDisplaybyText("Shop User");
            //action.isTrue(userobj.ListOfShipTo.Count==2, "One shipto available as expected");
            string fName = "Test_" + DateTime.Now.ToString("ddMMHHmmss");
            action.enterText(userobj.firstName, fName, "First Name");
            action.enterText(userobj.lastname, "Automation Test 2", "Last Name");
            action.enterText(userobj.phoneNumber, "34624525224", "Phone Number");
            action.enterText(userobj.jobtitle, "Developer", "Job Title");
            action.clickByJavaScript(userobj.Language_button, "Languege button");
            action.clickByJavaScript(userobj.Language_en, "English");
            action.clickByJavaScript(userobj.Save_btn, "Save button");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            action.checkElementDisplay(userobj.successMessage, "Success Message");
            action.clickByJavaScript(userobj.msgBarClose, "Close message bar");
          
            Logger.LogInfo("Veify Edited User");
            action.isTrue(userobj.firstName.GetAttribute("Value").Contains(fName), "First Name : " + fName);
            action.isTrue(userobj.lastname.GetAttribute("Value").Contains("Automation Test 2"), "Last Name : Automation Test 2");
            action.isTrue(userobj.phoneNumber.GetAttribute("Value").Contains("34624525224"), "Phone Number : 34624525224");
            action.isTrue(userobj.jobtitle.GetAttribute("Value").Contains("Developer"), "Job Title : Developer");
            action.checkElementDisplaybyText("English (English)");
            Logger.LogSnapShot("User Profile edited successfully", driver);
        }

        public void verifyChangePasswordProfile()
        {
            Logger.LogInfo("Verify Change Password Flow");
            action.checkElementDisplaybyText("Change password");
            action.clickByJavaScript(userobj.changePasswordLink, "Change Password Link");
            action.checkElementDisplaybyText("Password must be between 8 and 16 characters long and contain three of the following 4 items: upper case letter, lower case letter, a symbol, a number");
            action.checkElementDisplay(userobj.newPassword,"New Password");
            action.checkElementDisplay(userobj.confirmNewPassword, "Confirm Password");
            action.checkElementDisplaybyText("Change Password");
            action.clickOnObject(userobj.changePassword_close, "Close");
            action.waitForPageLoad();
            action.checkElementDisplay(userobj.firstName, "User Profile");
        }

        public void changePasswordProfile()
        {
            Logger.LogInfo("Verify Change Password Flow");
            action.clickByJavaScript(userobj.changePasswordLink, "Change Password Link");
            action.enterText(userobj.newPassword, "Access@1234", "New Password");
            action.enterText(userobj.confirmNewPassword, "Access@1234", "Confirm Password");
            Thread.Sleep(5000);
            action.clickOnObject(userobj.changePassword_Button, "Change Password");
            //action.clickByJavaScript(userobj.changePassword_Button, "Change Password");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            action.checkElementDisplay(userobj.successMessage, "Success Message");
            action.clickByJavaScript(userobj.msgBarClose, "Close message bar");

            action.clickByJavaScript(userobj.changePasswordLink, "Change Password Link");
            action.enterText(userobj.newPassword, "Access@123", "New Password");
            action.enterText(userobj.confirmNewPassword, "Access@123", "Confirm Password");
            Thread.Sleep(5000);
            action.clickOnObject(userobj.changePassword_Button, "Change Password");
            //action.clickByJavaScript(userobj.changePassword_Button, "Change Password");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            action.checkElementDisplay(userobj.successMessage, "Success Message");
            action.clickByJavaScript(userobj.msgBarClose, "Close message bar");


        }

        public void verifyAccessDenied()
        {
            action.checkElementDisplaybyText("Access Denied");
            action.isTrue(driver.Url.Contains("forbidden"), "URL Navigates to forbidden");
            action.checkElementDisplaybyText("Sorry you are not allowed here");
        }









        }
    }
