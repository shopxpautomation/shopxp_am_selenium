﻿using ShopXP_AM.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ShopXP_AM.Utility;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ShopXP_AM.PageOperations
{
    public class IDSpageOperation :SetUp
    {
        IDSpageObjects IDSobj;
        PageOperationBase action;
        IDS_Roles_pageObject roleObj;
        AMpageObjects userobj;

        public IDSpageOperation(IWebDriver driver)
        {
             IDSobj = new IDSpageObjects(driver);
             action = new PageOperationBase(driver);
            roleObj = new IDS_Roles_pageObject(driver);
            userobj = new AMpageObjects(driver);
        }
        public void Login()
        {
            Thread.Sleep(20000);
            action.waitForPageLoad();
            action.waitForElementToBeVisible(By.Id("hdr"), 2);
            Logger.LogInfo("Navigated to login page, URL: " + url);
            //action.checkElementDisplay(IDSobj.Username, "Login Page");
            action.enterText(IDSobj.Username, ConfigurationManager.AppSettings["UserName"], "User name");
            action.enterText(IDSobj.Password, ConfigurationManager.AppSettings["Password"], "Password");
            action.clickByJavaScript(IDSobj.singIn, "Sigin In");
            action.waitForPageLoad();
            action.checkElementDisplay(IDSobj.BrandList, "Home Page");
        }
        public void Login(string uName,string pwd)
        {
            action.waitForPageLoad();
            action.waitForElementToBeVisible(By.Id("hdr"), 2);
            Logger.LogInfo("Navigated to login page, URL: " + url);
            //action.checkElementDisplay(IDSobj.Username, "Login Page");
            action.enterText(IDSobj.Username, uName, "User name");
            action.enterText(IDSobj.Password, pwd, "Password");
            action.clickByJavaScript(IDSobj.singIn, "Sigin In");
            action.waitForPageLoad();
            //action.checkElementDisplay(IDSobj.BrandList, "Home Page");
        }

        public void Login2(string uName, string pwd)
        {
            action.waitForPageLoad();
           // action.waitForElementToBeVisible(By.Id("hdr"), 2);
            //Logger.LogInfo("Navigated to login page, URL: " + url);
            //action.checkElementDisplay(IDSobj.Username, "Login Page");
            action.enterText(IDSobj.Username, uName, "User name");
            action.enterText(IDSobj.Password, pwd, "Password");
            action.clickByJavaScript(IDSobj.singIn, "Sigin In");
            action.waitForPageLoad();
            action.checkElementDisplay(IDSobj.BrandList, "Home Page");
        }

        public void NavigateTestBlueSpace()
        {
            driver.Navigate().GoToUrl("https://usertest.widex.com");
        }

        public void Login_AM(string userName,string password)
        {
            action.waitForPageLoad();
            action.waitForElementToBeVisible(By.Id("hdr"), 2);
            Logger.LogInfo("Navigated to login page, URL: " + url);
            //action.checkElementDisplay(IDSobj.Username, "Login Page");
            action.enterText(IDSobj.Username, userName, "User name");
            action.enterText(IDSobj.Password, password, "Password");
            action.clickByJavaScript(IDSobj.singIn, "Sigin In");
            action.WaitForPageBodyToLoadFully();
           // action.checkElementDisplay(driver.FindElement(By.XPath("//a[text()='WSA.Microservice.AccountMgmt']")), "Home Page");
        }

        public void selectBrand(string brand)
        {

            action.selectTextFromDropdown(IDSobj.BrandList, brand);
            //action.WaitForPageBodyToLoadFully();
            Thread.Sleep(5000);
            action.waitForPageLoad();
            Logger.LogPass(brand + " Selected from dropdown");
            
        }

        public void openUserTab()
        {
            action.clickByJavaScript(IDSobj.User_Tab, "User Tab");
            Thread.Sleep(5000);
            action.waitForPageLoad();
        }

        public void updatePasswordForCustomer(string email,string pwd)
        {
            action.enterText(IDSobj.UserSearch_Input,email,"email");
            action.clickByJavaScript(IDSobj.UserSearch_btn, "Search");
            Thread.Sleep(5000);
            action.waitForPageLoad();
            IWebElement searchResult = driver.FindElement(By.XPath("//span[text()='"+ email + "']"));
            action.clickByJavaScript(searchResult, "User "+email);
            action.waitForPageLoad();
            action.checkElementDisplay(IDSobj.NewPassword, "User deatils Page");
            action.enterText(IDSobj.NewPassword, pwd, "Password");
            action.enterText(IDSobj.ConfirmPassword, pwd, "Confirm Password");
            Thread.Sleep(3000);
            action.clickByJavaScript(IDSobj.UserSave_btn, "Save");
            action.waitForPageLoad();
            action.clickOnObject(IDSobj.UserSave_btn, "Save");
            Thread.Sleep(6000);
            action.waitForPageLoad();
            action.checkElementDisplay(IDSobj.UserSearch_Input, "User Page");
            Logger.LogPass("User password changed successfully");
        }

        public void AddCompanies(int num)
        {
           

            action.clickByJavaScript(IDSobj.Company_Tab, "Company tab");

            for (int i= 44; i < num; i++)
            {

            string random = DateTime.Now.ToString("HHmmssff");
            string cust_Number = random;
            string companyName = "Company widex Child Automation "+i ;
            string street = "first cross building "+ random;
            action.waitForPageLoad();
            action.waitForItemToBeDisplayed(IDSobj.AddCompanyLink, 4);
            action.clickByJavaScript(IDSobj.AddCompanyLink, "Add Comapny");
            action.waitForPageLoad();
            action.checkElementDisplay(IDSobj.CustomerNumber, "Customer Number");
   
            action.enterText(IDSobj.CustomerNumber, cust_Number, "customer number");
                Thread.Sleep(3000);
                action.clickOnObject(IDSobj.continue_Button, "continue");
            action.waitForPageLoad();
            Thread.Sleep(3000);
            action.checkElementDisplay(IDSobj.CompanyName_txt, "Comapany ");
            action.enterText(IDSobj.CompanyName_txt, companyName,"Comapny Name");
            action.selectTextFromDropdown(IDSobj.country_select, "India");
            action.clickByJavaScript(IDSobj.AssignParentCopany, "parent");
            action.waitForPageLoad();
            action.checkElementDisplay(IDSobj.searchCompany_txt, "Comapany search ");
            action.enterText(IDSobj.searchCompany_txt, "WidexMultipleCompanyParent", "Comapny search");
            action.clickByJavaScript(IDSobj.searchCompany_btn, "search");
            action.waitForPageLoad();

            action.waitForItemToBeVisible(IDSobj.companyselect_checkbox, 2);
            Thread.Sleep(5000);
            action.clickByJavaScript(IDSobj.companyselect_checkbox, "Select Comapny");
            Thread.Sleep(3000);
            action.clickByJavaScript(IDSobj.selectParentCompany, "Select");

            action.waitForPageLoad();
             

            
           
            action.enterText(IDSobj.Street_txt, street, "street");
            action.enterText(IDSobj.ZipCode_txt, "560010", "ZIP code");
            action.enterText(IDSobj.City_txt, "Bnagalore", "City");
            action.enterText(IDSobj.State_txt, "Karnataka", "state");
            action.selectTextFromDropdown(IDSobj.DomainCountry_select, "India");

            action.selectTextFromDropdown(IDSobj.DispenserType, "Branch Office");
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            action.waitForPageLoad();
            Logger.LogPass("Company Created successfully "+companyName);
            }
        }


        public void createUserValidation(string email)
        {
            action.clickByJavaScript(roleObj.Add_Role, "Add User");
            action.enterText(roleObj.email_Input, email, "Email");
            Thread.Sleep(5000);
            action.clickOnObject(roleObj.continue_btn, "Continue button");
            action.waitForPageLoad();
            action.isElementDisplay(roleObj.firstName_input);
            action.enterText(roleObj.firstName_input, "Test", "First Name");
            action.enterText(roleObj.lastName_input, "Aut", "Last Name");

            action.selectTextFromDropdown(roleObj.country_select, "India");
            action.selectTextFromDropdown(roleObj.Language_select, "English (English)");

            action.clickByJavaScript(roleObj.lnkAddCompany, "Add Company");
            
            Thread.Sleep(5000);
            action.clickByJavaScript(IDSobj.selectParentIndiaCompany, "Select Comapny");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Select'])[4]")), "Select");
            action.waitForPageLoad();

            action.clickByJavaScript(roleObj.addRole_lnk, "Add Role");
            IWebElement role1 = driver.FindElement(By.XPath("//span[text()='Business Admin']//preceding::input[1]"));
            IWebElement role2 = driver.FindElement(By.XPath("//span[text()='Company Admin']//preceding::input[1]"));
            

            action.clickByJavaScript(role1, "ShopXP Role");
            action.clickByJavaScript(role2, "ShopXP Role");
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Select'])[2]")), "Select");
            action.waitForPageLoad();

            action.checkElementDisplaybyText2("Only one role per application is allowed for user");
            action.clickByJavaScript(roleObj.ok_btn, "ok");

            action.clickByJavaScript(role2, "ShopXP Role");
            
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Select'])[2]")), "Select");
            action.waitForPageLoad();

            action.checkElementDisplaybyText("Business Admin");
            //action.checkElementDisplaybyText("AUT TEST scope 11");
            action.clickByJavaScript(roleObj.save_btn, "Save");
            action.waitForPageLoad();

            //action.checkElementDisplay(roleObj.Search_input, "User Page");
            action.enterText(roleObj.user_Search, email, "Search");
            action.clickByJavaScript(roleObj.Search_btn, "Search");
            Thread.Sleep(6000);
            action.waitForPageLoad();

            action.clickByJavaScript(IDSobj.userList_links[0], "User Edit");
            action.waitForPageLoad();

            action.checkElementDisplaybyText("Manage Users");
            action.checkElementDisplaybyText("Order Overview");

            action.clickByJavaScript(roleObj.deleteScope, "Delete Manage User");
            Thread.Sleep(5000);
            action.clickByJavaScript(roleObj.selectRole_lnk, "Add Role");
            action.waitForPageLoad();
//            IWebElement role3 = driver.FindElement(By.XPath("//span[text()='AUT TEST scope 11']//preceding::input[1]"));
            //action.clickByJavaScript(role3, "Other Application Role");
         //   action.clickByJavaScript(role3, "Other Application Role");
         //   action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Select'])[2]")), "Select");
            action.waitForPageLoad();
            //action.checkElementDisplaybyText("AUT TEST scope 11");
            action.checkElementDisplaybyText("Business Admin");
            action.clickByJavaScript(roleObj.save_btn, "Save");
            action.waitForPageLoad();

           // action.checkElementDisplay(roleObj.Search_input, "User Page");
            action.enterText(roleObj.user_Search, email, "Search");
            action.clickByJavaScript(roleObj.Search_btn, "Search");
            action.waitForPageLoad();

            action.clickByJavaScript(IDSobj.userList_links[0], "User Edit");
            action.waitForPageLoad();

            //action.isTrue(driver.FindElements(By.XPath("//span[@class='li-delete-action']")).Count==5, "Number of Scope");
        }


        public void verifyCompanyTab(bool   Addcompany,bool importCompany)
        {
            action.clickByJavaScript(IDSobj.Company_Tab, "Company Tab");
            Thread.Sleep(5000);
            action.waitForPageLoad();
            if (Addcompany)
            {
                action.isTrue(action.isElementDisplay(IDSobj.AddCompanyLink).Equals(true), "Add Company Displayed");
            }else
            {
                action.isTrue(action.isElementDisplay(IDSobj.AddCompanyLink).Equals(false), "Add Company Not Displayed");
            }

            if (importCompany)
             action.isTrue(action.isElementDisplay(IDSobj.ImportCompany).Equals(true), "Import and Export Company Displayed");
            else
             action.isTrue(action.isElementDisplay(IDSobj.ImportCompany).Equals(false), "Import and Export Company Not Displayed");
        }

        public void validateClinicData()
        {
            action.isElementDisplay(userobj.loginCreateUser);
            action.clickByJavaScript(userobj.loginCreateUser, "Register User");
            action.waitForPageLoad();
            action.isElementDisplay(userobj.create_FirstName);
            action.selectTextFromDropdown(userobj.create_County, "United Kingdom");

            action.enterText(userobj.create_Company, "", "Company");
            action.clickOnObject(userobj.create_filterCompanies, "Search Company");
            Thread.Sleep(3000);
            //action.isTrue(userobj.create_Company.GetAttribute("title").Contains("Required"), "Error Message");

            action.enterText(userobj.create_Company, "64735631", "Company");
            action.clickOnObject(userobj.create_filterCompanies, "Search Company");
            Thread.Sleep(5000);
            action.isTrue(userobj.create_Company.GetAttribute("title").Contains("No clinic matches your search"), "Error Message");

            action.enterText(userobj.create_Company, "64735634", "Company");
            action.clickOnObject(userobj.create_filterCompanies, "Search Company");
            Thread.Sleep(8000);
            action.isTrue(userobj.create_Company.GetAttribute("title").Equals(""), "No Error Message");

            
        }

        public void verifyIDSLoginPage()
        {
            Thread.Sleep(7000);
            action.checkElementDisplay(IDSobj.Username, "User Name");
            action.checkElementDisplay(IDSobj.Password, "Password");
            action.checkElementDisplay(IDSobj.singIn, "Sign In");
            action.checkElementDisplay(IDSobj.language_select, "language Select");
            action.checkElementDisplay(IDSobj.createUser, "Create User");
            action.checkElementDisplay(IDSobj.forgotPassword, "Forget Password");
          }

        public void verifyLanguageChange_IDSLoginPage()
        {
            action.checkElementDisplay(IDSobj.language_select, "language Select");
            action.clickByJavaScript(IDSobj.language_select, "language");
            action.clickByJavaScript(driver.FindElement(By.XPath("//li[text()='German']")), "German");
            action.waitForPageToComplete();
            action.isTrue(IDSobj.forgotPassword.Text.Contains("Kennwort vergessen?"),"Language changed succesfully");
            action.clickByJavaScript(IDSobj.language_select, "language");
            action.clickByJavaScript(driver.FindElement(By.XPath("//li[text()='English']")), "English");
            action.waitForPageToComplete();
        }

        public void verifyWhenLoginFails()
        {
            Login("Automation_PendingUser_2202132858@wsa.com", "sfs1240");
            Thread.Sleep(4000);
            action.checkElementDisplaybyText("Your password is incorrect.");

            Login("2202132858@wsa.com", "sfs1240");
            Thread.Sleep(4000);
            action.checkElementDisplaybyText("seem to find your account.");
        }

        public void Logou_IDS()
        {
            action.clickByJavaScript(IDSobj.Logout, "Logout");
            action.waitForPageToComplete();
            action.checkElementDisplay(IDSobj.Username, "Login Page");
        }

        public void verify_ApplicationTab()
        {

            action.clickByJavaScript(IDSobj.Application_Tab, "Application Tab");
            action.WaitForPageLoad();
            action.checkElementDisplay(IDSobj.application_Table, "Application Grid dispalyed :" + IDSobj.application_List.Count);
            //check navigation displays
            action.checkElementDisplaybyText("First");
            action.checkElementDisplaybyText("Previous");
            action.checkElementDisplaybyText("Last");
            action.checkElementDisplaybyText("Next");

            selectBrand("Signia");
            int signia = IDSobj.application_List.Count;
            selectBrand("Widex");
            int widex = IDSobj.application_List.Count;
            //Assert.IsFalse(signia == widex);

            searchApplication("Widex Pro site");
            }

        public void verify_ScopeTab()
        {

            action.clickByJavaScript(IDSobj.PermissionScope_Tab, "Permission scope Tab");
            action.WaitForPageLoad();
            action.checkElementDisplay(IDSobj.application_Table, "Scope Grid dispalyed :" + IDSobj.application_List.Count);
            //check navigation displays
            action.checkElementDisplaybyText("First");
            action.checkElementDisplaybyText("Previous");
            action.checkElementDisplaybyText("Last");
            action.checkElementDisplaybyText("Next");

            action.checkElementDisplaybyText("Permission Scope");
            action.checkElementDisplaybyText("Alias");
            action.checkElementDisplaybyText("Application");
            action.checkElementDisplaybyText("Namespace");

            selectBrand("Signia");
            int signia = IDSobj.application_List.Count;
            selectBrand("Widex");
            int widex = IDSobj.application_List.Count;
            Assert.IsTrue(signia == widex);
            //search
            searchscope("lead.admin");
            action.clickByJavaScript(IDSobj.PermissionScope_Tab, "Permission scope Tab");
            action.waitForPageLoad();
            //search by application
            action.selectTextFromDropdown(IDSobj.Application_Select, "EBusiness");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            Thread.Sleep(4000);
            IList<IWebElement> apps = driver.FindElements(By.XPath("//table[@class='table table-bordered table-striped']//td[3]/span"));
            foreach(IWebElement appname  in apps)
            {
                action.isTrue(appname.Text.Contains("EBusiness"),"Scope belongs to application only displayed");
            }

            IWebElement user1 = driver.FindElement(By.XPath("//table[@class='table table-bordered table-striped']//td[5]/a"));
            action.clickByJavaScript(user1, "users");
            action.waitForPageLoad();
            action.checkElementDisplay(IDSobj.application_Table, "User table");
            action.checkElementDisplaybyText("Filtered by Scope");
            Logger.LogPass("Permission scope tab verified succesfullty");
        }

        public void searchApplication(string name)
        {
            action.enterText(IDSobj.application_input, name, "search Input");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
            action.waitForPageToComplete();
            string app = IDSobj.application_List[0].Text;
            action.isTrue(app.Contains(name), "Application search dispalyed");
        }

        public void addNewApplication(string name)
        {
            action.clickByJavaScript(IDSobj.Application_Tab, "Application Tab");
            action.waitForPageLoad();
            action.clickByJavaScript(IDSobj.add_application, "Add New Application");
            action.WaitForSpinnerDisappear();
            action.enterText(IDSobj.application_Name, name, "Application Name");
            action.checkElementDisplay(IDSobj.application_Brand, "Brand");
            action.enterText(IDSobj.application_NameSpace, "Test", "Application Namespace");
            action.clickByJavaScript(IDSobj.save_button, "Save");
            action.waitForPageToComplete();
            searchApplication("AUT_Test");
        }

        public void CancelAddingNewApp()
        {
            action.clickByJavaScript(IDSobj.add_application, "Add New Application");
            action.WaitForSpinnerDisappear();
            action.clickByJavaScript(IDSobj.save_button, "Save");
            action.checkElementDisplaybyText("This field is required");
            action.enterText(IDSobj.application_Name, "AUT_Test2", "Application Name");
            action.checkElementDisplay(IDSobj.application_Brand, "Brand");
            action.enterText(IDSobj.application_NameSpace, "Test", "Application Namespace");
            action.clickByJavaScript(IDSobj.cancel_Button, "Cancel");
            action.WaitForSpinnerDisappear();
            action.enterText(IDSobj.application_input, "AUT_Test2", "search Input");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
            Assert.IsFalse(action.isElementDisplay(IDSobj.application_Table));
        }

        public void editApplication()
        {
            searchApplication("AUT_Test");
            action.clickByJavaScript(IDSobj.application_List[0], "Application Edit");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            action.checkElementDisplay(IDSobj.strategy_Select, "Strategy");
            //edit and cancel application
            action.enterText(IDSobj.application_Name, "AUT_Test_Update", "Application Name");
            action.clickByJavaScript(IDSobj.cancel_Button, "Cancel");
            action.WaitForSpinnerDisappear();
            action.enterText(IDSobj.application_input, "AUT_Test_Update", "search Input");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            Assert.IsFalse(action.isElementDisplay(IDSobj.application_Table));

            //Edit and save application
            searchApplication("AUT_Test");
            action.clickByJavaScript(IDSobj.application_List[0], "Application Edit");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            action.checkElementDisplay(IDSobj.strategy_Select, "Strategy");
            //edit and cancel application
            action.enterText(IDSobj.application_Name, "AUT_Test_Update", "Application Name");
            Thread.Sleep(4000);
            action.enterText(IDSobj.application_NameSpace, "Test", "Application Namespace");
            action.clickByJavaScript(IDSobj.save_button, "Save");
            action.waitForPageToComplete();
            if (action.isElementDisplay(IDSobj.save_button))
            {
                action.clickByJavaScript(IDSobj.save_button, "Save");
                action.waitForPageToComplete();
            }
            
            searchApplication("AUT_Test_Update");
        }

        public void AddSameApplication()
        {
            //Add same application in same brand
            action.clickByJavaScript(IDSobj.add_application, "Add New Application");
            action.WaitForSpinnerDisappear();
            action.enterText(IDSobj.application_Name, "AUT_Test_Update", "Application Name");
            action.checkElementDisplay(IDSobj.application_Brand, "Brand");
            action.enterText(IDSobj.application_NameSpace, "Test", "Application Namespace");
            action.clickByJavaScript(IDSobj.save_button, "Save");
            action.waitForPageToComplete();
            action.checkElementDisplaybyText("An Application with (AUT_Test_Update) already exists");

            //switch brand while edit application
            action.clickByJavaScript(IDSobj.Application_Tab, "Application Tab");
            searchApplication("AUT_Test_Update");
            action.clickByJavaScript(IDSobj.application_List[0], "Application Edit");
            selectBrand("Signia");
            action.waitForPageLoad();
            action.checkElementDisplay(IDSobj.application_Table, "Application table");
            Logger.LogPass("Application grid display as expected after switching brand");

            //Add same application in different brand
            action.clickByJavaScript(IDSobj.add_application, "Add New Application");
            action.WaitForSpinnerDisappear();
            action.enterText(IDSobj.application_Name, "AUT_Test_Update", "Application Name");
            action.checkElementDisplay(IDSobj.application_Brand, "Brand");
            action.enterText(IDSobj.application_NameSpace, "Test", "Application Namespace");
            action.clickByJavaScript(IDSobj.save_button, "Save");
            action.waitForPageToComplete();
            action.checkElementDisplaybyText("An Application with (AUT_Test_Update) already exists");
            action.clickByJavaScript(IDSobj.Application_Tab, "Application Tab");
            selectBrand("Widex");
        }


        public void deleteApplication(string name)
        {
            action.clickByJavaScript(IDSobj.Application_Tab, "Application Tab");
            action.waitForPageLoad();
            searchApplication(name);
            action.clickByJavaScript(IDSobj.application_List[0], "Application Edit");
            action.waitForPageLoad();
            action.clickByJavaScript(IDSobj.delete_Application, "Delete Application");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Cancel'])[last()]")),"Cancle delete");
            Thread.Sleep(3000);
            action.checkElementDisplay(IDSobj.application_Name, "Application Edit");
            action.clickByJavaScript(IDSobj.delete_Application, "Delete Application");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Confirm'])[last()]")), "Confirm delete");
            action.waitForPageLoad();
            action.enterText(IDSobj.application_input, name, "search Input");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
           // Assert.IsFalse(action.isElementDisplay(IDSobj.application_Table));
            action.isTrue(action.isElementDisplay(IDSobj.application_Table) == false,"Search Result displayed");
            Logger.LogPass("Application deleted succesfully");
            //Test
        }

        public void addScopeToApplication(string Scope_Name,string Appname)
        {
            action.clickByJavaScript(IDSobj.PermissionScope_Tab, "Permission Scope Tab");
            action.waitForPageLoad();
            action.checkElementDisplay(IDSobj.AddPermissionScope, "Add Permission scope");
            action.clickByJavaScript(IDSobj.AddPermissionScope, "Add Permission scope");
            action.waitForPageLoad();
            action.enterText(IDSobj.ScopeName_Input, Scope_Name,"Scope Name");
            action.enterText(IDSobj.ScopeNameSpace_Input, "Name", "Scope Name");
            action.selectTextFromDropdown(IDSobj.Application_Select2, Appname);
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            action.waitForPageLoad();
            searchscope(Scope_Name);
        }

        public void searchscope(string name)
        {
            action.enterText(IDSobj.scope_search_input, name,"Search Input ");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
            Thread.Sleep(4000);
            action.waitForPageToComplete();
            string scope = IDSobj.application_List[0].Text;
            action.isTrue(scope.Contains(name), "Scope search dispalyed");

        }

        public void verifyScopeAddedToApplication(string Scope_Name, string Appname)
        {
            action.clickByJavaScript(IDSobj.Application_Tab, "Application Tab");
            action.waitForPageLoad();
            searchApplication(Appname);
            action.clickByJavaScript(IDSobj.applicationScope_List[0], "Scope Link");
            action.waitForPageLoad();
            action.isTrue(IDSobj.application_List[0].Text.Contains(Scope_Name),"Scope List dispalyed as expected");

            action.clickByJavaScript(IDSobj.Application_Tab, "Application Tab");
            action.waitForPageLoad();
            searchApplication(Appname);
            action.clickByJavaScript(IDSobj.application_List[0], "Application Edit");
            action.waitForPageLoad();
            action.clickByJavaScript(IDSobj.delete_Application, "Delete Application");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Confirm'])[last()]")), "Confirm delete");
            action.waitForPageLoad();
            action.checkElementDisplaybyText("Cannot delete this application, please delete all its referenced Scopes");
            Logger.LogPass("Application not deleted");
        }

        public void deleteScope(string Scope_Name)
        {
            action.clickByJavaScript(IDSobj.PermissionScope_Tab, "Permission Scope Tab");
            action.waitForPageLoad();
            searchscope(Scope_Name);
            action.clickByJavaScript(IDSobj.application_List[0], "Scope Edit");
            action.waitForPageLoad();
            action.clickByJavaScript(IDSobj.Delete_PermissionScope, "Delete Scope");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Cancel'])[last()]")), "Cancle delete");
            Thread.Sleep(3000);
            action.checkElementDisplay(IDSobj.ScopeNameSpace_Input, "Scope Edit");

            action.clickByJavaScript(IDSobj.Delete_PermissionScope, "Delete Scope");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Confirm'])[last()]")), "Confirm delete");
            action.waitForPageLoad();
            action.enterText(IDSobj.scope_search_input, Scope_Name, "search Input");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
            Assert.IsFalse(action.isElementDisplay(IDSobj.application_Table));
            Logger.LogPass("Scope deleted succesfully");

        }


        public void Verify_deleteScopeWithStrategy(string Scope_Name)
        {
            action.clickByJavaScript(IDSobj.PermissionScope_Tab, "Permission Scope Tab");
            action.waitForPageLoad();
            searchscope(Scope_Name);
            action.clickByJavaScript(IDSobj.application_List[0], "Scope Edit");
            action.waitForPageLoad();
            action.clickByJavaScript(IDSobj.Delete_PermissionScope, "Delete Scope");
            Thread.Sleep(3000);
            action.checkElementDisplaybyText("");
            Logger.LogPass("Scope not deleted as expected");
        }

        public void CancelAddingNewScope(string Appname)
        {
            action.clickByJavaScript(IDSobj.AddPermissionScope, "Add New scope");
            action.waitForPageToComplete();
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            int error = driver.FindElements(By.XPath("//span[text()='Required']")).Count;
            action.isTrue(error== 3,"Mondratery error displayed as expected");
            action.enterText(IDSobj.ScopeName_Input, "Test_AUT2", "Scope Name");
            action.enterText(IDSobj.ScopeDesc_Input, "Test", "Scope Name");
            action.enterText(IDSobj.ScopeNameSpace_Input, "Name", "Scope Name");
            action.selectTextFromDropdown(IDSobj.Application_Select2, Appname);
            action.clickByJavaScript(IDSobj.cancel_Button, "Cancel");
            action.waitForPageToComplete();
            action.enterText(IDSobj.scope_search_input, "Test_AUT2", "search Input");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.waitForPageToComplete();
            Assert.IsFalse(action.isElementDisplay(IDSobj.application_Table));
            Logger.LogPass("Cancel adding New scope working as expected");
        }

        public void editScope(string scopeName)
        {
            action.clickByJavaScript(IDSobj.PermissionScope_Tab, "Permission Scope Tab");
            action.waitForPageLoad();
            searchscope(scopeName);
            action.clickByJavaScript(IDSobj.application_List[0], "Scope Edit");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            action.checkElementDisplaybyText("Alias");
            //edit and cancel application
            action.enterText(IDSobj.ScopeName_Input, "TestScope_Updated", "Scope Name");
            action.clickByJavaScript(IDSobj.cancel_Button, "Cancel");
            action.waitForPageLoad();
            action.enterText(IDSobj.scope_search_input, "TestScope_Updated", "search Input");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            Assert.IsFalse(action.isElementDisplay(IDSobj.application_Table));

            //Edit and save application
            searchscope(scopeName);
            action.clickByJavaScript(IDSobj.application_List[0], "Scope Edit");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            action.enterText(IDSobj.ScopeName_Input, "TestScope_Updated", "Scope Name");
            Thread.Sleep(4000);
            action.enterText(IDSobj.ScopeNameSpace_Input, "Test2", "Scope Namespace");
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            action.enterText(IDSobj.ScopeDesc_Input, "Test", "Description");
            action.selectTextFromDropdown(IDSobj.Application_Select2, "Customer Portal");
            action.waitForPageToComplete();
            if (action.isElementDisplay(IDSobj.Save_btn))
            {
                action.clickByJavaScript(IDSobj.Save_btn, "Save");
                action.waitForPageToComplete();
            }

            searchscope("TestScope_Updated");
            Logger.LogPass("Edit scope working as expected");
        }

        public void AddSameScope(string Scope_Name,string appName)
        {
            //Add same scope in same application
            action.clickByJavaScript(IDSobj.AddPermissionScope, "Add New scope");
            action.waitForPageToComplete();
            action.enterText(IDSobj.ScopeName_Input, Scope_Name, "Scope Name");
            action.enterText(IDSobj.ScopeNameSpace_Input, "Name", "Scope Name");
            action.selectTextFromDropdown(IDSobj.Application_Select2, appName);
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            action.waitForPageToComplete();
            action.checkElementDisplaybyText("A Scope with (TestScope) already exists");

            //switch brand while edit application
            action.clickByJavaScript(IDSobj.PermissionScope_Tab, "Permission scope Tab");
            action.waitForPageToComplete();
            searchscope(Scope_Name);
            action.clickByJavaScript(IDSobj.application_List[0], "Scope Edit");
            selectBrand("Signia");
            action.waitForPageLoad();
            action.checkElementDisplay(IDSobj.application_Table, "Scope table");
            action.checkElementDisplay(IDSobj.scope_search_input, "Scope search");
            Logger.LogPass("Scope grid display as expected after switching brand");

            //Add same Scope in different Application
            selectBrand("Widex");
            action.clickByJavaScript(IDSobj.AddPermissionScope, "Add New scope");
            action.waitForPageToComplete();
            action.enterText(IDSobj.ScopeName_Input, Scope_Name, "Scope Name");
            action.enterText(IDSobj.ScopeNameSpace_Input, "Name", "Scope Name");
            action.selectTextFromDropdown(IDSobj.Application_Select2, "EBusiness");
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            action.waitForPageToComplete();
            action.checkElementDisplaybyText("A Scope with (TestScope) already exists");
        }

        public void searchStrategy(string name)
        {
            action.enterText(IDSobj.application_input, name, "Search Input ");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
            Thread.Sleep(4000);
            action.waitForPageToComplete();
            string scope = IDSobj.application_List[0].Text;
            action.isTrue(scope.Contains(name), "Strategy search dispalyed");

        }

        public void verify_StrategyTab()
        {

            action.clickByJavaScript(IDSobj.strategy_Tab, "Strategy Tab");
            action.WaitForPageLoad();
            action.checkElementDisplay(IDSobj.application_Table, "Strategy Grid dispalyed :" + IDSobj.application_List.Count);
            //check navigation displays
            action.checkElementDisplaybyText("First");
            action.checkElementDisplaybyText("Previous");
            action.checkElementDisplaybyText("Last");
            action.checkElementDisplaybyText("Next");

            //action.checkElementDisplaybyText("Name");
            
            selectBrand("Signia");
            int signia = IDSobj.application_List.Count;
            selectBrand("Widex");
            int widex = IDSobj.application_List.Count;
            //Assert.IsFalse(signia == widex);
            //search
            searchStrategy("Pro XP Strategy");
            
           
            Logger.LogPass("Strategy tab verified succesfullty");
        }

        public void addNewStrategy(string name,string AppName)
        {
            action.clickByJavaScript(IDSobj.strategy_Tab, "Strategy Tab");
            action.waitForPageLoad();
            action.clickByJavaScript(IDSobj.AddStrategy_Link, "Add New Strategy");
            action.WaitForSpinnerDisappear();
            action.enterText(IDSobj.strategy_Name, name, "Strategy Name");
            action.enterText(IDSobj.strategy_Description, name, "Strategy Description");
            action.selectTextFromDropdown(IDSobj.Application_Select2, AppName);
            Thread.Sleep(4000);
            action.checkElementDisplay(IDSobj.Decision_Table, "Decision Table");
            action.clickByJavaScript(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[1]/input")),"Scope");
            Thread.Sleep(2000);

            Assert.IsFalse(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[3]/label")).GetAttribute("style").Contains("display: none;"));
            //Assert.IsFalse(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[4]/label")).GetAttribute("style").Contains("display: none;"));

            action.clickByJavaScript(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[3]/label/input")), "Active");
            action.clickByJavaScript(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[4]/div/label/input")), "editable");
            //action.clickByJavaScript(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[5]/div/input")), "Hide");

           
            action.clickByJavaScript(IDSobj.save_button, "Save");
            action.waitForPageToComplete();
            searchStrategy(name);
        }

        public void CancelAddingNewStrategy(string AppName)
        {
            action.clickByJavaScript(IDSobj.AddStrategy_Link, "Strategy Link");
            action.waitForPageToComplete();
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            int error = driver.FindElements(By.XPath("//span[text()='This field is required.']")).Count;
            action.isTrue(error == 2, "Mondratery error displayed as expected");
            action.checkElementDisplaybyText("This field is required");
            action.enterText(IDSobj.strategy_Name, "AUT_Strategy", "Strategy Name");
            action.enterText(IDSobj.strategy_Description, "Test", "Strategy Description");
            action.selectTextFromDropdown(IDSobj.Application_Select2, AppName);
            action.clickByJavaScript(IDSobj.save_button, "Save");
            Thread.Sleep(3000);
            //action.checkElementDisplaybyText("Atleast one decision needs to be selected");
            action.checkElementDisplay(IDSobj.OK_btn, "Alert message");
            action.clickByJavaScript(IDSobj.OK_btn, "OK Button");
            action.clickByJavaScript(IDSobj.cancel_Button, "Cancel");
            action.waitForPageToComplete();
            action.enterText(IDSobj.application_input, "AUT_Strategy", "search Input");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.waitForPageToComplete();
            Assert.IsFalse(action.isElementDisplay(IDSobj.application_Table));
            Logger.LogPass("Cancel adding New Strategy working as expected");
        }

        public void editStrategy(string strategyName)
        {
            action.clickByJavaScript(IDSobj.strategy_Tab, "Strategy Tab");
            action.waitForPageLoad();
            searchStrategy(strategyName);
            action.clickByJavaScript(IDSobj.application_List[0], "strategy Edit");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            //action.checkElementDisplaybyText("Alias");
            //edit and cancel application
            action.enterText(IDSobj.strategy_Name, "TestStrategy_Updated", "Scope Name");
            action.clickByJavaScript(IDSobj.cancel_Button, "Cancel");
            action.waitForPageLoad();
            action.enterText(IDSobj.application_input, "TestStrategy_Updated", "search Input");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            Assert.IsFalse(action.isElementDisplay(IDSobj.application_Table));

            //Edit and save application
            searchStrategy(strategyName);
            action.clickByJavaScript(IDSobj.application_List[0], "Scope Edit");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();

            //verify Analyze
            action.clickByJavaScript(IDSobj.Analyze_link, "Analyze Link");
            action.waitForPageLoad();
            action.checkElementDisplay(IDSobj.Analyze_Table, "Analyze Table");
            action.checkElementDisplaybyText("Strategy");
            action.checkElementDisplaybyText("Company");
            //action.checkElementDisplaybyText("User");
            action.clickByJavaScript(IDSobj.closeMark, "Close Analyze");
            action.waitForPageLoad();
            //Edit strategy
            action.enterText(IDSobj.strategy_Name, "TestStrategy_Updated", "Scope Name");
            Thread.Sleep(4000);
            action.enterText(IDSobj.strategy_Description, "Test2", "Scope Namespace");

            //verify decision table
            action.isTrue(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[3]/label")).GetAttribute("style").Contains("display: none;"),"Active disabled");
            //action.isTrue(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[4]/label")).GetAttribute("style").Contains("display: none;"),"Editable disabled");
            //action.isTrue(!driver.FindElement(By.XPath("//table[@id='tblDecision']//td[5]/label")).GetAttribute("style").Contains("display: none;"), "Hide enabled");

            action.clickByJavaScript(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[3]/label/input")), "Active");
            action.clickByJavaScript(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[4]/div/label/input")), "editable");
            //action.clickByJavaScript(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[5]/div/input")), "Hide");

           // action.clickByJavaScript(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[1]/input")), "Scope");

            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            action.waitForPageToComplete();
            if (action.isElementDisplay(IDSobj.Save_btn))
            {
                action.clickByJavaScript(IDSobj.Save_btn, "Save");
                action.waitForPageToComplete();
            }

            searchStrategy("TestStrategy_Updated");
            Logger.LogPass("Edit scope working as expected");
        }

        public void deleteStrategy(string s_Name)
        {
            action.clickByJavaScript(IDSobj.strategy_Tab, "Strategy Tab");
            action.waitForPageLoad();
            searchStrategy(s_Name);
            action.clickByJavaScript(IDSobj.application_List[0], "Scope Edit");
            action.waitForPageLoad();
            action.clickByJavaScript(IDSobj.delete_Strategy, "Delete Strategy");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Cancel'])[last()]")), "Cancle delete");
            Thread.Sleep(3000);
            action.checkElementDisplay(IDSobj.strategy_Name, "Strategy Edit");

            action.clickByJavaScript(IDSobj.delete_Strategy, "Delete Scope");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Confirm'])[last()]")), "Confirm delete");
            action.waitForPageLoad();
            action.enterText(IDSobj.application_input, s_Name, "search Input");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
            Assert.IsFalse(action.isElementDisplay(IDSobj.application_Table));
            Logger.LogPass("Strategy deleted succesfully");

        }

        public void AddSameStrategy(string StrategyName, string appName)
        {
            //Add same strategy in same application
            action.clickByJavaScript(IDSobj.AddStrategy_Link, "Add New Strategy");
            action.waitForPageToComplete();
            action.enterText(IDSobj.strategy_Name, StrategyName, "Strategy Name");
            action.enterText(IDSobj.strategy_Description, "Name", "Strategy Description");
            action.selectTextFromDropdown(IDSobj.Application_Select2, appName);
            action.checkElementDisplay(IDSobj.Decision_Table, "Decision Table");
            action.clickByJavaScript(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[1]/input")), "Scope");
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            action.waitForPageToComplete();
            action.checkElementDisplaybyText("A Scope with (TestScope) already exists");

            //switch brand while edit application
            action.clickByJavaScript(IDSobj.PermissionScope_Tab, "Permission scope Tab");
            action.waitForPageToComplete();
            searchStrategy(StrategyName);
            action.clickByJavaScript(IDSobj.application_List[0], "Strategy Edit");
            selectBrand("Signia");
            action.waitForPageLoad();
            action.checkElementDisplay(IDSobj.application_Table, "Strategy table");
            action.checkElementDisplay(IDSobj.AddStrategy_Link, "Strategy search");
            Logger.LogPass("Strategy grid display as expected after switching brand");


            //Add same strategy in different brand

            action.clickByJavaScript(IDSobj.AddStrategy_Link, "Add New Strategy");
            action.waitForPageToComplete();
            action.enterText(IDSobj.strategy_Name, StrategyName, "Strategy Name");
            action.enterText(IDSobj.strategy_Description, "Name", "Strategy Description");
            action.selectTextFromDropdown(IDSobj.Application_Select2, appName);
            action.checkElementDisplay(IDSobj.Decision_Table, "Decision Table");
            action.clickByJavaScript(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[1]/input")), "Scope");
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            action.waitForPageToComplete();
            action.checkElementDisplaybyText("A Scope with (TestScope) already exists");

            //Add same strategy in different application
            selectBrand("Widex");
            action.clickByJavaScript(IDSobj.AddStrategy_Link, "Add New Strategy");
            action.waitForPageToComplete();
            action.enterText(IDSobj.strategy_Name, StrategyName, "Strategy Name");
            action.enterText(IDSobj.strategy_Description, "Name", "Strategy Description");
            action.selectTextFromDropdown(IDSobj.Application_Select2, "EBusiness");
            action.checkElementDisplay(IDSobj.Decision_Table, "Decision Table");
            action.clickByJavaScript(driver.FindElement(By.XPath("//table[@id='tblDecision']//td[1]/input")), "Scope");
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            action.waitForPageToComplete();
            action.checkElementDisplaybyText("A Scope with (TestScope) already exists");

        }


        public void verify_UserTab()
        {

            action.clickByJavaScript(IDSobj.User_Tab, "User Tab");
            action.WaitForPageLoad();
            action.checkElementDisplay(IDSobj.application_Table, "Scope Grid dispalyed :" + IDSobj.application_List.Count);
            //check navigation displays
            action.checkElementDisplaybyText("First");
            action.checkElementDisplaybyText("Previous");
            action.checkElementDisplaybyText("Last");
            action.checkElementDisplaybyText("Next");

            action.checkElementDisplaybyText("Status");
            action.checkElementDisplaybyText("E-Mail");
            //action.checkElementDisplaybyText("Name");
            action.checkElementDisplaybyText("Company");
            action.checkElementDisplaybyText("Country");
            action.checkElementDisplaybyText("Company type");
            action.checkElementDisplaybyText("Date last updated");

            selectBrand("Signia");
            int signia = IDSobj.application_List.Count;
            selectBrand("Widex");
            int widex = IDSobj.application_List.Count;
            Assert.IsTrue(signia == widex);
            //search
            searchUser("anisa.regmi@wsa.com");
            action.clickByJavaScript(IDSobj.User_Tab, "User Tab");
            action.waitForPageLoad();
            //search by country
            action.selectTextFromDropdown(IDSobj.county_select, "India");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            Thread.Sleep(4000);
            action.waitForPageLoad();
            IList<IWebElement> user = driver.FindElements(By.XPath("//table[@class='table table-bordered table-striped']//td[5]/span"));
            foreach (IWebElement appname in user)
            {
                action.isTrue(appname.Text.Contains("India"), "Only India company user displayed");
            }

            action.selectTextFromDropdown(IDSobj.county_select, "Select Country");
            action.selectTextFromDropdown(IDSobj.AllUser_select, "Inactive Users");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            Thread.Sleep(4000);
            action.waitForPageLoad();
            IList<IWebElement> status = driver.FindElements(By.XPath("//table[@class='table table-bordered table-striped']//td[1]/span"));
            foreach (IWebElement appname in status)
            {
                action.isTrue(appname.Text.Contains("Inactive"), "Only Inactive members displayed");
            }



            Logger.LogPass("User tab verified succesfullty");
        }

        public void searchUser(string name)
        {
            action.enterText(IDSobj.userSearchInput, name, "Search Input ");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
            Thread.Sleep(4000);
            action.waitForPageToComplete();
            string user = IDSobj.userList_links[0].Text;
            action.isTrue(user.Contains(name), "User search dispalyed");
        }

       
        public void cancelAddingUser()
        {
            action.clickByJavaScript(IDSobj.User_Tab, "User Tab");
            action.waitForPageLoad();
            action.clickByJavaScript(IDSobj.Add_User_Link, "Add User");
            //invalid email address
            action.enterText(roleObj.email_Input, "541351351", "Email");
            action.clickOnObject(roleObj.continue_btn, "Continue button");
            action.waitForPageLoad();
            action.checkElementDisplaybyText("Please enter a proper email address");
            //existing email addresss
            action.enterText(roleObj.email_Input, "admin.test@widex.com", "Email");
            action.clickOnObject(roleObj.continue_btn, "Continue button");
            action.waitForPageLoad();
            action.checkElementDisplaybyText("Email already exists.");
           

            //Cancle adding new user
            action.enterText(roleObj.email_Input, "testAut2343@gmail.com", "Email");
            action.clickOnObject(roleObj.continue_btn, "Continue button");
            Thread.Sleep(8000);
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            action.clickByJavaScript(roleObj.save_btn, "Save");
            action.waitForPageLoad();
            action.checkElementDisplaybyText("No Company assigned");
            action.checkElementDisplaybyText("Required");


            action.isElementDisplay(roleObj.firstName_input);
            action.enterText(roleObj.firstName_input, "Test", "First Name");
            action.enterText(roleObj.lastName_input, "Aut", "Last Name");

            action.selectTextFromDropdown(roleObj.country_select, "India");
            action.selectTextFromDropdown(roleObj.Language_select, "English (English)");

            action.clickByJavaScript(roleObj.lnkAddCompany, "Add Company");

            Thread.Sleep(5000);
            action.clickByJavaScript(IDSobj.selectParentIndiaCompany, "Select Comapny");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Select'])[4]")), "Select");
            action.waitForPageLoad();

            action.clickByJavaScript(roleObj.addRole_lnk, "Add Role");
            IWebElement role1 = driver.FindElement(By.XPath("//span[text()='Business Admin']//preceding::input[1]"));
            IWebElement role2 = driver.FindElement(By.XPath("//span[text()='Company Admin']//preceding::input[1]"));


            action.clickByJavaScript(role1, "ShopXP Role");
            action.clickByJavaScript(role2, "ShopXP Role");
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Select'])[2]")), "Select");
            action.waitForPageLoad();

            action.checkElementDisplaybyText2("Only one role per application is allowed for user");
            action.clickByJavaScript(roleObj.ok_btn, "ok");

            action.clickByJavaScript(role2, "ShopXP Role");

            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Select'])[2]")), "Select");
            action.waitForPageLoad();

            action.checkElementDisplaybyText("Business Admin");
            action.clickByJavaScript(IDSobj.cancel_Button, "Cancle button");
            action.waitForPageLoad();
            action.enterText(IDSobj.userSearchInput, "testAut2343@gmail.com", "Search Input ");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
            Thread.Sleep(4000);
            action.waitForPageToComplete();
            int user = IDSobj.userList_links.Count;
            Assert.IsTrue(user == 0);
            Logger.LogPass("User creation cancled as expected");
        }

        public void deleteUser(string user)
        {
            action.clickByJavaScript(IDSobj.User_Tab, "User Tab");
            action.waitForPageLoad();
            searchUser(user);
            action.clickByJavaScript(IDSobj.userList_links[0], "User");
            action.waitForPageLoad();
            /*
            action.clickByJavaScript(IDSobj.remove_Brand, "Remove Brand");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Confirm'])[last()]")), "Confirm delete");
            action.waitForPageLoad();

            searchUser(user);
            action.clickByJavaScript(IDSobj.userList_links[0], "User");
            action.waitForPageLoad();
            */
            action.clickByJavaScript(IDSobj.Delete_User, "Delete User");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Cancel'])[2]")), "Cancle delete");
            Thread.Sleep(3000);
            action.checkElementDisplay(IDSobj.Delete_User, "User Edit");
            action.clickByJavaScript(IDSobj.Delete_User, "Delete User");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Confirm'])")), "Confirm delete");
            action.waitForPageLoad();


            action.enterText(IDSobj.userSearchInput, user, "Search Input ");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
            Thread.Sleep(4000);
            action.waitForPageToComplete();
            int userList = IDSobj.userList_links.Count;
            Assert.IsTrue(userList == 0);
            Logger.LogPass("User deleted as expected");

        }

        public void verify_CompanyTab()
        {

            action.clickByJavaScript(IDSobj.Company_Tab, "Company Tab");
            action.WaitForPageLoad();
            action.checkElementDisplay(IDSobj.application_Table, "Company Grid dispalyed :" + IDSobj.application_List.Count);
            //check navigation displays
            action.checkElementDisplaybyText("First");
            action.checkElementDisplaybyText("Previous");
            action.checkElementDisplaybyText("Last");
            action.checkElementDisplaybyText("Next");

            //action.checkElementDisplaybyText("Name");
            action.checkElementDisplaybyText("Country");
            action.checkElementDisplaybyText("Company type");
            action.checkElementDisplaybyText("Date last updated");

            selectBrand("Signia");
            int signia = IDSobj.application_List.Count;
            selectBrand("Widex");
            int widex = IDSobj.application_List.Count;
            Assert.IsTrue(signia == widex);
            //search

            searchCompany("AB Widex");
            action.clickByJavaScript(IDSobj.Company_Tab, "Company Tab");
            action.waitForPageLoad();
            //search by application
            action.selectTextFromDropdown(IDSobj.county_select, "France");
            action.clickByJavaScript(IDSobj.searchCompany_btn, "search button");
            Thread.Sleep(4000);
            action.waitForPageLoad();
            IList<IWebElement> co = driver.FindElements(By.XPath("//table[@class='table table-bordered table-striped']//td[2]/span"));
            foreach (IWebElement c in co)
            {
                action.isTrue(c.Text.Contains("France"), "County belongs to Company only displayed");
            }
            
            action.selectTextFromDropdown(IDSobj.companyType_Select, "Sales Office");
            action.clickByJavaScript(IDSobj.searchCompany_btn, "search button");
            Thread.Sleep(4000);
            action.waitForPageLoad();
            IList<IWebElement> type = driver.FindElements(By.XPath("//table[@class='table table-bordered table-striped']//td[3]/span"));
            foreach (IWebElement t in type)
            {
                action.isTrue(t.Text.Contains("Sales Office"), "Sales Office Comapany type only displayed");
            }           
            Logger.LogPass("Company tab verified succesfullty");
        }

        public void searchCompany(string name)
        {
            action.enterText(IDSobj.searchCompany_txt, name, "Search Input ");
            action.clickByJavaScript(IDSobj.searchCompany_btn, "search button");
            action.WaitForSpinnerDisappear();
            Thread.Sleep(4000);
            action.waitForPageToComplete();
            string scope = IDSobj.application_List[0].Text;
            Assert.IsTrue(IDSobj.application_Table.Displayed);
            //action.isTrue(scope.Contains(name), "Company search dispalyed");

        }

        public void AddNewCompany(string name)
        {
            action.clickByJavaScript(IDSobj.Company_Tab, "Company Tab");
            action.WaitForPageLoad();
            action.clickByJavaScript(IDSobj.AddCompanyLink, "Add company");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            action.enterText(IDSobj.CustomerNumber, name, "Customer Number");
            //action.clickByJavaScript(IDSobj.continue_Button, "Continue");
            action.clickOnObject(IDSobj.continue_Button, "Continue");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            action.enterText(IDSobj.CompanyName_txt, "Test_Comapny", "Name");
            action.selectTextFromDropdown(IDSobj.country_select, "India");
            action.enterText(IDSobj.Street_txt, "Test", "street");
            action.enterText(IDSobj.ZipCode_txt, "560001", "ZIP code");
            action.enterText(IDSobj.City_txt, "Bangalore", "City");
            action.enterText(IDSobj.State_txt, "Karnataka", "State");

            action.clickByJavaScript(IDSobj.AssignParentCopany, "Select Company");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            action.clickByJavaScript(IDSobj.selectParentIndiaCompany, "India");
            action.clickByJavaScript(IDSobj.selectParentCompany, "Select");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            action.selectTextFromDropdown(IDSobj.DispenserType, "Branch Office");
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            //action.clickOnObject(IDSobj.Save_btn, "Save");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            searchCompany(name);
        }

        public void CancelAddingNewCompany(string name)
        {
            action.clickByJavaScript(IDSobj.Company_Tab, "Company Tab");
            action.WaitForPageLoad();
            selectBrand("Signia");
            action.clickByJavaScript(IDSobj.AddCompanyLink, "Add company");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            action.enterText(IDSobj.CustomerNumber, name, "Existing Customer Number");
            action.clickOnObject(IDSobj.continue_Button, "Continue");
            action.WaitForSpinnerDisappear();
            action.checkElementDisplaybyText("Company with local customer number");
            action.clickOnObject(IDSobj.YES_Button, "Yes");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            //action.clickOnObject(IDSobj.Save_btn, "Save");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();

            selectBrand("Widex");

            action.clickByJavaScript(IDSobj.AddCompanyLink, "Add company");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            action.enterText(IDSobj.CustomerNumber, name, "Existing Customer Number");
            action.clickOnObject(IDSobj.continue_Button, "Continue");
            action.WaitForSpinnerDisappear();
            action.checkElementDisplaybyText("Local customer number already exists");

            action.enterText(IDSobj.CustomerNumber, "347246212662345", "Existing Customer Number");
            action.clickOnObject(IDSobj.continue_Button, "Continue");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            action.WaitForSpinnerDisappear();
            action.checkElementDisplaybyText("No Parent assigned");
            action.checkElementDisplaybyText("Required");
            action.enterText(IDSobj.CompanyName_txt, "Test_Comapny", "Name");
            action.selectTextFromDropdown(IDSobj.country_select, "India");
            action.enterText(IDSobj.Street_txt, "Test", "street");
            action.enterText(IDSobj.ZipCode_txt, "560001", "ZIP code");
            action.enterText(IDSobj.City_txt, "Bangalore", "City");
            action.enterText(IDSobj.State_txt, "Karnataka", "State");

          
            action.clickByJavaScript(IDSobj.cancel_Button, "Cancel");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            action.enterText(IDSobj.searchCompany_txt, "347246212662345", "Search Input ");
            action.clickByJavaScript(IDSobj.searchCompany_btn, "search button");
            action.WaitForSpinnerDisappear();
            Thread.Sleep(4000);
            action.waitForPageToComplete();
            Assert.IsFalse(action.isElementDisplay(IDSobj.application_Table));
            Logger.LogPass("Cancel adding New Comapny working as expected");
        }

        public void editCompany(string name)
        {
            searchCompany(name);
            action.isTrue(IDSobj.application_List[0].Text.Contains("Test_Comapny"),"Comapany Name :"+ IDSobj.application_List[0].Text);
            action.clickByJavaScript(IDSobj.application_List[0], "Company");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            action.enterText(IDSobj.CompanyName_txt, "Test_Company_updated", "Name");
            action.enterText(IDSobj.Street_txt, "Test", "street");
            action.clickByJavaScript(IDSobj.Save_btn, "Save");
            action.WaitForSpinnerDisappear();
            searchCompany(name);
            action.isTrue(IDSobj.application_List[0].Text.Contains("Test_Company_updated"), "Comapany Name :" + IDSobj.application_List[0].Text);
        }

        public void deleteComapany(string name)
        {
            action.clickByJavaScript(IDSobj.Company_Tab, "Company Tab");
            action.waitForPageLoad();
            searchCompany(name);
            action.clickByJavaScript(IDSobj.application_List[0], "Comapany Edit");
            action.waitForPageLoad();
            action.clickByJavaScript(IDSobj.company_Delete, "Delete Company");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Cancel'])[last()]")),"Cancle delete");
            Thread.Sleep(3000);
            action.checkElementDisplay(IDSobj.AssignParentCopany, "Company Edit");
            action.clickByJavaScript(IDSobj.company_Delete, "Delete Company");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("(//button[text()='Confirm'])[last()]")), "Confirm delete");
            action.waitForPageLoad();
            action.enterText(IDSobj.application_input, name, "search Input");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            action.WaitForSpinnerDisappear();
            Assert.IsFalse(action.isElementDisplay(IDSobj.application_Table));
            Logger.LogPass("Comapny deleted succesfully");
        }

        public void verify_TaskTab()
        {

            action.clickByJavaScript(IDSobj.Tasks_Tab, "Company Tab");
            action.WaitForPageLoad();
            action.checkElementDisplay(IDSobj.application_Table, "Company Grid dispalyed :" + IDSobj.application_List.Count);
            //check navigation displays
            action.checkElementDisplaybyText("First");
            action.checkElementDisplaybyText("Previous");
            action.checkElementDisplaybyText("Last");
            action.checkElementDisplaybyText("Next");

            //action.checkElementDisplaybyText("Name");
            //action.checkElementDisplaybyText("User");
            action.checkElementDisplaybyText("Date");
            action.checkElementDisplaybyText("Action");

           


            action.selectTextFromDropdown(IDSobj.TaskType, "Approve/Decline Changes");
            action.clickByJavaScript(IDSobj.application_search, "search button");
            Thread.Sleep(4000);
            action.waitForPageLoad();
            IList<IWebElement> type = driver.FindElements(By.XPath("//table[@class='table table-bordered table-striped']//td[2]/span"));
            foreach (IWebElement t in type)
            {
                action.isTrue(t.Text.Contains("Approve/Decline Changes"), "Task Type filter working as expected");
            }

            action.clickByJavaScript(IDSobj.Task_Details, "Task details");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            action.checkElementDisplaybyText("Property");
            action.checkElementDisplaybyText("Value");
            action.checkElementDisplaybyText("Changed to");
            action.checkElementDisplaybyText("Approve");
            action.checkElementDisplaybyText("Decline");
            //action.checkElementDisplay(IDSobj.Approve_Button, "Approve");
            //action.checkElementDisplay(IDSobj.Decline_Button, "Decline");
            Logger.LogPass("Task tab verified succesfullty");
        }


    }
}
