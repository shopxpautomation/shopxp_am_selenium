﻿using ShopXP_AM.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ShopXP_AM.Utility;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace ShopXP_AM.PageOperations
{
    public class AM_User_pageOperation : SetUp
    {
        AMpageObjects userobj;
        PageOperationBase action;
        SikuliOperations sikuli;

        public AM_User_pageOperation(IWebDriver driver)
        {
            userobj = new AMpageObjects(driver);
            action = new PageOperationBase(driver);
            sikuli = new SikuliOperations();
        }
        public void verifyManageUserPage()
        {
            action.waitForPageLoad();
            action.checkElementDisplay(userobj.userListTable, "List of User ");
            action.checkElementDisplay(userobj.userSearch_Input, "Search Box");
            action.checkElementDisplay(userobj.addUser, "add new User");
            action.checkElementDisplaybyText("Name");
            action.checkElementDisplaybyText2("Email");
            action.checkElementDisplaybyText("Role");
            action.checkElementDisplaybyText("Status");
            action.checkElementDisplaybyText("Filters");
            action.checkElementDisplaybyText("Shop");
            action.checkElementDisplay(userobj.searchShops, "Search Shops");
            action.checkElementDisplaybyText("Role");
            action.checkElementDisplaybyText("Status");
            Logger.LogPass("Number of user dispalying : " + userobj.deleteUser.Count);
        }


        public void verifyRolesInManageUsers()
        {
            action.waitForPageLoad();
            action.checkElementDisplay(userobj.userListTable, "List of User ");
            IDS_API_External_GET externalAPI = new IDS_API_External_GET();
            string AppID = "d09257a8-7a67-46a5-8323-8a38f36d8451";
            IDS_API_External_GET.newToken = true;
            RestClientHelper.token_UserName = "None";
            RestClientHelper.token_Password = "None";
            var response1 = externalAPI.getRolesByAppID(AppID, "0", "20");
            string apiResponse = response1.ToString();
            foreach (IWebElement role in userobj.ListOfRoles)
            {
                action.isTrue(apiResponse.Contains(role.Text), "ShopXP Role " + role.Text);
            }
            
        }

        public void verifyUserSearch(string email)
        {
            action.enterText(userobj.userSearch_Input, "user AUT", "Search User by Name");
            action.WaitForSpinnerDisappear();
            
            Boolean found = true;
            foreach (IWebElement result in userobj.userNamesColoumn)
            {
                if (result.Text.Contains("user AUT"))
                {
                    found = true;
                }
                else
                {
                    found = false;
                }
            }
            if (found)
            {
                Logger.LogPass("User name search working as expected");
            }
            else
            {
                Logger.LogFail("User name search not working as expected");
            }

            action.enterText(userobj.userSearch_Input, "automationTest", "Search user by invalid Name");
            action.WaitForSpinnerDisappear();
            action.checkElementDisplaybyText("There are no users yet");

            action.clickByJavaScript(userobj.searchClear, "clear");
            action.WaitForSpinnerDisappear();
            action.checkElementDisplay(userobj.userListTable, "List of User ");

            action.enterText(userobj.userSearch_Input, "Automation_Admin_0301154640@wsa.com", "Search User by email");
            action.WaitForSpinnerDisappear();
            action.checkElementDisplaybyText("Automation_Admin_0301154640@wsa.com");

            action.enterText(userobj.userSearch_Input, email, "Search User by admin email");
            action.WaitForSpinnerDisappear();
            action.checkElementDisplaybyText("There are no users yet");
            Logger.LogPass("Login user not found in User List");
            action.clickByJavaScript(userobj.searchClear, "clear");
            action.WaitForSpinnerDisappear();
        }

        public void verifyUserFilter()
        {
            int totalcount = getUserCount();
            clickFilterCheckbox("72352342-test com");
            action.WaitForSpinnerDisappear();
            action.isTrue(getUserCount() < totalcount, "Shop filter applied successfully");
            clickFilterCheckbox("72352342-test com");
            action.WaitForSpinnerDisappear();

            clickFilterCheckbox("Shop User");
            Thread.Sleep(4000);
            action.WaitForSpinnerDisappear();
            action.isTrue(getUserCount() < totalcount, "Role filter applied successfully");
            Boolean found1 = true;
            foreach (IWebElement result in userobj.RoleColoumn)
            {
                if (result.Text.Contains("Shop User"))
                {
                    found1 = true;
                }
                else
                {
                    found1 = false;
                }
            }
            if (found1)
            {
                Logger.LogPass("Role filter working as expected");
            }
            else
            {
                Logger.LogFail("Role filter not working as expected");
            }
            clickFilterCheckbox("Company Admin");
            action.WaitForSpinnerDisappear();
            action.isTrue(getUserCount() <= totalcount, "multiple role filter applied successfully");
            clickFilterCheckbox("Company Admin");
            clickFilterCheckbox("Shop User");
            action.WaitForSpinnerDisappear();

            clickFilterCheckbox("Inactive");
            action.WaitForSpinnerDisappear();
            action.isTrue(getUserCount() <= totalcount, "Status filter applied successfully");
            Boolean found2 = true;
            foreach (IWebElement result in userobj.statusColoumn)
            {
                if (result.Text.Contains("Inactive"))
                {
                    found2 = true;
                }
                else
                {
                    found2 = false;
                }
            }
            if (found2)
            {
                Logger.LogPass("Status filter working as expected");
            }
            else
            {
                Logger.LogFail("Status filter not working as expected");
            }

            clickFilterCheckbox("Inactive");
            action.WaitForSpinnerDisappear();
            action.isTrue(getUserCount() == totalcount, "Filter removed successfully");

        }

        public void userListOrderingVerification()
        {
            action.isTrue(driver.Url.Contains("/manageusers"), "URL Contains /manageusers");
            clickFilterCheckbox("Business Admin");
            action.WaitForSpinnerDisappear();
            List<string> unames = new List<string>();
            List<string> status = new List<string>();
            
            int i = 0;
            foreach (IWebElement name in userobj.userNamesColoumn)
            {
                unames.Add(name.Text);
                status.Add(userobj.statusColoumn[i].Text);
                i++;
            }
            List<string> unames2 = unames;
            unames.Sort();
            unames.Reverse();
            int count = status.Count();
            action.isTrue(unames.SequenceEqual(unames2), "Name is in decending order");
            //action.isTrue(status[0].Contains("Active"), "User status ordering : Active");
            //action.isTrue(status[count-1].Contains("Inactive"), "User status ordering : Inactive");

            clickFilterCheckbox("Business Admin");
            action.WaitForSpinnerDisappear();
        }



        public void clickFilterCheckbox(string value)
        {
            IWebElement checkbox = driver.FindElement(By.XPath("//p[text()='"+value+"']//preceding::input[1]"));
            action.clickByJavaScript(checkbox, value);
        }

        public int getUserCount()
        {
            Thread.Sleep(5000);
            string tUser = userobj.totalUser.Text;
            tUser = tUser.Replace(" Users", "");
            int Users = Int32.Parse(tUser);
            return Users;
        }

        public void verifyPanigationInUserList()
        {
            Logger.LogInfo("Verify panigation in user list");
            action.isTrue(userobj.page1.GetAttribute("aria-disabled").Contains("true"),"page 1 button disabled");
            action.isTrue(userobj.previousPage_button.GetAttribute("aria-disabled").Contains("true"), "Previous page button disabled");
            action.isTrue(userobj.firstPage_button.GetAttribute("aria-disabled").Contains("true"), "First page button disabled");

            action.clickByJavaScript(userobj.nextPage_button, "Next Page");
            action.WaitForSpinnerDisappear();
            action.isTrue(userobj.page2.GetAttribute("aria-disabled").Contains("true"), "page 2 button disabled");
            action.isTrue(userobj.previousPage_button.GetAttribute("aria-disabled")==null, "Previous page button enabled");
            action.isTrue(userobj.firstPage_button.GetAttribute("aria-disabled")==null, "First page button enabled");

            action.clickByJavaScript(userobj.previousPage_button, "Previous Page");
            action.WaitForSpinnerDisappear();
            action.isTrue(userobj.page1.GetAttribute("aria-disabled").Contains("true"), "page 1 button disabled");
            action.isTrue(userobj.previousPage_button.GetAttribute("aria-disabled").Contains("true"), "Previous page button disabled");
            action.isTrue(userobj.firstPage_button.GetAttribute("aria-disabled").Contains("true"), "First page button disabled");

            action.clickByJavaScript(userobj.lastPage_button, "Next Page");
            action.WaitForSpinnerDisappear();
            action.isTrue(userobj.lastPageNumber.GetAttribute("aria-disabled").Contains("true"), "Last page button disabled");
            action.isTrue(userobj.lastPage_button.GetAttribute("aria-disabled").Contains("true"), "Last page button disabled");
            action.isTrue(userobj.nextPage_button.GetAttribute("aria-disabled").Contains("true"), "Next page button disabled");
            action.isTrue(userobj.firstPage_button.GetAttribute("aria-disabled") == null, "First page button enabled");

            action.clickByJavaScript(userobj.firstPage_button, "First Page");
            action.WaitForSpinnerDisappear();
            action.isTrue(userobj.page1.GetAttribute("aria-disabled").Contains("true"), "page 1 button disabled");
            action.isTrue(userobj.previousPage_button.GetAttribute("aria-disabled").Contains("true"), "Previous page button disabled");
            action.isTrue(userobj.firstPage_button.GetAttribute("aria-disabled").Contains("true"), "First page button disabled");

            action.clickByJavaScript(userobj.page3, "3 Page");
            action.WaitForSpinnerDisappear();
            action.isTrue(userobj.page3.GetAttribute("aria-disabled").Contains("true"), "page 1 button disabled");

        }

        public void selectRole(string name)
        {
            action.clickByJavaScript(userobj.role_select, "Role select");
            Thread.Sleep(3000);
            action.clickByJavaScript(driver.FindElement(By.XPath("//li[text()='"+name+"']")),name);
            Logger.LogPass("Role selected " + name);
        }

        public void verifyAvaialbeAddress(string shipto)
        {
            Logger.LogInfo("Verify Available shipto address in create users");

          

            //SAM 193 Invalid search
            action.enterText(userobj.addressSearch_input2, "testInvalid", "Search Address by Invalid Name");
            action.WaitForSpinnerDisappear();
            action.checkElementDisplaybyText("No results");

            action.clickByJavaScript(userobj.addUser, "Add User");
            action.WaitForSpinnerDisappear();
            action.checkElementDisplay(userobj.emailIdSearch_Input, "Eamil Search");
            action.enterText(userobj.emailIdSearch_Input, "testaut123@gmail.com", "Email Search");
            action.clickByJavaScript(userobj.checkEmail_btn, "Check Email");
            action.waitForPageLoad();
            action.waitForItemToBeDisplayed(userobj.firstName, 20);
            action.checkElementDisplay(userobj.firstName, "Create User page");
            //action.isTrue(userobj.Add_User.GetAttribute("aria-disabled").Contains("true"), "Add User Button disabled");
            action.checkElementDisplaybyText("Available Addresses");
            action.checkElementDisplay(userobj.addressSearch_input,"Address search");
            selectRole("Business Admin");
            Logger.LogPass("Number of address displaying is " + userobj.address_Radio.Count);

            if (shipto.Contains("Multiple"))
            {
                //action.isTrue(userobj.Add_User.GetAttribute("aria-disabled").Contains("true"), "Add User Button disabled");
                action.clickByJavaScript(userobj.address_Radio[0], "Shipto Address");
                action.isTrue(userobj.Add_User.GetAttribute("aria-disabled") ==null, "Add User Button enabled after slecting Address");
                action.isTrue(userobj.address_Radio[0].Selected, "Address selected as expected");

                //SAM 193 Invalid search
                action.enterText(userobj.addressSearch_input, "12345666", "Search Address by Invalid ID");
                action.WaitForSpinnerDisappear();
                action.checkElementDisplaybyText("No results");

                action.enterText(userobj.addressSearch_input, "Company widex Child Automation 56", "Search Address by Name");
                action.WaitForSpinnerDisappear();
                action.isTrue(userobj.address_Radio.Count == 1,"1 result dispalyed");
                action.isTrue(userobj.address_Radio[0].Selected, "Search Address selected as expected");

                action.enterText(userobj.addressSearch_input, "13052765", "Search Address by ID");
                action.WaitForSpinnerDisappear();
                action.isTrue(userobj.address_Radio.Count == 1, "1 result dispalyed");
                action.isTrue(userobj.address_Radio[0].Selected, "Search Address selected as expected");

                action.enterText(userobj.addressSearch_input, "Company widex child ", "Search Address by ID");
                action.WaitForSpinnerDisappear();
                action.isTrue(userobj.address_Radio.Count > 1, "More Address result dispalyed");
                action.isTrue(userobj.address_Radio[0].Selected==false, "Search Address NOT selected as expected");

                action.clickByJavaScript(userobj.address_Radio[0], "Shipto Address");
                action.isTrue(userobj.Add_User.GetAttribute("aria-disabled") == null, "Add User Button enabled");
                
            }
            else
            {
                action.isTrue(userobj.address_Radio[0].Selected, "Address pre selected as expected");
                action.isTrue(userobj.Add_User.GetAttribute("aria-disabled") == null, "Add User Button enabled");
                Logger.LogPass("Address preSelected when single address present");
            }

            Logger.LogSnapShot("Address Page", driver);
        }


        public void verifyCheckEmailPopUp()
        {
            Logger.LogInfo("Verify Check email Popup");
            action.clickByJavaScript(userobj.addUser, "Add User");
           
            action.checkElementDisplay(userobj.emailIdSearch_Input, "Email Search");
            action.checkElementDisplaybyText("Add User");
            action.checkElementDisplaybyText("Please enter User email");

            
            action.clickByJavaScript(userobj.checkEmail_btn, "Check Email");
            action.waitForPageLoad();
            action.checkElementDisplaybyText("Email is required");

            action.clickByJavaScript(userobj.Cancel_btn, "Cancel");
            action.checkElementDisplay(userobj.addUser, "Add User");
            action.clickByJavaScript(userobj.addUser, "Add User");

            action.enterText(userobj.emailIdSearch_Input, "testaut123", "Invalid Email Search");
            //action.clickByJavaScript(userobj.checkEmail_btn, "Check Email");
            //SAM 186 _Email field should take enter key
            userobj.emailIdSearch_Input.SendKeys(Keys.Enter);
            action.waitForPageLoad();
            action.checkElementDisplaybyText("Email is not valid");

            action.clickByJavaScript(userobj.Cancel_btn, "Cancel");
            action.checkElementDisplay(userobj.addUser, "Add User");

        }

        public void verifyCreateUserFlow()
        {
            Logger.LogInfo("Verify Add User Page");

            action.clickByJavaScript(userobj.addUser, "Add User");
            action.checkElementDisplay(userobj.emailIdSearch_Input, "Email Search");
            action.enterText(userobj.emailIdSearch_Input, "assfagad@gmail.com", "Email Search");
            action.clickByJavaScript(userobj.checkEmail_btn, "Check Email");
            action.waitForPageLoad();
            action.waitForItemToBeDisplayed(userobj.firstName, 20);
            action.checkElementDisplaybyText("assfagad@gmail.com");
            action.checkElementDisplaybyText("Personal Information");
            action.checkElementDisplaybyText("Access To Functionalities");
            action.checkElementDisplaybyText("Available Addresses");
            action.checkElementDisplaybyText("Online Shop");
            action.checkElementDisplay(userobj.firstName, "First Name");
            action.checkElementDisplay(userobj.lastname, "Last Name");
            action.checkElementDisplay(userobj.phoneNumber, "Phone Number");
            action.checkElementDisplay(userobj.jobtitle, "Job Title");
            action.isTrue(userobj.Add_User.GetAttribute("aria-disabled") == null, "Add User Button enabled after slecting Address and Role");
            action.clickByJavaScript(userobj.Add_User, "Add User");
            action.checkElementDisplaybyText("Please select one of the addresses");
            action.isTrue(userobj.role_select.GetAttribute("class").Contains("partials-error"), "Roles Required");

            //action.isTrue(userobj.Add_User.GetAttribute("aria-disabled").Contains("true"), "Add User Button disabled");
            action.clickByJavaScript(userobj.address_Radio[0], "Shipto Address");
            selectRole("Business Admin");
            action.isTrue(userobj.Add_User.GetAttribute("aria-disabled") == null, "Add User Button enabled after slecting Address and Role");

            action.isTrue(userobj.statusInput.GetAttribute("aria-checked").Contains("true"), "Status Active displayed");

            action.clickByJavaScript(userobj.Add_User, "Add User");
            action.checkElementDisplaybyText("First Name is required");
            action.checkElementDisplaybyText("Last Name is required");

            action.enterText(userobj.phoneNumber, "fasfa", "Phone Number");
            action.checkElementDisplaybyText("Last Name is required");

            action.clickByJavaScript(userobj.Cancel_btn, "Cancel button");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            action.checkElementDisplay(userobj.userListTable, "User List");
        }

        public void createUser(string email,string status,string Role)
        {
            action.waitForPageLoad();
            int totalcount = getUserCount();
            Logger.LogPass("User Count Before Add User :" + totalcount);
            Logger.LogInfo("Add User :"+email);
            action.clickByJavaScript(userobj.addUser, "Add User");
            action.checkElementDisplay(userobj.emailIdSearch_Input, "Email Search");
            action.enterText(userobj.emailIdSearch_Input, email, "Email Search");
            action.clickByJavaScript(userobj.checkEmail_btn, "Check Email");
            action.waitForPageLoad();
            action.waitForItemToBeDisplayed(userobj.firstName, 20);
            action.isTrue(driver.Url.Contains("/manageusers/add"), "URL Contains /manageusers/add");
            if (status.Contains("InActive"))
            {
                action.clickByJavaScript(userobj.statusInput, "Status Inactive");
            }
            string fName = "Test_" + DateTime.Now.ToString("ddMMHHmmss");
            action.enterText(userobj.firstName, fName, "First Name");
            action.enterText(userobj.lastname, "Automation", "Last Name");
            action.enterText(userobj.phoneNumber, "9258445522" , "Phone Number");
            action.enterText(userobj.jobtitle, "Tester" , "Job Title");
            action.clickByJavaScript(userobj.address_Radio[0], "Shipto Address");
            selectRole(Role);
            action.clickByJavaScript(userobj.Add_User, "Add User");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            action.checkElementDisplay(userobj.successMessage, "Success Message");
            action.checkElementDisplaybyText(fName+ " Automation has been created");
            action.clickByJavaScript(userobj.msgBarClose, "Close message bar");
            int totalcount_After = getUserCount();
            Logger.LogPass("User Count After Adding User :" + totalcount_After);
            //SAM 219
            action.isTrue(totalcount_After == totalcount + 1, "All users are loaded after creating new User");


        }

        public void verifyUserExist(string email,string status)
        {
            action.enterText(userobj.userSearch_Input, email, "Search User by email");
            userobj.userSearch_Input.SendKeys(Keys.Enter);
            action.WaitForSpinnerDisappear();
            action.isTrue(userobj.statusColoumn[0].Text.Contains(status), "Status is " + status);
            action.isTrue(userobj.emailColoumn[0].Text.Contains(email), "email is " + email);
            action.isTrue(userobj.emailColoumn.Count == 1, "1 Search result displayed");
           
        }

        public void clearSearchInUserGrid()
        {
            action.clickByJavaScript(userobj.clearSearch, "Clear button");
            action.WaitForSpinnerDisappear();

        }

        public void verifyDeleteUser(string email)
        {
            IWebElement delete = driver.FindElement(By.XPath("//p[text()='" + email + "']//following::td[@class='delete-user']/button"));
            action.clickByJavaScript(delete, "Delete Element");
            action.waitForPageLoad();
            action.checkElementDisplaybyText("Are you sure that you want to delete the user");
            action.checkElementDisplay(userobj.Cancel_btn, "Cancel");
            action.clickByJavaScript(userobj.Cancel_btn, "Cancel button");
            action.waitForPageLoad();
            action.checkElementDisplay(delete, "User List page");

        }


        public void DeleteUser(string email)
        {
            IWebElement delete = driver.FindElement(By.XPath("//p[text()='"+ email + "']//following::td[@class='delete-user']/button"));
            action.clickByJavaScript(delete, "Delete Element");
            action.waitForPageLoad();
            action.checkElementDisplaybyText("Are you sure that you want to delete the user");
            action.clickByJavaScript(userobj.Delete_Button, "Delete");
            action.WaitForSpinnerDisappear();
            action.checkElementDisplay(userobj.successMessage, "Success Message");
            //action.checkElementDisplaybyText("Automation has been deleted");
            action.clickByJavaScript(userobj.msgBarClose, "Close message bar");
            action.enterText(userobj.userSearch_Input, email, "Search for deleted User");
            action.WaitForSpinnerDisappear();
            action.checkElementDisplaybyText("There are no users yet");
            Logger.LogPass("User Deleted Successfully");
        }

        public void verifyEditUserPage(string email)
        {
            Logger.LogInfo("Verify Edit User page");
            IWebElement edit = driver.FindElement(By.XPath("//p[text()='" + email + "']//following::td[@class='edit-user']/button"));
            //verify edit icon displayed
            //sikuli.verifyImageExists("Edit_Icon.PNG");

            action.clickByJavaScript(edit, "Edit User button");
           // Thread.Sleep(10000);
            action.WaitForSpinnerDisappear();
            action.checkElementDisplaybyText("Personal Information");
            action.checkElementDisplaybyText("Access To Functionalities");
            action.checkElementDisplaybyText("Available Addresses");
            action.checkElementDisplaybyText("Online Shop");
            action.checkElementDisplay(userobj.firstName, "First Name");
            action.checkElementDisplay(userobj.lastname, "Last Name");
            action.checkElementDisplay(userobj.phoneNumber, "Phone Number");
            action.checkElementDisplay(userobj.jobtitle, "Job Title");
            action.isTrue(userobj.address_Radio[0].Selected, "Search Address selected as expected");
            action.checkElementDisplay(userobj.role_select, "Role select");
            action.checkElementDisplay(userobj.Cancel_btn, "Cancel Button");
            action.checkElementDisplay(userobj.Save_btn, "Save Button");
            action.isTrue(driver.Url.Contains("/manageusers/edit"), "URL Contains  /manageusers/edit");
            action.clickByJavaScript(userobj.Cancel_btn, "Cancel Button");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            action.checkElementDisplay(userobj.userListTable, "User List");
        }

        public void editUserAndVerify(string email,string status,string Role)
        {
            IWebElement edit = driver.FindElement(By.XPath("//p[text()='" + email + "']//following::td[@class='edit-user']/button"));
            action.clickByJavaScript(edit, "Edit User button");
            action.WaitForSpinnerDisappear();
            action.isTrue(userobj.statusInput.GetAttribute("aria-checked").Contains("true"), "Status Active displayed before Edit");
            action.checkElementDisplaybyText(Role);
            action.isTrue(userobj.address_Radio[0].Selected, "Address selected as expected");
            action.isTrue(userobj.Save_btn.GetAttribute("aria-disabled").Contains("true"), "Save Button disabled when there is no changes");


            Logger.LogInfo("Edit User with new value");
            action.clickByJavaScript(userobj.statusInput, "Status to Inactive");
            string fName = "Test_" + DateTime.Now.ToString("ddMMHHmmss");
            action.enterText(userobj.firstName, fName, "First Name");
            action.enterText(userobj.lastname, "Automation Test 2", "Last Name");
            action.enterText(userobj.phoneNumber, "34624525224", "Phone Number");
            action.enterText(userobj.jobtitle, "Developer", "Job Title");
            action.clickByJavaScript(userobj.address_Radio[1], "Shipto Address");
            selectRole("Shop User");
            action.isTrue(userobj.Save_btn.GetAttribute("aria-disabled") == null, "Save Button enabled when there is changes");
            action.clickByJavaScript(userobj.Save_btn, "Save button");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            action.checkElementDisplay(userobj.successMessage, "Success Message");
            action.checkElementDisplaybyText(fName + " Automation Test 2 has been updated");
            action.clickByJavaScript(userobj.msgBarClose, "Close message bar");
            verifyUserExist(email,"Inactive");

           
            Logger.LogInfo("Veify Edited User");
            IWebElement edit2 = driver.FindElement(By.XPath("//p[text()='" + email + "']//following::td[@class='edit-user']/button"));
            action.clickByJavaScript(edit2, "Edit User button");
            action.WaitForSpinnerDisappear();
            action.isTrue(userobj.statusInput.GetAttribute("aria-checked").Contains("false"), "Status InActive displayed after Edit");
            action.checkElementDisplaybyText("Shop User");
            action.isTrue(userobj.address_Radio[1].Selected, "Address selected as expected");
            action.isTrue(userobj.firstName.GetAttribute("Value").Contains(fName), "First Name : "+fName);
            action.isTrue(userobj.lastname.GetAttribute("Value").Contains("Automation Test 2"), "Last Name : Automation Test 2");
            action.isTrue(userobj.phoneNumber.GetAttribute("Value").Contains("34624525224"), "Phone Number : 34624525224");
            action.isTrue(userobj.jobtitle.GetAttribute("Value").Contains("Developer"), "Job Title : Developer");
            Logger.LogSnapShot("User edited successfully", driver);

            action.clickByJavaScript(userobj.Cancel_btn, "Cancel Button");
            action.waitForPageLoad();
            action.WaitForSpinnerDisappear();
            action.checkElementDisplay(userobj.userListTable, "User List");

        }

        public void createPendingUser(string customerNumber,string email)
        {
            Logger.LogInfo("Register User "+email);
            action.isElementDisplay(userobj.loginCreateUser);
            action.clickByJavaScript(userobj.loginCreateUser, "Register User");
            action.waitForPageLoad();
            action.isElementDisplay(userobj.create_FirstName);

            action.enterText(userobj.create_FirstName, "Test ","First Name");
            action.enterText(userobj.create_LastName, "Automation ", "Last Name");
            action.enterText(userobj.create_email, email, "Email");

            action.selectTextFromDropdown(userobj.create_Laungauge, "English (English)");
           
            action.selectTextFromDropdown(userobj.create_County, "United Kingdom");
            action.enterText(userobj.create_Company, customerNumber, "Company");
            action.clickOnObject(userobj.create_filterCompanies, "Search Company");
            Thread.Sleep(5000);
            action.clickOnObject(userobj.RegisterUser, "Register User");
            Thread.Sleep(5000);
            action.waitForPageLoad();
            action.checkElementDisplaybyText("Thank you"); 
            action.clickByJavaScript(userobj.Close_RegisterSuccess, "Close");
            action.waitForPageLoad();
        }
       

        public void verifyApproveUserFlow(string useremail)
        {
            Logger.LogInfo("Verify Approve User flow");
            action.waitForPageLoad();
            action.enterText(userobj.userSearch_Input, useremail, "Search User by email");
            Thread.Sleep(5000);
            action.WaitForSpinnerDisappear();
            action.isTrue(userobj.statusColoumn[0].Text.Contains("Pending"), "Status is Pending" );
            action.isTrue(userobj.emailColoumn[0].Text.Contains(useremail), "email is " + useremail);
            action.isTrue(userobj.emailColoumn.Count == 1, "1 Search result displayed");

            //action.moveToElement(userobj.actionColoumn[0]);
            //action.checkElementDisplay(userobj.Pending_Tooltip, "Approve/ Decline tooltip");
            action.clickByJavaScript(userobj.actionColoumn[0], "Action Tooltip");
            action.WaitForSpinnerDisappear();
            action.checkElementDisplaybyText(useremail);
            action.checkElementDisplay(userobj.Approve_button, "Approve button");
            action.checkElementDisplay(userobj.Decline_button, "Decline button");
            action.checkElementDisplay(userobj.Cancel_btn, "Cancel button");

            action.clickOnObject(userobj.Cancel_btn, "Cancel Button");
            action.WaitForSpinnerDisappear();
            action.waitForPageLoad();
            action.checkElementDisplay(userobj.userSearch_Input, "Manage User Page");
            action.enterText(userobj.userSearch_Input, useremail, "Search User by email");
            action.WaitForSpinnerDisappear();
            action.isTrue(userobj.statusColoumn[0].Text.Contains("Pending"), "Status is Pending");
            action.isTrue(userobj.emailColoumn[0].Text.Contains(useremail), "email is " + useremail);
            action.isTrue(userobj.emailColoumn.Count == 1, "1 Search result displayed");
        }

        public void approveUser(string useremail,string status)
        {
            action.enterText(userobj.userSearch_Input, useremail, "Search User by email");
            action.WaitForSpinnerDisappear();
            action.clickByJavaScript(userobj.actionColoumn[0], "Action Tooltip");
            action.WaitForSpinnerDisappear();
            selectRole("Default");
            if (status.Contains("Active"))
            {
                action.isTrue(userobj.statusInput.GetAttribute("aria-checked").Contains("true"), "Status Active displayed before Edit");
            }
            else
            {
                action.clickByJavaScript(userobj.statusInput, "Status InActive");
            }
            action.clickByJavaScript(userobj.Approve_button, "Approve Button");
            action.WaitForSpinnerDisappear();

            action.checkElementDisplay(userobj.successMessage, "Success Message");
            action.checkElementDisplaybyText("User Test Automation has been updated and approved");
            action.clickByJavaScript(userobj.msgBarClose, "Close message bar");

            action.enterText(userobj.userSearch_Input, useremail, "Search User by email");
            action.WaitForSpinnerDisappear();
            action.isTrue(userobj.statusColoumn[0].Text.Contains(status), "Status is "+status);
            action.isTrue(userobj.emailColoumn[0].Text.Contains(useremail), "email is " + useremail);

            action.isTrue(userobj.emailColoumn.Count == 1, "1 Search result displayed");
        }

        public void declineUser(string useremail)
        {
            action.enterText(userobj.userSearch_Input, useremail, "Search User by email");
            action.WaitForSpinnerDisappear();
            action.clickByJavaScript(userobj.actionColoumn[0], "Action Tooltip");
            action.WaitForSpinnerDisappear();
            selectRole("Default");
            action.clickByJavaScript(userobj.Decline_button, "Decline Button");
            action.WaitForSpinnerDisappear();

            action.checkElementDisplay(userobj.successMessage, "Success Message");
            action.checkElementDisplaybyText("User has been declined");
            action.clickByJavaScript(userobj.msgBarClose, "Close message bar");

            action.enterText(userobj.userSearch_Input, useremail, "Search User by email");
            action.WaitForSpinnerDisappear();
            action.checkElementDisplaybyText("There are no users yet");
        }


        public void verifyAlreadyProcesedUser()
        {
            //driver.Close();
            //driver.SwitchTo().DefaultContent();
            List<String> tabs = new List<String>(driver.WindowHandles);
            driver.SwitchTo().Window(tabs[0]);
            action.clickByJavaScript(userobj.actionColoumn[0], "Action Tooltip");
            action.waitForPageLoad();
            action.checkElementDisplaybyText("Already processed");
            action.checkElementDisplaybyText("The user’s request is already processed. No more action required");
            action.clickByJavaScript(userobj.ok_btn, "Ok button");
        }








        }
}
