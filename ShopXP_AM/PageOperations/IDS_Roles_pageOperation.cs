﻿using ShopXP_AM.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ShopXP_AM.Utility;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ShopXP_AM.PageOperations
{
    public class IDS_Roles_pageOperation : SetUp
    {
        IDS_Roles_pageObject roleObj;
        PageOperationBase action;


        public IDS_Roles_pageOperation(IWebDriver driver)
        {
            roleObj = new IDS_Roles_pageObject(driver);
            action = new PageOperationBase(driver);
        }
        public void openRolesTab()
        {
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.Role_Tab, "Role Tab");
            action.clickOnObject(roleObj.Role_Tab, "Roles Tab");
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.Add_Role, "Role Page");
        }

        public void openUserTab()
        {
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.User_Tab, "User Tab");
            action.clickOnObject(roleObj.User_Tab, "User Tab");
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.Add_Role, "User Page");
        }


        public void verifyRolePageNavigation()
        {
            Logger.LogInfo("Roles page navigation validation");
            action.waitForPageLoad();
            Assert.IsTrue(roleObj.listOfRoles.Count< 21);
            Logger.LogPass("Less Than 20 Roles dislayed as expected in a page");
            action.AssertEqual(roleObj.listOfRoles[0].FindElements(By.TagName("th"))[0].Text, "Role");
            action.AssertEqual(roleObj.listOfRoles[0].FindElements(By.TagName("th"))[1].Text, "Application");
            int pageNumber = Int32.Parse(roleObj.pageNumber.Text);

            int totalPage = Int32.Parse(roleObj.totalPage.Text);
            if (totalPage > 1)
            {

            action.isTrue(roleObj.First.GetAttribute("Class").Contains("disabled"), "First button disabled");
            action.isTrue(roleObj.Previous.GetAttribute("Class").Contains("disabled"), "Previous button disabled");
            action.isTrue(roleObj.Next.GetAttribute("Class").Contains(""), "Next button enabled");
            action.isTrue(roleObj.Last.GetAttribute("Class").Contains(""), "Last button enabled");

            action.isTrue(Int32.Parse(roleObj.pageNumber.Text) == 1, "First Page");
            action.clickByJavaScript(roleObj.Next, "Next Page");
            Thread.Sleep(3000);
            action.isTrue(Int32.Parse(roleObj.pageNumber.Text) == 2, "Next Page");
            action.waitForPageLoad();
            action.isTrue(roleObj.First.GetAttribute("Class").Contains(""), "First button enabled");
            action.isTrue(roleObj.Previous.GetAttribute("Class").Contains(""), "Previous button enabled");

            action.clickByJavaScript(roleObj.Previous, "Previous Page");
            action.waitForPageLoad();
            action.isTrue(Int32.Parse(roleObj.pageNumber.Text) == 1, "First Page");

            action.clickByJavaScript(roleObj.Last, "Last Page");
            action.waitForPageLoad();
            action.isTrue(Int32.Parse(roleObj.pageNumber.Text) == totalPage, "Last Page");

            action.isTrue(roleObj.First.GetAttribute("Class").Contains(""), "First button disabled");
            action.isTrue(roleObj.Previous.GetAttribute("Class").Contains(""), "Previous button disabled");
            action.isTrue(roleObj.Next.GetAttribute("Class").Contains("disabled"), "Next button enabled");
            action.isTrue(roleObj.Last.GetAttribute("Class").Contains("disabled"), "Last button enabled");

            action.clickByJavaScript(roleObj.First, "First Page");
            action.waitForPageLoad();

            }

        }


        public void verifySearchRole()
        {
            Logger.LogInfo("Roles page search validation");
            action.selectTextFromDropdown(roleObj.ApplicationId_filter, "ShopXP");
            action.clickByJavaScript(roleObj.Search_button, "Search Button");
            action.waitForPageLoad();
            //action.isTrue(roleObj.listOfRoles[1].FindElements(By.TagName("td"))[1].FindElement(By.XPath("//span")).Text.Contains("ShopXP"), "Search by application is working as expected");
            action.checkElementDisplaybyText("ShopXP");

            action.enterText(roleObj.Search_input, "Company Admin", "Search Role");
            action.clickByJavaScript(roleObj.Search_button, "Search Button");
            action.waitForPageLoad();

            action.checkElementDisplaybyText("Company Admin");
            //action.isTrue(roleObj.listOfRoles[1].FindElement(By.TagName("td")).Text.Contains("Company Admin"), "Search by role is working as expected");
            //action.isTrue(roleObj.tooltip.GetAttribute("data-original-title").Contains("ShopXP Company User"), "Tooltip displayed as expected");
        }

        public void SearchRole(string text)
        {
            Logger.LogInfo("Search Role");

            action.enterText(roleObj.Search_input, text, "Search Role");
            action.clickByJavaScript(roleObj.Search_button, "Search Button");
            action.waitForPageLoad();
            IWebElement searchResult1 = roleObj.listOfRoles[1].FindElement(By.TagName("span"));
            action.isTrue(searchResult1.Text.Contains(text), "Search result displayed as expected");
            action.clickByJavaScript(searchResult1, "Search Result");
            action.waitForPageLoad();

            action.checkElementDisplay(roleObj.RollNAme_txt, " Role Details Page");
        }


        public void CreateRolePageValidation()
        {
            Logger.LogInfo("Create Roles page validation");
            action.clickOnObject(roleObj.Add_Role, "Add Role");
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.RollNAme_txt, "Create Role Page");
            action.clickByJavaScript(roleObj.save_Role, "Save button");
            action.waitForPageLoad();
           // action.isTrue(roleObj.RollNAme_txt.GetAttribute("class").Contains("form-control form-control-error"), "Role Name required error displayed");
            //action.isTrue(roleObj.application_select.GetAttribute("class").Contains("dropdown form-control form-control-error"), "Application required error displayed");
            action.enterText(roleObj.RollNAme_txt, "Test Cancel", "Role Name");
            action.enterText(roleObj.RollDescription_txt, "Test Description", "Description");
            action.selectTextFromDropdown(roleObj.application_select, "DAM Signia");
            action.waitForPageLoad();
            int scopeCount = roleObj.scope_table.FindElements(By.TagName("tr")).Count;
            action.isTrue(scopeCount > 0, scopeCount + "Scopes displayed as expected as application DAM Rexton");
            action.clickByJavaScript(roleObj.cancel_Role, "Delete Role");
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.Add_Role, "Naviagted to Roles list page as expected by clicking cancel button");
        }

        public void CreateNewRole(string Name, String Description, string application)
        {
            Logger.LogInfo("Create new Role");
            action.clickOnObject(roleObj.Add_Role, "Add Role");
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.RollNAme_txt, "Create Role Page");
            action.enterText(roleObj.RollNAme_txt, Name, "Role Name");
            action.enterText(roleObj.RollDescription_txt, Description, "Description");
            action.selectTextFromDropdown(roleObj.application_select, application);
            action.waitForPageLoad();
            int scopeCount = roleObj.scope_table.FindElements(By.TagName("tr")).Count;
            action.isTrue(scopeCount > 0, scopeCount + " Scopes displayed as expected as application " + application);

            foreach (IWebElement scope in driver.FindElements(By.XPath("//input[@type='checkbox']")))
            {
                action.clickByJavaScript(scope, "scope select ");
            }
            action.clickByJavaScript(roleObj.save_Role, "Save Role");
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.Add_Role, "Naviagted to Roles list page as expected by clicking Save button");
        }

        public void VerifyCreatedNewRole(string Name, String Description, string application)
        {
            Logger.LogInfo("Verify Created new Role");
            action.isTrue(roleObj.RollNAme_txt.GetAttribute("value").Contains(Name), "Role Name displayed as expected :" + Name);
            action.isTrue(roleObj.RollDescription_txt.GetAttribute("value").Contains(Description), "Role Despription displayed as expected :" + Description);
            action.isTrue(roleObj.applicationName_lbl.Text.Contains(application), "Application Name displayed as expected :" + application);
        }

        public void VerifyEditRole(string OldName, string newName, string oldDescription, string newDescription)
        {
            action.clickByJavaScript(roleObj.cancel_Role, "Cancel Role");
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.Add_Role, "Role Page");
            SearchRole(OldName);
            Logger.LogInfo("Verify updated Role details");
            action.isTrue(roleObj.RollNAme_txt.GetAttribute("value").Contains(OldName), "Role Name displayed as expected :" + OldName);
            action.isTrue(roleObj.RollDescription_txt.GetAttribute("value").Contains(oldDescription), "Role Despription displayed as expected :" + oldDescription);
            action.enterText(roleObj.RollDescription_txt, newDescription, "Description");
            action.enterText(roleObj.RollNAme_txt, newName, "Role Name");
            action.clickByJavaScript(roleObj.scopeCheckBox, "Scope");
            Assert.IsFalse(action.isElementDisplay(roleObj.application_select));
            Thread.Sleep(4000);
            //action.clickOnObject(roleObj.save_Role, "Save Role");
            action.clickOnObject(roleObj.save_Role, "Save Role");
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.Add_Role, "Role Page");
            SearchRole(newName);
            Logger.LogInfo("Verify updated Role details");
            action.isTrue(roleObj.RollNAme_txt.GetAttribute("value").Contains(newName), "Role Name displayed as expected :" + newName);
            action.isTrue(roleObj.RollDescription_txt.GetAttribute("value").Contains(newDescription), "Role Despription displayed as expected :" + newDescription);

        }


        public void VerifyDeleteRole(string Name)
        {
            Logger.LogInfo("Verify Delete Role");
            action.clickByJavaScript(roleObj.delete_Role, "Delete Role");
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.deleteRolePopUp, "Are you sure want to delete");
            action.clickByJavaScript(roleObj.deletePopUp_Cancel, "Cancel delete");
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.RollNAme_txt, "Create Role Page");
            action.clickByJavaScript(roleObj.delete_Role, "Delete Role");
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.deleteRolePopUp, "Are you sure want to delete");
            action.clickByJavaScript(roleObj.deletePopUp_Confirm, "Confirm delete");
            action.waitForPageLoad();
            action.checkElementDisplay(roleObj.Add_Role, "Role Page");

            action.enterText(roleObj.Search_input, Name, "Search Role");
            action.clickByJavaScript(roleObj.Search_button, "Search Button");
            Thread.Sleep(3000);
            action.waitForPageLoad();
            Assert.IsFalse(action.isElementDisplay(roleObj.pageNumber));
            Logger.LogPass("Role Deleted Succesfully " + Name);
        }

       

        }

    
}
